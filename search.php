<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
		?>
		<title> BRK </title>
	</head>
	<body>
		<?php getHeaderView(); ?>
		<div class="container">
			<?php
				getComingSoonView(); 
			?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>