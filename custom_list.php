<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
			loadGoods(); 
		?>
		<style type="text/css">
                </style>
		<!-- cdn for modernizr, if you haven't included it already -->
		<script src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
		<!-- polyfiller file to detect and load polyfills -->
		<script src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>
		<script>
		  webshims.setOptions('waitReady', false);
		  webshims.setOptions('forms-ext', {types: 'date'});
		  webshims.polyfill('forms forms-ext');
		</script>
		<script type="text/javascript" charset="UTF-8">
			function update(){
                            document.getElementById('total_count').innerHTML = "загрузка";		
			}
                        function show($flow){
                            if(!document.getElementById("checkbox_" + $flow).checked){
                                document.getElementById($flow).style.width = "0";
                                document.getElementById($flow).style.height = "0";
                                document.getElementById($flow).style.padding = "0px";
                                document.getElementById($flow).style.margin = "0px";  
                            }
                            else {
                                document.getElementById($flow).style.width = "auto";
                                document.getElementById($flow).style.height = "130px";
                                document.getElementById($flow).style.padding = "0 10px";
                                document.getElementById($flow).style.margin = "10px";  
                            }
                        }
                        
		</script>
		<title> BRK </title>
	</head>
	<body>
                
		<?php getHeaderView(); ?>
		<div class="container">
			<?php 
				//TODO change to main url
				global $main_url;
				$url = $main_url . "custom_list.php"; 
				$is_sells = "";
				$is_net_sells = "";
				$is_returns = "";
				$is_moves = "";
				$is_debits = "";
				$is_flows = "";
                                $datetime = new DateTime();
                                $to_date = $datetime->format('Y-m-d');
				$datetime = new DateTime();
                                $datetime->modify('-1 month');
                                $from_date = $datetime->format('Y-m-d');
                                global $all_stores,$all_categories,$all_inflows,$all_outflows,$all_accounts,$all_employees;
                                if(isset($_GET["stores"]))
                                    $selected_stores = $_GET["stores"];
                                else    $all_stores = true;
                                if(isset($_GET["employees"]))
                                    $selected_employees = $_GET["employees"];
                                else    $all_employees = true;
                                if(isset($_GET["inflows"]))
                                    $selected_inflows = $_GET["inflows"];
				else    $all_inflows = true;
                                if(isset($_GET["outflows"]))
                                    $selected_outflows = $_GET["outflows"];
				else    $all_outflows = true;
                                if(isset($_GET["accounts"]))
                                    $selected_accounts = $_GET["accounts"];
				else    $all_accounts = true;
                                if(isset($_GET["categories"]))
                                    $selected_categories = $_GET["categories"];
				else $all_categories = true;
                                if(isset($_GET['checkbox_sells'])) 
					$is_sells = "checked";
				if(isset($_GET['checkbox_net_sells'])) 
					$is_net_sells = "checked";
				if(isset($_GET['checkbox_returns'])) 
					$is_returns = "checked";
				if(isset($_GET['checkbox_moves'])) 
					$is_moves = "checked";
				if(isset($_GET['checkbox_debits'])) 
					$is_debits = "checked";
				if(isset($_GET['checkbox_inflows'])) 
					$is_inflows = "checked";
				else $all_inflows = true;
                                if(isset($_GET['checkbox_outflows'])) 
					$is_outflows = "checked";
				else $all_outflows = true;
                                if(isset($_GET['checkbox_categories'])) 
					$is_categories = "checked";
                                else    $all_categories = true;
                                if(isset($_GET['to_date'])) 
					$to_date = $_GET['to_date'];
				if(isset($_GET['from_date'])) 
					$from_date = $_GET['from_date'];
//                                if(isset($_GET['all_stores'])) 
//                                    $all_stores = $_GET['all_stores'];
//                                if(isset($_GET['all_employees'])) 
//                                    $all_employees = $_GET['all_employees'];
//                                if(isset($_GET['all_categories'])) 
//                                    $all_categories = $_GET['all_categories'];
//                                if(isset($_GET['all_inflows'])) 
//                                    $all_inflows = $_GET['all_inflows'];
//                                if(isset($_GET['all_outflows'])) 
//                                    $all_outflows = $_GET['all_outflows'];
//                                if(isset($_GET['all_outflows'])) 
//                                    $all_outflows = $_GET['all_outflows'];
//            

			?>
			<form action="<?php echo $url; ?>" method="GET" style="width:100%; height:auto; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;" >
				<div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
					<input id="" type="checkbox" name="checkbox_sells" <?php echo $is_sells; ?> /> Продажи<br>
					<input id="" type="checkbox" name="checkbox_net_sells" <?php echo $is_net_sells; ?> /> Интернет продажи<br>
					<input id="" type="checkbox" name="checkbox_returns" <?php echo $is_returns; ?> /> Возвраты<br>
					<input id="" type="checkbox" name="checkbox_moves" <?php echo $is_moves; ?> /> Перемещения<br>
					<input id="" type="checkbox" name="checkbox_debits" <?php echo $is_debits; ?> /> Оприходования<br>
					<input id="checkbox_inflows" type="checkbox" onchange="show('inflows')" name="checkbox_inflows" <?php echo $is_inflows; ?> /> Притоки<br>
                                        <input id="checkbox_outflows" type="checkbox" onchange="show('outflows')" name="checkbox_outflows" <?php echo $is_outflows; ?> /> Оттоки<br>
				        <input id="checkbox_categories" type="checkbox" onchange="show('categories')" name="checkbox_categories" <?php echo $is_categories; ?> /> Категории<br>
				</div>
				<div style="width:auto; height:auto; overflow:auto; border:solid 1px #aabbcc; float:left; margin:10px; padding: 0 10px;">
					Период времени:<br>
					от: <input type="date" name="from_date" style="margin:10px;" value="<?php echo $from_date; ?>" /><br>
					до: <input type="date" name="to_date" style="margin:10px;" value="<?php echo $to_date; ?>" /><br>
				</div>
				<div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
				<?php
                                    loadStores();
                                    global $stores;
                                    echo "<input type='checkbox' name='all_stores' ";
                                    if(isset($all_stores))
                                        echo " checked ";
                                    echo "/> Все магазины <br>";
                                    foreach ($stores as $key => $store) {
                                        echo "<input type='checkbox' name='stores[" . $store["store_id"] . "]' ";
                                        if(isset($selected_stores[$store["store_id"]]))
                                            echo " checked ";
                                        echo "/> " . $store["name"] . " <br>";
                                    }
                                ?>
				</div>
				<div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
				<?php
                                    loadEmployees();
                                    global $employees;
                                    echo "<input type='checkbox' name='all_employees' ";
                                    if(isset($all_employees))
                                        echo " checked ";
                                    echo "/> Все сотрудники <br>";
                                    foreach ($employees as $key => $emp) {
                                        echo "<input type='checkbox' name='employees[" . $emp["id"] . "]' ";
                                        if(isset($selected_employees[$emp["id"]]))
                                            echo " checked ";
                                        echo "/> " . $emp["name"] . " <br>";
                                    }
                                ?>
				</div>
                                <div id="accounts" style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
				<?php
                                    loadAccounts();
                                    global $accounts;
                                    echo "<input type='checkbox' name='all_accounts' ";
                                    if(isset($all_accounts))
                                        echo " checked ";
                                    echo "/> Все кассы <br>";
                                    foreach ($accounts as $key => $account) {
                                        echo "<input type='checkbox' name='accounts[" . $account["id"] . "]' ";
                                        if(isset($selected_accounts[$account["id"]]))
                                            echo " checked ";
                                        echo "/> " . $account["title"] . " <br>";
                                    }
                                ?>
				</div>
					
				<div id="inflows" style="width:0; height:0px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:0px; padding: 0px;">
				<?php
                                    loadFlowCategories();
                                    global $inflows;
                                    echo "<input type='checkbox' name='all_inflows' ";
                                    if(isset($all_inflows))
                                        echo " checked ";
                                    echo "/> Все притоки <br>";
                                    foreach ($inflows as $key => $inflow) {
                                        echo "<input type='checkbox' name='inflows[" . $inflow["cashflow_type_id"] . "]' ";
                                        if(isset($selected_inflows[$inflow["cashflow_type_id"]]))
                                            echo " checked ";
                                        echo "/> " . $inflow["title"] . " <br>";
                                    }
                                ?>
				</div>
				<div id="outflows" style="width:0; height:0px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:0px; padding: 0px;">
				<?php
                                    global $outflows;
                                    echo "<input type='checkbox' name='all_outflows' ";
                                    if(isset($all_outflows))
                                        echo " checked ";
                                    echo "/> Все оттоки <br>";
                                    foreach ($outflows as $key => $outflow) {
                                        echo "<input type='checkbox' name='outflows[" . $outflow["cashflow_type_id"] . "]' ";
                                        if(isset($selected_outflows[$outflow["cashflow_type_id"]]))
                                            echo " checked ";
                                        echo "/> " . $outflow["title"] . " <br>";
                                    }
                                ?>
				</div>
				
				<div id="categories" style="width:0; height:0px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:0px; padding: 0px;">
				<?php
                                    loadCategories();
                                    global $categories;
                                    echo "<input type='checkbox' name='all_categories' ";
                                    if(isset($all_categories))
                                        echo " checked ";
                                    echo "/> Все категории <br>";
                                    foreach ($categories as $key => $category) {
                                        echo "<input type='checkbox' name='categories[" . $category["cat_id"] . "]' ";
                                        if(isset($selected_categories[$category["cat_id"]]))
                                            echo " checked ";
                                        echo "/> " . $category["title"] . " <br>";
                                    }
                                ?>
				</div>
				<input class="UPD_BTN btn btn-primary" type="submit" style="width:150px; height:auto; overflow:auto; border:solid 1px #C3E4FE; float:left; margin:10px;  padding: 2em 0; text-align: center; font-size:24px;" onclick="update();" value="Обновить"/>
				
				
				
			</form>
			<div id="custom_table">
			<?php
                            getSelectedActionsView(); 
                            $user = unserialize($_COOKIE["user"]);
                            if(hasAccessToNetPrice($user['job'])){
                                ?>
                                <p>Сумма цен: <?php echo $GLOBALS["total_prices"];?> </p>
                                <div style="width:auto; height:auto; overflow:auto; border:solid 1px #aabbcc; float:right; margin:10px; padding: 0 10px 0 10px;">
                                    Итого продаж:<br>
                                    <p>Количество: <span id="total_count"><?php echo $GLOBALS["total_sell_count"]; ?> штук</span>  </p>
                                    <p>Сумма:      <span><?php echo $GLOBALS["total_sell_money"]; ?> тенге</span> </p>
                                    <p>Сумма себ: <span><?php echo $GLOBALS["total_sell_net_prices"]; ?> тенге</span> </p>
                                </div>
                                <div style="width:auto; height:auto; overflow:auto; border:solid 1px #aabbcc; float:right; margin:10px; padding: 0 10px 0 10px;">
                                    Интернет продаж:<br>
                                    <p>Количество: <span id="total_count"><?php echo $GLOBALS["total_net_sell_count"]; ?> штук</span>  </p>
                                    <p>Сумма:      <span><?php echo $GLOBALS["total_net_sell_money"]; ?> тенге</span> </p>
                                    <p>Сумма себ: <span><?php echo $GLOBALS["total_net_sell_net_prices"]; ?> тенге</span> </p>
                                </div>
                                <div style="width:auto; height:auto; overflow:auto; border:solid 1px #aabbcc; float:right; margin:10px; padding: 0 10px 0 10px;">
                                    Итого перемещении:<br>
                                    <p>Количество: <span id="total_count"><?php echo $GLOBALS["total_move_count"]; ?> штук</span>  </p>
                                    <p>Сумма:      <span><?php echo $GLOBALS["total_move_money"]; ?> тенге</span> </p>
                                    <p>Сумма себ: <span><?php echo $GLOBALS["total_move_net_prices"]; ?> тенге</span> </p>
                                </div>
                                <?php 
                                }
                            ?>
			</div>
		</div>
                <script>
                    show("inflows");
                    show("outflows");
                    show("categories");
                </script>
		<?php getFooterView(); ?>
	</body>
</html>
<script type="text/javascript" charset="UTF-8">
    function update(){
        document.getElementById('total_count').innerHTML = "загрузка";
        /*
        custom_table = '<div class="table">';
        custom_table += '<table class="my_table table table-striped table-bordered table-hover ">';
        custom_table += '<thead>	<tr> <td>Asadasd</td><td>Asadasd</td><td>Asadasd</td></tr>	</thead>';
        custom_table += '<tbody>	<tr> <td>' + '<?php //echo $selected_actions; ?>' +'</td><td>Asadasd</td><td>Asadasd</td></tr> ';
        custom_table += '<tr> <td>' + to_date + '</td><td>Asadasd</td><td>Asadasd</td></tr>  </tbody>';
        custom_table += '</table>';
        custom_table += '</div>';
        document.getElementById('custom_table').innerHTML = custom_table;
        var foo = <?php //echo json_encode($somePhpVar); ?>
        is_sells = document.getElementById('checkbox_sells').value;
        is_returns = document.getElementById('checkbox_returns').value;
        is_moves = document.getElementById('checkbox_moves').value;
        is_debits = document.getElementById('checkbox_debits').value;
        is_flows = document.getElementById('checkbox_flows').value;
        to_date = document.getElementById('to_date').value;
        from_date = document.getElementById('from_date').value;
        selected_actions = 0;
        if(is_sells)   selected_actions += 1;
        if(is_returns) selected_actions += 2;
        if(is_moves)   selected_actions += 4;
        if(is_debits)  selected_actions += 8;
        if(is_flows)   selected_actions += 16;				
        */
    }
</script>