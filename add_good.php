<?php ob_start(); ?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead(); 
		checkPostAdd();
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php addGoodFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>