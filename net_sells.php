<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
            <?php 
                    include_once 'config.php'; 
                    checkLoggedIn("yes");
                    getHead();
                    global $main_url;
                    setcookie('SAVED_PAGE', $main_url . "net_sells.php", 0, '/');   
            ?>
            <title> BRK </title>
    </head>
    <body>
        <?php getHeaderView(); ?>
        <div class="container">
            <?php
                loadGoods(); 
                $user = unserialize($_COOKIE["user"]);
                $user["selected_actions"] = 255;
                $emp_id = 0;
                $datetime = new DateTime();
                $to_date = $datetime->format('Y-m-d');
                $datetime = new DateTime();
                $datetime->modify('-1 month');
                $from_date = $datetime->format('Y-m-d');
                if(isset($_GET["stores"]))
                    $selected_stores = $_GET["stores"];
                else    $all_stores = true;
                if(isset($_GET["employees"]))
                    $selected_employees = $_GET["employees"];
                else    $all_employees = true;
                if(isset($_GET["categories"]))
                    $selected_categories = $_GET["categories"];
                else    $all_categories = true;
                if(isset($_GET['checkbox_net_sells'])) 
                        $is_net_sells = "checked";
                if(isset($_GET['to_date'])) 
                        $to_date = $_GET['to_date'];
                if(isset($_GET['from_date'])) 
                        $from_date = $_GET['from_date'];
                if(isset($_GET['all_stores'])) 
                    $all_stores = $_GET['all_stores'];
                if(isset($_GET['all_employees'])) 
                    $all_employees = $_GET['all_employees'];
                if(isset($_GET['all_categories'])) 
                    $all_categories = $_GET['all_categories'];
            ?>
            <form action="<?php echo $url; ?>" method="GET" style="width:100%; height:auto; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;" >
                <div style="width:auto; height:auto; overflow:auto; border:solid 1px #aabbcc; float:left; margin:10px; padding: 0 10px;">
                        Период времени:<br>
                        от: <input type="date" name="from_date" style="margin:10px;" value="<?php echo $from_date; ?>" /><br>
                        до: <input type="date" name="to_date" style="margin:10px;" value="<?php echo $to_date; ?>" /><br>
                </div>
                <div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
                <?php
                    loadStores();
                    global $stores;
                    echo "<input type='checkbox' name='all_stores' ";
                    if(isset($all_stores))
                        echo " checked ";
                    echo "/> Все магазины <br>";
                    foreach ($stores as $key => $store) {
                        echo "<input type='checkbox' name='stores[" . $store["store_id"] . "]' ";
                        if(isset($selected_stores[$store["store_id"]]))
                            echo " checked ";
                        echo "/> " . $store["name"] . " <br>";
                    }
                ?>
                </div>
                <div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
                <?php
                    loadEmployees();
                    global $employees;
                    echo "<input type='checkbox' name='all_employees' ";
                    if(isset($all_employees))
                        echo " checked ";
                    echo "/> Все сотрудники <br>";
                    foreach ($employees as $key => $emp) {
                        echo "<input type='checkbox' name='employees[" . $emp["id"] . "]' ";
                        if(isset($selected_employees[$emp["id"]]))
                            echo " checked ";
                        echo "/> " . $emp["name"] . " <br>";
                    }
                ?>
                </div>
                <div id="categories" style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
                <?php
                    loadCategories();
                    global $categories;
                    echo "<input type='checkbox' name='all_categories' ";
                    if(isset($all_categories))
                        echo " checked ";
                    echo "/> Все категории <br>";
                    foreach ($categories as $key => $category) {
                        echo "<input type='checkbox' name='categories[" . $category["cat_id"] . "]' ";
                        if(isset($selected_categories[$category["cat_id"]]))
                            echo " checked ";
                        echo "/> " . $category["title"] . " <br>";
                    }
                ?>
                </div>
                <input class="UPD_BTN btn-primary btn" type="submit" style="width:150px; height:auto; overflow:auto; border:solid 1px #C3E4FE; float:left; margin:10px;  padding: 2em 0; text-align: center; font-size:24px;" onclick="update();" value="Обновить"/>

                


        </form>
            <div class="table">
                <h2 class="text-center">Интернет продажи:</h2>
                <table class="my_table table table-striped table-bordered table-hover ">
                    <thead>	
                        <tr>
                            <?php
                            $headers = array("Магазин", "Касса", "Сотрудник", "Категория","Товар","Коментарии", "Цена", "Дата заказа","Дата оплаты", "Кол", "Статус", "Отменить");
                            foreach ($headers as $key => $header) {
                                echo "<td>" . $header . "</td>";
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>	
                    <?php
                        $user = unserialize($_COOKIE["user"]);
                        loadNetSells();
                        loadStores();
                        loadEmployees();
                        loadGoods();
                        loadCategories();
                        loadAccounts();
                        global $net_sells, $goods, $stores, $employees;
                        
                        foreach ($net_sells as $key => $action) {
                            if (isset($_GET["from_date"]) && $action['sell_date'] < $_GET["from_date"]){
                                echo "from";
                                continue;
                            }
                            if (isset($_GET["to_date"]) && $action['sell_date'] > ($to_date . " 23:59:59")){
                                echo "to" .$action['sell_date'] . $to_date;
                                continue;
                            }
                            if ( !isset($all_categories) && (!isset($selected_categories[$goods[$action["good_id"]]['cat_id']])) ){
                                echo "cat";
                                continue;
                            }
                            if ( !isset($all_stores) && !isset($selected_stores[$action['store_id']])){
                                echo "store";
                                continue;
                            }
                            if ( !isset($all_employees) && !isset($selected_employees[$action['employee_id']])){
                                echo "emp";
                                continue;
                            }                            
                            $price = "-";
                            if (isset($action['price']))
                                $price = $action['price'];
                            
                            $count = "-";
                            if (isset($action['count']))
                                $count = $action['count'];
                            
                            $sell_date = toDate($action["sell_date"]) . " "  . toTime($action["sell_date"]);
                            if(isset($action["cash_date"]) && $action["cash_date"]>"2000-00-00")
                                $cash_date = toDate($action["cash_date"]) . " "  . toTime($action["cash_date"]);
                            else 
                                $cash_date = "-";
                            $category = "-";
                            if(isset($action["good_id"]))
                                $category = $GLOBALS["categories"][$goods[$action["good_id"]]['cat_id']]["title"];

                            $good = "-";
                            if (isset($action["good_id"]) && isset($goods[$action["good_id"]]["name"]))
                                $good = "<a href='" . $goods[$action["good_id"]]["url"] . "' >" . $goods[$action["good_id"]]["name"] . "</a>";
                            else if ($action["table"] == "cashflow")
                                $good = $action['comment'];

                            $employee = "-";
                            if (isset($employees[$action["employee_id"]]["name"]))
                                $employee = "<a href='employee.php?emp_id=" . $action["employee_id"] . "'>" .
                                        $employees[$action["employee_id"]]["surname"] . " " . 
                                        $employees[$action["employee_id"]]["name"] . " " .
                                        //$employees[$action["employee_id"]]["patronymic"] .	
                                        "</a>";

                            $store = "-";
                            if (isset($stores[$action["store_id"]]["name"]))
                                $store = "<a href='" . $stores[$action["store_id"]]["url"] . "'>" . $stores[$action["store_id"]]["name"] . "</a>";
                            
                            $comment = "-";
                            if (isset($action["comment"]))
                                $comment = $action["comment"];
                            
                            $account = "-";
                            if (isset($accounts[$action["account_id"]]["title"]))
                                $account = "<a href='" . $accounts[$action["account_id"]]["url"] . "'>" . $accounts[$action["account_id"]]["title"] . "</a>";
                            $background = "";
                            if($action["is_net_sell"] == 2){
                                $background = "background-color:#E0EDFF;";
                            }
                            echo "<tr style=" . "'$background'" . ">";
                            echo "<td>" . $store . "</td>";
                            echo "<td>" . $account . "</td>";
                            echo "<td>" . $employee . "</td>";
                            echo "<td>" . $category . "</td>";
                            echo "<td>" . $good . "</td>";
                            echo "<td>" . $comment . "</td>";
                            echo "<td>" . $price . "</td>";
                            echo "<td>" . $sell_date . "</td>";
                            echo "<td>" . $cash_date . "</td>";
                            echo "<td>" . $count . "</td>";
                            $operation_info = "?table=" . $action["table"] . "&type=" . $action["type"] . "&id=" . $action["id"];
                            ?>
                        <!-- old button 
                            <td><button class='myButton'>
                                <span class='glyphicon glyphicon-pencil' onclick="if (confirm('Уверен, что хочешь изменить?'))
                                            location.href = 'edit_operation.php<?php echo $operation_info; ?>';" style='font-size: 20px;'></span>
                            </button>
                        </td> 
                            <button class='myButton' type="button" onclick="if (confirm('Уверен, что хочешь удалить?'))
                                    location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                                <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
                            </button>
                            -->
                        <td>
                        <?php 
                            if($action["is_net_sell"] == 2){ 
                        ?>
                            <button class='btn btn-primary' onclick="if (confirm('Уверен, что хочешь подтвердить продажу?'))
                                            location.href = 'confirm_net_sell.php?sell_id=<?php echo $action["sell_id"]; ?>';">
                                Произвести<br>
                                оплату<br>
                            </button>
                        <?php 
                            }
                            else { 
                        ?>
                            <button class='btn btn-success' onclick="if (confirm('Уверен, что хочешь отменить оплату?'))
                                            location.href = 'unconfirm_net_sell.php?sell_id=<?php echo $action["sell_id"]; ?>';">
                                Оплачено<br>
                            </button>
                        <?php 
                            }
                        ?>
                        </td> 
                        <td>
                            <button class='myButton' type="button" onclick="location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                                <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
                            </button>
                        </td> 
                        </tr>
                    <?php } 

                    ?>
                    </tbody>
                </table>
            </div>
        </div>  
        <?php getFooterView(); ?>
    </body>
</html>