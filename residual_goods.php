<?php ob_start(); ?>
<!DOCTYPE html>
<html>
	<head>		
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes"); 
			getHead(); 
			$cat_id = -1;
			if(isset($_GET["cat_id"])) $cat_id = $_GET["cat_id"];
			global $page;
			$page = "residual_goods.php";
		?>
        <title> Все товары </title>
	</head>
	<body>
			<?php getHeaderView(); ?>
			<?php 
				//TODO change to main url
				global $main_url;
				$url = $main_url . $page; 
                                global $selected_categories,$selected_stores,$in_way;
				if(isset($_GET["stores"]))
                                    $selected_stores = $_GET["stores"];
				if(isset($_GET["categories"]))
                                    $selected_categories = $_GET["categories"];
                                if(isset($_GET["in_way"]))
                                    $in_way = $_GET["in_way"];
//                                echo "<pre>";
//                                var_dump($selected_stores);
//                                echo "</pre>";
				
			?>
			<form action="<?php echo $url; ?>" method="GET" style="width:100%; height:auto; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;" >
                            <div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
                            <?php
                                loadStores();
                                global $stores;
                                echo "<input type='checkbox' name='in_way' ";
                                    if(isset($in_way))
                                        echo " checked ";
                                    echo "/> Заказы в пути <br>";
                                foreach ($stores as $key => $store) {
                                    echo "<input type='checkbox' name='stores[" . $store["store_id"] . "]' ";
                                    if(isset($selected_stores[$store["store_id"]]))
                                        echo " checked ";
                                    echo "/> " . $store["name"] . " <br>";
                                }
                                
                            ?>
                            </div>
                            <div style="width:auto; height:130px; overflow:auto; border:solid 1px #aabbcc; float:left;  margin:10px; padding: 0 10px;">
                            <?php
                                loadCategories();
                                global $categories;
                                foreach ($categories as $key => $category) {
                                    echo "<input type='checkbox' name='categories[" . $category["cat_id"] . "]' ";
                                    if(isset($selected_categories[$category["cat_id"]]))
                                        echo " checked ";
                                    echo "/> " . $category["title"] . " <br>";
                                }
                            ?>
                            </div>

                            <input class="btn btn-primary" type="submit" style="width:150px; height:auto; overflow:auto; border:solid 1px #C3E4FE; float:left; margin:10px;  padding: 2em 0; text-align: center; font-size:24px;" onclick="update();" value="Обновить"/>

				
			</form>
			<div class="container" style="margin: auto 0 auto 0;">
				<h2 style="text-align:center; margin-top:0px;" > 
					<?php
						loadCategories(); 
						if($cat_id == -1)
							echo "Все товары компании";
					?> 
				</h2>
				<div  style=" margin: 0 auto; padding: 0 auto; width: 100%;" >
					<?php getResidualGoodsView(); ?>
				</div>
			</div>
			<?php getFooterView(); ?>
	</body>
</html>