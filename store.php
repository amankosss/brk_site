<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
			loadStores();
			$days = 7; $active = 1; $store = reset($stores);	
			if(isset($_GET["days"])) 	$days =  $_GET["days"];
			if(isset($_GET["active"])) 	$active =  $_GET["active"];
			if(isset($_GET["store_id"])) 	$store = $stores[$_GET["store_id"]];
		?>
		<title> Магазин </title>
	</head>
	<body>
		<?php getHeaderView(); ?>
		<div class="container">
			<?php
				getStoreInfoView();
				echo '<h1 class = "top_text"> Последние продажи магазина:</h1>';
				getTabs($active,"store.php?store_id=".$store["store_id"]."&");
				getCompanySellsView($store["store_id"]); 
			?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>