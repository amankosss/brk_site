<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php';
			checkLoggedIn("yes");
			loadEmployees();
			$emp_id = reset($employees);
			if(isset($_GET["emp_id"])) $emp_id = $_GET["emp_id"]; 
		?>
    	<?php getHead(); ?>
    	<title> BRK </title>
	</head>
	<body >
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getUserInfoView($emp_id); ?>
			<?php getUserActionsView($emp_id); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>