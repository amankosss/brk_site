<?php ob_start(); ?>
<!DOCTYPE html>
<html>
	<head>		
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes"); 
			getHead(); 
			$cat_id = -1;
			if(isset($_GET["cat_id"])) $cat_id = $_GET["cat_id"];
			global $page;
			$page = "all_goods.php";
		?>
    	<title> Все товары </title>
	</head>
	<body>
			<?php getHeaderView(); ?>
			<div class="container" style="margin: auto 0 auto 0;">
				<h2 style="text-align:center; margin-top:0px;" > 
					<?php
						loadCategories(); 
						if($cat_id == -1)
							echo "Все товары компании";
						else 
							echo $categories[$cat_id]["title"]; 
					?> 
				</h2>
				<div class="col-lg-3" style="width:auto; margin:0; padding:0;">
					<?php getCategoriesView(); ?>
				</div>
				<div class="col-lg-9" style="margin: 0 auto;">
					<?php getAllGoodsView(); ?>
				</div>
			</div>
			<?php getFooterView(); ?>
	</body>
</html>