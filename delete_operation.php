<?php 
//DON'T TOUCH
ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php';
			checkLoggedIn("yes");
			checkPostDeleteOperation();
		?>
    	<?php getHead(); ?>
    	<title> BRK </title>
	</head>
	<body >
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getDeleteOperationFormView(); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>