<?php  
	//ob_start();
	if(!isset($_COOKIE["user"]))
		header("Location: oauth.php");
	
?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead(); 
		checkPostSell();
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php sellGoodFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>