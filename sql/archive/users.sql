-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Июн 05 2016 г., 18:45
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bhelp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(30) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `surname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `patronymic` varchar(30) CHARACTER SET utf8 NOT NULL,
  `pic_url` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8 NOT NULL,
  `info` text CHARACTER SET utf8 NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 NOT NULL,
  `job` int(30) NOT NULL DEFAULT '0',
  `comp_id` int(30) NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `patronymic`, `pic_url`, `gender`, `info`, `type`, `job`, `comp_id`, `email`, `password`) VALUES
(1, 'Аманкос', 'Рахымбергенов', 'Канатович', '', 'M', 'Mobile Developer', 'seller', 0, 1, 'amankosss@mail.ru', 'A555555a'),
(2, 'Елена', 'Сергеева', '', '', 'F', 'Бухгалтер', 'counter', 15, 2, 'elena@gmail.com', 'A555555a'),
(5, 'Елена', 'Смирнова', 'Олеговна', NULL, 'F', 'Активистка компании, высшее образование...', 'keeper', 2, 4, 'keeper_meloman@gmail.com', 'A555555a'),
(4, 'Иван', 'Иванов', 'Иванович', NULL, 'M', 'Образованный, пунктуальный ...', 'seller', 1, 4, 'seller_meloman@gmail.com', 'A555555a'),
(6, 'Аманкос', 'Рахымбергенов', 'Канатович', 'images/amankos_ava.jpg', 'M', 'Выпускник КБТУ, ответственный , пунктуальный...', 'admin', 255, 4, 'admin_meloman@gmail.com', 'A555555a'),
(7, 'Аманкос', 'Рахымбергенов', 'Канатович', 'images/amankos_ava.jpg', 'M', 'Выпускник КБТУ, ответственный , пунктуальный...', 'admin', 255, 5, 'amankosss@gmail.com', 'A555555a'),
(8, 'Гаухар', 'Каиргалиева', 'Ерболовна', '', 'F', '', 'admin', 255, 5, 'g.kairgaliyeva@brk.kz', 'qwerty123');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
