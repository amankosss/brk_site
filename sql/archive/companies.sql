-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Июн 16 2016 г., 08:10
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bhelp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `comp_id` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Очистить таблицу перед добавлением данных `companies`
--

TRUNCATE TABLE `companies`;
--
-- Дамп данных таблицы `companies`
--

INSERT INTO `companies` (`comp_id`, `name`, `description`, `email`) VALUES
(1, 'TOO BRK', 'Financial company', 'amankosss@mail.ru'),
(2, 'TOO KAZMUNAIGAZ', 'Fuel creater company', 'kazmaz@kmg.kz'),
(3, 'TOO "Logycom"', 'Компания занимающаяся торговлей качественной техники по доступной цене. ', 'logycom@gmail.com'),
(4, 'Меломан', 'ООО Меломан — казахстанская ритейлинговая, дистрибуторская и производственная компания, специализирующаяся на производстве и продаже аудио- и видеопродукции, книг, бытовой электроники и сопутствующих товаров.', 'meloman@gmail.com'),
(5, 'BRK Consulting', 'Financial company', 'amankosss@gmail.com');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`comp_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `comp_id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
