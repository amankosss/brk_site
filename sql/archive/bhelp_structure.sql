-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Июн 16 2016 г., 07:38
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bhelp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cashflows`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `cashflows`;
CREATE TABLE IF NOT EXISTS `cashflows` (
  `cashflow_id` int(11) NOT NULL,
  `cashflow_type_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `comp_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `cashflow_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `cashflow_types`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `cashflow_types`;
CREATE TABLE IF NOT EXISTS `cashflow_types` (
  `cashflow_type_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `flow_type` int(11) NOT NULL,
  `is_real` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(30) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(30) NOT NULL DEFAULT '0',
  `level` int(30) NOT NULL DEFAULT '1',
  `comp_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--
-- Создание: Июн 16 2016 г., 05:36
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `client_id` int(30) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL,
  `comp_id` int(30) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `surname` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `companies`;
CREATE TABLE IF NOT EXISTS `companies` (
  `comp_id` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `debits`
--
-- Создание: Июн 15 2016 г., 11:15
--

DROP TABLE IF EXISTS `debits`;
CREATE TABLE IF NOT EXISTS `debits` (
  `debit_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `net_price` int(11) DEFAULT NULL,
  `employee_id` int(30) NOT NULL,
  `debit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--
-- Создание: Июн 16 2016 г., 05:35
--

DROP TABLE IF EXISTS `goods`;
CREATE TABLE IF NOT EXISTS `goods` (
  `good_id` int(30) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `valid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `author` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `net_price` int(11) DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `comp_id` int(11) NOT NULL,
  `pic_url` varchar(255) DEFAULT NULL,
  `bonus` int(11) NOT NULL DEFAULT '0',
  `discount` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `history`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `history_id` int(30) NOT NULL,
  `emp_id` int(30) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `history_date` datetime NOT NULL,
  `history_text` text NOT NULL,
  `history_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `moves`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `moves`;
CREATE TABLE IF NOT EXISTS `moves` (
  `move_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `from_store_id` int(30) NOT NULL,
  `to_store_id` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `move_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `net_prices`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `net_prices`;
CREATE TABLE IF NOT EXISTS `net_prices` (
  `net_price_id` int(30) NOT NULL,
  `from_date` datetime NOT NULL,
  `net_price` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `returns`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `returns`;
CREATE TABLE IF NOT EXISTS `returns` (
  `return_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `client_id` int(30) NOT NULL,
  `return_date` datetime NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `discount` int(30) NOT NULL,
  `bonus` int(30) NOT NULL,
  `reason` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `sells`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `sells`;
CREATE TABLE IF NOT EXISTS `sells` (
  `sell_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `client_id` int(30) NOT NULL,
  `sell_date` datetime NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `discount` int(30) NOT NULL,
  `bonus` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `stores`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `store_id` int(11) NOT NULL,
  `pic_url` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
-- Создание: Июн 16 2016 г., 05:35
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(30) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 NOT NULL,
  `surname` varchar(30) CHARACTER SET utf8 NOT NULL,
  `patronymic` varchar(30) CHARACTER SET utf8 NOT NULL,
  `pic_url` varchar(200) CHARACTER SET utf8 DEFAULT NULL,
  `gender` varchar(10) CHARACTER SET utf8 NOT NULL,
  `info` text CHARACTER SET utf8 NOT NULL,
  `type` varchar(30) CHARACTER SET utf8 NOT NULL,
  `job` int(30) NOT NULL DEFAULT '0',
  `comp_id` int(30) NOT NULL,
  `email` varchar(30) CHARACTER SET utf8 NOT NULL,
  `password` varchar(30) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cashflows`
--
ALTER TABLE `cashflows`
  ADD PRIMARY KEY (`cashflow_id`);

--
-- Индексы таблицы `cashflow_types`
--
ALTER TABLE `cashflow_types`
  ADD PRIMARY KEY (`cashflow_type_id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`comp_id`);

--
-- Индексы таблицы `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`debit_id`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`good_id`),
  ADD UNIQUE KEY `id` (`good_id`);

--
-- Индексы таблицы `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`);

--
-- Индексы таблицы `moves`
--
ALTER TABLE `moves`
  ADD PRIMARY KEY (`move_id`);

--
-- Индексы таблицы `net_prices`
--
ALTER TABLE `net_prices`
  ADD PRIMARY KEY (`net_price_id`);

--
-- Индексы таблицы `returns`
--
ALTER TABLE `returns`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `sells`
--
ALTER TABLE `sells`
  ADD PRIMARY KEY (`sell_id`);

--
-- Индексы таблицы `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cashflows`
--
ALTER TABLE `cashflows`
  MODIFY `cashflow_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `cashflow_types`
--
ALTER TABLE `cashflow_types`
  MODIFY `cashflow_type_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `comp_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `debits`
--
ALTER TABLE `debits`
  MODIFY `debit_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `good_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `moves`
--
ALTER TABLE `moves`
  MODIFY `move_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `net_prices`
--
ALTER TABLE `net_prices`
  MODIFY `net_price_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `returns`
--
ALTER TABLE `returns`
  MODIFY `return_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `sells`
--
ALTER TABLE `sells`
  MODIFY `sell_id` int(30) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `stores`
--
ALTER TABLE `stores`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
