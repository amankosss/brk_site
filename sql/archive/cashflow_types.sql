-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Июн 16 2016 г., 07:59
-- Версия сервера: 5.5.42
-- Версия PHP: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bhelp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `cashflow_types`
--
-- Создание: Июн 09 2016 г., 11:43
--

DROP TABLE IF EXISTS `cashflow_types`;
CREATE TABLE IF NOT EXISTS `cashflow_types` (
  `cashflow_type_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `flow_type` int(11) NOT NULL,
  `is_real` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Очистить таблицу перед добавлением данных `categories`
--

TRUNCATE TABLE `categories`;

--
-- Дамп данных таблицы `cashflow_types`
--

INSERT INTO `cashflow_types` (`cashflow_type_id`, `title`, `description`, `flow_type`, `is_real`, `comp_id`) VALUES
(8, 'Прочие поступления', '', 1, 1, 5),
(9, 'За семинар', '', 1, 1, 5),
(10, 'Покупка', 'Покупка книг', 0, 1, 5),
(11, 'Единицы', '', 0, 1, 5),
(12, 'Зарплата', '', 0, 1, 5),
(13, 'Интернет', '', 0, 1, 5),
(14, 'Прочее', '', 0, 1, 5),
(15, 'Связь сити', '', 0, 1, 5),
(16, 'Отток к Алмазу', '', 0, 0, 5),
(17, 'Аренда', '', 0, 1, 5);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `cashflow_types`
--
ALTER TABLE `cashflow_types`
  ADD PRIMARY KEY (`cashflow_type_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `cashflow_types`
--
ALTER TABLE `cashflow_types`
  MODIFY `cashflow_type_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
