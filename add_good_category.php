<?php ob_start(); ?>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php';
			checkLoggedIn("yes");
			getHead();
			checkPostAddCategory();
		?>
    	<title> BRK </title>
	</head>
	<body >
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getAddGoodCategoryFormView(); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>