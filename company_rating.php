<?php ob_start(); ?>
<!DOCTYPE html>
<html>
	<head>		
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes"); 
			getHead();
			$days = 7; $active = 2;
			if(isset($_GET["days"])) $days = $_GET["days"];
			if(isset($_GET["active"])) $active = $_GET["active"];
		?>
    	<title> BRK </title>
	</head>
	<body>
			<?php getHeaderView(); ?>
			<div class="container">
				<?php getTabs($active,"company_rating.php?"); ?>
				<h1 class = "top_text"> Архив продаж компании </h1>
				<?php getCompanySellsView(0); ?>
			</div>
			<?php getFooterView(); ?>
	</body>
</html>