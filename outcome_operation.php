<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php';
			checkLoggedIn("yes");
			checkPostFlowOperation();
		?>
    	<?php getHead(); ?>
    	<title> BRK </title>
	</head>
	<body >
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getFinanceOutcomeOperationFormView(); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>