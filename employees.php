<?php ob_start(); ?>
<!DOCTYPE html>
<html>
    <head>		
        <?php 
            include_once 'config.php'; 
            checkLoggedIn("yes"); 
            getHead(); 
        ?>
        <title> Все товары </title>
    </head>
    <body>
        <?php getHeaderView(); ?>
        <div class="container" >
            <h2 style="text-align:center; margin-top:0px;" > Все сотрудники компании </h2>
            <div class="col-lg-offset-1 col-lg-10">
                <?php # employees views
                loadEmployees();
                global $employees;
                ?>
                <div class="table" style="margin-left:10px;">
                    <table class="my_table table table-striped table-bordered table-hover ">
                        <thead>
                            <tr>
                                <?php
                                $days = 7;
                                $table_headers = array("№", "ФИО", "Почта", "Должность");
                                foreach ($table_headers as $key => $value) {
                                    echo "<td>" . $value . "</td>";
                                }
                                ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $cnt = 1;
                            $user = unserialize($_COOKIE["user"]);
                            foreach ($employees as $key => $emp) {
                                //if($emp["id"] != $_SESSION["user"]["id"]){
                                echo "<tr ";
                                if ($emp["id"] == $user["id"])
                                    echo " style='' ";
                                echo " >";
                                echo "<td>" . $cnt++ . "</td>";
                                echo "<td><a href='employee.php?emp_id=" . $emp["id"] . "'>" .
                                $emp["surname"] . " " . $emp["name"] . " " . $emp["patronymic"] . "</a></td>";
                                echo "<td>" . $emp["email"] . "</td>";
                                echo "<td>" . $emp["type"] . "</td>";
                                echo "</tr>";
                                //}
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php getFooterView(); ?>
    </body>
</html>