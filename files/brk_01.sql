-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 28 2015 г., 08:33
-- Версия сервера: 10.0.17-MariaDB
-- Версия PHP: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `brk_01`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(30) NOT NULL,
  `title` varchar(200) NOT NULL,
  `parent_id` int(30) NOT NULL DEFAULT '0',
  `level` int(30) NOT NULL DEFAULT '1',
  `comp_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`cat_id`, `title`, `parent_id`, `level`, `comp_id`) VALUES
(3, 'Детективы', 0, 1, 4),
(4, 'Боевики', 0, 1, 4),
(5, 'Драмы', 0, 1, 4),
(6, 'Комедии', 0, 1, 4),
(7, 'Мультфилмы', 0, 1, 4),
(8, 'Мелодрамы', 0, 1, 4),
(9, 'Триллеры', 0, 1, 4),
(10, 'Ужасы', 0, 1, 4),
(11, 'Фантастика', 0, 1, 4),
(12, 'Спортивные', 0, 1, 4),
(13, 'Другие', 0, 1, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE `clients` (
  `client_id` int(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`client_id`, `phone`, `email`, `password`, `comp_id`, `name`, `surname`) VALUES
(2, '87789158745', 'amankosss@gmail.com', 'A555555a', 4, 'Аманкос', 'Рахымбергенов');

-- --------------------------------------------------------

--
-- Структура таблицы `companies`
--

CREATE TABLE `companies` (
  `comp_id` int(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `companies`
--

INSERT INTO `companies` (`comp_id`, `name`, `description`, `email`) VALUES
(1, 'TOO BRK', 'Financial company', 'amankosss@gmail.com'),
(2, 'TOO KAZMUNAIGAZ', 'Fuel creater company', 'kazmaz@kmg.kz'),
(3, 'TOO "Logycom"', 'Компания занимающаяся торговлей качественной техники по доступной цене. ', 'logycom@gmail.com'),
(4, 'Меломан', 'ООО Меломан — казахстанская ритейлинговая, дистрибуторская и производственная компания, специализирующаяся на производстве и продаже аудио- и видеопродукции, книг, бытовой электроники и сопутствующих товаров.', 'meloman@gmail.com');

-- --------------------------------------------------------

--
-- Структура таблицы `debits`
--

CREATE TABLE `debits` (
  `debit_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `debit_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `debits`
--

INSERT INTO `debits` (`debit_id`, `good_id`, `count`, `comp_id`, `store_id`, `employee_id`, `debit_date`) VALUES
(1, 11, 100, 4, 3, 5, '2015-11-01 13:34:52'),
(2, 12, 80, 4, 3, 5, '2015-11-01 13:34:52'),
(3, 13, 200, 4, 3, 5, '2015-11-02 14:11:21'),
(4, 14, 150, 4, 3, 5, '2015-11-03 12:22:25'),
(5, 15, 120, 4, 3, 5, '2015-11-02 15:45:27'),
(6, 16, 110, 4, 3, 5, '2015-11-04 13:47:38'),
(7, 17, 150, 4, 3, 5, '2015-11-02 16:34:17'),
(8, 18, 190, 4, 3, 5, '2015-11-01 12:32:13'),
(9, 12, 210, 4, 3, 5, '2015-11-03 17:25:00'),
(10, 15, 20, 4, 3, 5, '2015-11-02 12:32:22'),
(11, 12, 100, 4, 4, 5, '2015-11-01 13:34:52'),
(12, 11, 100, 4, 4, 5, '2015-10-01 13:34:52'),
(13, 14, 80, 4, 5, 5, '2015-11-01 13:34:52'),
(14, 18, 200, 4, 5, 5, '2015-11-02 14:11:21'),
(15, 19, 150, 4, 6, 5, '2015-11-03 12:22:25'),
(16, 13, 120, 4, 4, 5, '2015-11-02 15:45:27'),
(17, 16, 110, 4, 7, 5, '2015-11-04 13:47:38'),
(18, 17, 150, 4, 6, 5, '2015-11-02 16:34:17'),
(19, 18, 190, 4, 5, 5, '2015-11-01 12:32:13'),
(20, 19, 210, 4, 7, 5, '2015-11-03 17:25:00'),
(21, 13, 20, 4, 4, 5, '2015-11-02 12:32:22'),
(22, 18, 100, 4, 5, 5, '2015-11-01 13:34:52'),
(23, 20, 10, 4, 3, 5, '2015-11-03 17:20:46'),
(24, 20, 10, 4, 3, 5, '2015-11-22 13:30:37'),
(25, 20, 10, 4, 4, 5, '2015-11-25 10:21:33'),
(26, 17, 100, 4, 6, 6, '2015-12-04 09:40:59'),
(27, 11, 10, 4, 3, 6, '2015-12-06 03:30:02'),
(28, 11, 100, 4, 3, 6, '2015-12-06 04:54:39'),
(29, 11, 11, 4, 3, 4, '2015-12-09 21:36:06'),
(31, 11, 10, 4, 3, 6, '2015-12-16 01:18:30'),
(32, 18, 20, 4, 6, 5, '2015-12-16 01:23:03'),
(33, 17, 30, 4, 5, 5, '2015-12-16 01:32:42'),
(34, 16, 10, 4, 7, 6, '2015-12-16 01:33:50'),
(35, 11, 10, 4, 3, 5, '2015-12-17 04:12:59'),
(36, 25, 10, 4, 7, 5, '2015-12-17 04:13:08'),
(37, 24, 100, 4, 3, 6, '2015-12-24 04:21:39');

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE `goods` (
  `good_id` int(30) NOT NULL,
  `code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `valid` int(30) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `cat_id` int(30) NOT NULL DEFAULT '0',
  `comp_id` int(30) NOT NULL,
  `pic_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `bonus` int(30) NOT NULL,
  `discount` int(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`good_id`, `code`, `valid`, `name`, `description`, `price`, `net_price`, `cat_id`, `comp_id`, `pic_url`, `bonus`, `discount`) VALUES
(1, '12345678999', 1, 'Ручка', 'Очень красивая', 100, 70, 0, 1, '', 0, 0),
(2, '12312312312', 1, 'Тарелка', 'Не бьется', 299, 112, 0, 1, '', 0, 0),
(3, '1231245123', 1, 'Шкаф', 'Комфортный', 100000, 32100, 0, 1, '', 10, 0),
(4, '12341512312', 1, 'Нож KENZO', 'Очень острый', 500, 133, 0, 1, '', 0, 10),
(5, '12412423124', 1, 'Бензин', 'В упаковке 10 литров', 999, 121, 0, 2, '', 0, 10),
(11, '1', 1, 'Звёздные войны: Пробуждение силы (2015)', 'Через тридцать лет после гибели Дарта Вейдера и Императора галактика по-прежнему в опасности. Государственное образование Новый Порядок во главе с их таинственным верховным лидером Сноуком и его правой рукой Кайло Реном идёт по стопам Империи, пытаясь захватить всю власть. В это нелёгкое время судьба сводит юную девушку Рей и бывшего штурмовика Нового Порядка Финна с героями времён войны с Империей — Ханом Соло, Чубаккой и Королевой Леей. Вместе они должны дать бой Новому Порядку, однако настаёт тот момент, когда становится очевидно, что лишь джедаи могут остановить Сноука и Кайло Рена. И в галактике в живых остаётся только один…', 800, 197, 11, 4, '', 0, 0),
(12, '2', 1, '1+1 (2011)', 'Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники человека, который менее всего подходит для этой работы, — молодого жителя предместья Дрисса, только что освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается привнести в размеренную жизнь аристократа дух приключений.', 500, 127, 6, 4, '', 0, 0),
(13, '4', 1, 'Начало (2010)', 'Кобб — талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что он когда-либо любил. ', 500, 147, 4, 4, '', 0, 10),
(14, '5', 1, 'Интерстеллар (2014)', 'Когда засуха приводит человечество к продовольственному кризису, коллектив исследователей и учёных отправляется сквозь червоточину (которая предположительно соединяет области пространства-времени через большое расстояние) в путешествие, чтобы превзойти прежние ограничения для космических путешествий человека и переселить человечество на другую планету.', 650, 147, 11, 4, '', 10, 10),
(15, '6', 1, 'Крестный отец (1972)', 'Криминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне. Фильм охватывает период 1945-1955 годов. Глава семьи, Дон Вито Корлеоне, выдаёт замуж свою дочь. В это время со Второй мировой войны возвращается его любимый сын Майкл. Майкл, герой войны, гордость семьи, не выражает желания заняться жестоким семейным бизнесом. Дон Корлеоне ведёт дела по старым правилам, но наступают иные времена, и появляются люди, желающие изменить сложившиеся порядки. На Дона Корлеоне совершается покушение.', 700, 342, 9, 4, '', 0, 50),
(16, '7', 1, 'Шерлок Холмс (2009)', 'Величайший в истории сыщик Шерлок Холмс вместе со своим верным соратником Ватсоном вступают в схватку, требующую нешуточной физической и умственной подготовки, ведь их враг представляет угрозу для всего Лондона.', 1000, 500, 3, 4, '', 0, 0),
(17, '8', 1, 'Ип Ман (2008)', 'Ип Ман — признанный мастер кунг фу, живущий в Фошане, городе, славном своими школами боевых искусств. Ип Ман практикует Вин Чун и хотя является сильнейшим бойцом города, своей школы у него нет, и он не берет учеников. Дома он проводит лишь легкие спарринги с приятелями, чтобы указать на ошибки друг друга. Проходят годы, Китай захватывают воинственные японцы, и Ип Ман оказывается одним из немногих, кто даже в жесточайших условиях оккупации не забывает о чести, достоинстве и, конечно же, мудрости, которую несет в себе кунг-фу.', 1500, 721, 4, 4, '', 0, 20),
(18, '9', 1, 'Гарри Поттер и философский камень (2001)', 'Жизнь десятилетнего Гарри Поттера нельзя назвать сладкой: его родители умерли, едва ему исполнился год, а от дяди и тётки, взявших сироту на воспитание, достаются лишь тычки да подзатыльники. Но в одиннадцатый день рождения Гарри всё меняется. Странный гость, неожиданно появившийся на пороге, приносит письмо, из которого мальчик узнаёт, что на самом деле он волшебник и принят в Хогвартс — школу магии. А уже через пару недель Гарри будет мчаться в поезде Хогвартс-экспресс навстречу новой жизни, где его ждут невероятные приключения, верные друзья и самое главное — ключ к разгадке тайны смерти его родителей.', 1300, 612, 11, 4, '', 20, 0),
(19, '10', 1, 'Гарри Поттер и узник Азкабана (2004)', 'В третьей части истории о юном волшебнике полюбившиеся всем герои — Гарри Поттер, Рон и Гермиона — возвращаются уже на третий курс школы чародейства и волшебства Хогвартс. На этот раз они должны раскрыть тайну узника, сбежавшего из зловещей тюрьмы Азкабан, чье пребывание на воле создает для Гарри смертельную опасность…', 1100, 552, 11, 4, '', 20, 0),
(20, '20', 1, 'iPhone 6s', 'iPhone 6s и iPhone 6s Plus — смартфоны корпорации Apple, работающие на iOS 9, представленные 9 сентября 2015 года. Смартфоны представляют собой девятое поколение iPhone.', 250000, 100000, 13, 4, 'images/iphone_6s.png', 0, 0),
(25, '12hbjhb23h43', 1, 'Бэтман 2 ', 'История...  тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест', 1500, 500, 6, 4, 'images/goods/good_25.png', 10, 0),
(24, '1231241312', 1, 'Домик в деревне 2', 'Это тоже тестовый фильм. Не обращайте внимания', 1000, 0, 4, 4, 'images/goods/good_24.png', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `history`
--

CREATE TABLE `history` (
  `history_id` int(30) NOT NULL,
  `emp_id` int(30) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `history_date` datetime NOT NULL,
  `history_text` text NOT NULL,
  `history_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `history`
--

INSERT INTO `history` (`history_id`, `emp_id`, `comp_id`, `history_date`, `history_text`, `history_type`) VALUES
(2, 6, 4, '2015-12-16 01:18:30', 'Оприходавано 10 штук товара "Звёздные войны: Пробуждение силы (2015)" на склад Меломан 1', 'Оприходование'),
(3, 5, 4, '2015-12-16 01:23:03', 'Оприходавано 20 штук товара "Гарри Поттер и философский камень (2001)" на склад "Меломан 4"', 'Оприходование'),
(4, 5, 4, '2015-12-16 01:32:42', 'Оприходавано 30 штук товара "Ип Ман (2008)" на склад "Меломан 3"', 'Оприходование'),
(5, 6, 4, '2015-12-16 01:33:50', 'Оприходавано 10 штук товара "Шерлок Холмс (2009)" на склад "Меломан 5"', 'Оприходование'),
(6, 6, 4, '2015-12-16 01:34:08', 'Продано 1 штук товара "Шерлок Холмс (2009)" на склад "Меломан 5"', 'Продажа'),
(8, 6, 4, '2015-12-16 01:34:47', 'Возврат 1 штук товара "Шерлок Холмс (2009)" на склад "Меломан 5"', 'Возврат'),
(9, 6, 4, '2015-12-16 01:34:59', 'Перемещено 5 штук товара "Шерлок Холмс (2009)" со склада "Меломан 5" на склад "Меломан 4"', 'Перемещение'),
(10, 6, 4, '2015-12-16 01:46:38', ' Пользователь зашел в веб-сайт ', 'Вход'),
(11, 6, 4, '2015-12-16 03:09:42', ' Пользователь вышел с веб-сайта ', 'Выход'),
(12, 1, 1, '2015-12-16 03:10:06', ' Пользователь вышел с веб-сайта ', 'Выход'),
(13, 1, 1, '2015-12-16 03:10:18', ' Пользователь вышел с веб-сайта ', 'Выход'),
(14, 1, 1, '2015-12-16 03:10:22', ' Пользователь вышел с веб-сайта ', 'Выход'),
(15, 1, 1, '2015-12-16 03:10:54', ' Пользователь вышел с веб-сайта ', 'Выход'),
(16, 1, 1, '2015-12-16 03:10:55', ' Пользователь вышел с веб-сайта ', 'Выход'),
(17, 1, 1, '2015-12-16 03:11:17', ' Пользователь вышел с веб-сайта ', 'Выход'),
(18, 1, 1, '2015-12-16 03:11:18', ' Пользователь зашел в веб-сайт ', 'Вход'),
(19, 1, 1, '2015-12-16 03:11:23', ' Пользователь вышел с веб-сайта ', 'Выход'),
(20, 6, 4, '2015-12-16 03:11:33', ' Пользователь зашел в веб-сайт ', 'Вход'),
(21, 6, 4, '2015-12-16 03:11:43', ' Пользователь вышел с веб-сайта ', 'Выход'),
(22, 6, 4, '2015-12-16 03:11:51', ' Пользователь зашел в веб-сайт ', 'Вход'),
(23, 6, 4, '2015-12-16 20:02:09', 'Продано 1 штук товара "Звёздные войны: Пробуждение силы (2015)" в магазине "Меломан 1"', 'Продажа'),
(24, 6, 4, '2015-12-17 04:07:58', 'Добавлен новый товар. \r\n		    		Категория: "Боевики",\r\n		    		Штрих-код: "1231241312", \r\n		    		Название: "Домик в деревне 2",\r\n					Описание: "Это тоже тестовый фильм. Не обращайте внимания",\r\n					Цена: "1000",\r\n					Себестоимость: "0",\r\n					Бонус: "0%",\r\n					Скидка: "0%".', 'Новый товар'),
(25, 6, 4, '2015-12-17 04:10:06', ' Пользователь вышел с веб-сайта ', 'Выход'),
(26, 5, 4, '2015-12-17 04:10:16', ' Пользователь зашел в веб-сайт ', 'Вход'),
(27, 5, 4, '2015-12-17 04:12:08', 'Добавлен новый товар. \r\n		    		Категория: "Комедии",\r\n		    		Штрих-код: "12hbjhb23h43", \r\n		    		Название: "Бэтман 2 ",\r\n					Описание: "История...  тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест",\r\n					Цена: "1500",\r\n					Себестоимость: "500",\r\n					Бонус: "10%",\r\n					Скидка: "0%".', 'Новый товар'),
(28, 5, 4, '2015-12-17 04:12:59', 'Оприходавано 10 штук товара "Звёздные войны: Пробуждение силы (2015)" на склад "Меломан 1"', 'Оприходование'),
(29, 5, 4, '2015-12-17 04:13:08', 'Оприходавано 10 штук товара "Бэтман 2 " на склад "Меломан 5"', 'Оприходование'),
(30, 5, 4, '2015-12-17 04:13:56', 'Продано 2 штук товара "Бэтман 2 " в магазине "Меломан 5"', 'Продажа'),
(31, 5, 4, '2015-12-17 21:31:13', ' Пользователь вышел с веб-сайта ', 'Выход'),
(32, 6, 4, '2015-12-24 03:28:05', ' Пользователь зашел в веб-сайт ', 'Вход'),
(33, 6, 4, '2015-12-24 04:21:39', 'Оприходавано 100 штук товара "Домик в деревне 2" на склад "Меломан 1"', 'Оприходование'),
(34, 6, 4, '2015-12-24 04:22:08', 'Продано 1 штук товара "Домик в деревне 2" в магазине "Меломан 1"', 'Продажа'),
(35, 6, 4, '2015-12-24 05:22:16', ' Пользователь вышел с веб-сайта ', 'Выход'),
(36, 6, 4, '2015-12-24 05:22:22', ' Пользователь зашел в веб-сайт ', 'Вход'),
(37, 6, 4, '2015-12-24 06:55:24', ' Пользователь зашел в веб-сайт ', 'Вход');

-- --------------------------------------------------------

--
-- Структура таблицы `moves`
--

CREATE TABLE `moves` (
  `move_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `from_store_id` int(30) NOT NULL,
  `to_store_id` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `move_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `moves`
--

INSERT INTO `moves` (`move_id`, `good_id`, `comp_id`, `from_store_id`, `to_store_id`, `count`, `employee_id`, `move_date`) VALUES
(1, 11, 4, 3, 4, 1, 6, '2015-12-04 11:26:33'),
(2, 11, 4, 3, 4, 1, 6, '2015-12-04 11:32:18'),
(3, 16, 4, 7, 6, 5, 6, '2015-12-16 01:34:59');

-- --------------------------------------------------------

--
-- Структура таблицы `net_prices`
--

CREATE TABLE `net_prices` (
  `net_price_id` int(30) NOT NULL,
  `from_date` datetime NOT NULL,
  `net_price` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `net_prices`
--

INSERT INTO `net_prices` (`net_price_id`, `from_date`, `net_price`, `good_id`, `comp_id`) VALUES
(1, '2015-11-20 08:00:00', 170000, 20, 4),
(2, '2015-11-23 08:00:00', 180000, 20, 4);

-- --------------------------------------------------------

--
-- Структура таблицы `returns`
--

CREATE TABLE `returns` (
  `return_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `client_id` int(30) NOT NULL,
  `return_date` datetime NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `discount` int(30) NOT NULL,
  `bonus` int(30) NOT NULL,
  `reason` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `returns`
--

INSERT INTO `returns` (`return_id`, `good_id`, `comp_id`, `store_id`, `employee_id`, `client_id`, `return_date`, `price`, `net_price`, `count`, `discount`, `bonus`, `reason`) VALUES
(2, 11, 4, 3, 6, 0, '2015-12-06 04:12:39', 800, 0, 5, 0, 0, 'Брак'),
(4, 16, 4, 7, 6, 0, '2015-12-16 01:34:47', 1000, 0, 1, 0, 0, 'Брак');

-- --------------------------------------------------------

--
-- Структура таблицы `sells`
--

CREATE TABLE `sells` (
  `sell_id` int(30) NOT NULL,
  `good_id` int(30) NOT NULL,
  `comp_id` int(30) NOT NULL,
  `store_id` int(30) NOT NULL,
  `employee_id` int(30) NOT NULL,
  `client_id` int(30) NOT NULL,
  `sell_date` datetime NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `count` int(30) NOT NULL,
  `discount` int(30) NOT NULL,
  `bonus` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sells`
--

INSERT INTO `sells` (`sell_id`, `good_id`, `comp_id`, `store_id`, `employee_id`, `client_id`, `sell_date`, `price`, `net_price`, `count`, `discount`, `bonus`) VALUES
(27, 11, 4, 3, 4, 2, '2015-11-21 03:10:23', 800, 197, 2, 0, 0),
(28, 11, 4, 4, 4, 2, '2015-11-20 19:20:53', 800, 197, 1, 0, 0),
(29, 12, 4, 4, 4, 2, '2015-11-19 16:34:35', 500, 127, 2, 0, 0),
(30, 11, 4, 3, 4, 2, '2015-11-12 15:26:23', 800, 197, 1, 0, 0),
(31, 11, 4, 3, 4, 2, '2015-10-10 11:10:22', 800, 197, 5, 0, 0),
(32, 20, 4, 3, 4, 0, '2015-11-13 13:10:23', 150000, 100000, 2, 0, 0),
(33, 20, 4, 3, 4, 0, '2015-11-26 10:27:35', 250000, 100000, 12, 0, 0),
(34, 20, 4, 3, 4, 0, '2015-11-28 12:23:28', 250000, 100000, 5, 0, 0),
(35, 20, 4, 4, 4, 0, '2015-11-23 10:30:34', 250000, 100000, 8, 0, 0),
(36, 11, 4, 3, 6, 2, '2015-12-04 12:44:11', 800, 0, 1, 0, 0),
(37, 11, 4, 3, 6, 0, '2015-12-04 12:44:27', 800, 0, 12, 0, 0),
(38, 11, 4, 3, 4, 0, '2015-12-05 21:02:16', 800, 0, 10, 0, 0),
(39, 11, 4, 3, 6, 0, '2015-12-06 02:45:31', 800, 0, 10, 0, 0),
(40, 11, 4, 3, 6, 0, '2015-12-06 03:30:51', 800, 0, 10, 0, 0),
(41, 16, 4, 7, 6, 0, '2015-12-16 01:34:08', 1000, 0, 1, 0, 0),
(42, 11, 4, 3, 6, 0, '2015-12-16 20:02:09', 800, 0, 1, 0, 0),
(43, 25, 4, 7, 5, 0, '2015-12-17 04:13:56', 1500, 0, 2, 0, 10),
(44, 24, 4, 3, 6, 0, '2015-12-24 04:22:08', 1000, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `stores`
--

CREATE TABLE `stores` (
  `store_id` int(30) NOT NULL,
  `pic_url` varchar(200) NOT NULL,
  `name` varchar(30) NOT NULL,
  `address` varchar(30) NOT NULL,
  `comp_id` int(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `stores`
--

INSERT INTO `stores` (`store_id`, `pic_url`, `name`, `address`, `comp_id`) VALUES
(1, '', 'Рита', 'Turgut Ozala 70', 1),
(2, '', 'Светоч', 'Tole bi 70', 1),
(3, 'images\\meloman01.png', 'Меломан 1', 'Тургут Озала 70', 4),
(4, 'images\\meloman02.png', 'Меломан 2', 'Тургут Озала 80', 4),
(5, 'images\\meloman03.png', 'Меломан 3', 'Тургут Озала 90', 4),
(6, '', 'Меломан 4', 'Тургут Озала 83', 4),
(7, '', 'Меломан 5', 'Тургут Озала 82', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(30) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `patronymic` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pic_url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `info` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `job` int(30) NOT NULL DEFAULT '0',
  `comp_id` int(30) NOT NULL,
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `patronymic`, `pic_url`, `gender`, `info`, `type`, `job`, `comp_id`, `email`, `password`) VALUES
(1, 'Аманкос', 'Рахымбергенов', 'Канатович', '', 'M', 'Mobile Developer', 'seller', 0, 1, 'amankosss@gmail.com', 'A555555a'),
(2, 'Елена', 'Сергеева', '', '', 'F', 'Бухгалтер', 'counter', 15, 2, 'elena@gmail.com', 'A555555a'),
(5, 'Елена', 'Смирнова', 'Олеговна', NULL, 'F', 'Активистка компании, высшее образование...', 'keeper', 2, 4, 'keeper_meloman@gmail.com', 'A555555a'),
(4, 'Иван', 'Иванов', 'Иванович', NULL, 'M', 'Образованный, пунктуальный ...', 'seller', 1, 4, 'seller_meloman@gmail.com', 'A555555a'),
(6, 'Аманкос', 'Рахымбергенов', 'Канатович', 'images/amankos_ava.jpg', 'M', 'Выпускник КБТУ, ответственный , пунктуальный...', 'admin', 255, 4, 'admin_meloman@gmail.com', 'A555555a');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Индексы таблицы `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Индексы таблицы `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`comp_id`);

--
-- Индексы таблицы `debits`
--
ALTER TABLE `debits`
  ADD PRIMARY KEY (`debit_id`);

--
-- Индексы таблицы `goods`
--
ALTER TABLE `goods`
  ADD PRIMARY KEY (`good_id`),
  ADD UNIQUE KEY `id` (`good_id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`);

--
-- Индексы таблицы `moves`
--
ALTER TABLE `moves`
  ADD PRIMARY KEY (`move_id`);

--
-- Индексы таблицы `net_prices`
--
ALTER TABLE `net_prices`
  ADD PRIMARY KEY (`net_price_id`);

--
-- Индексы таблицы `returns`
--
ALTER TABLE `returns`
  ADD PRIMARY KEY (`return_id`);

--
-- Индексы таблицы `sells`
--
ALTER TABLE `sells`
  ADD PRIMARY KEY (`sell_id`);

--
-- Индексы таблицы `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `companies`
--
ALTER TABLE `companies`
  MODIFY `comp_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `debits`
--
ALTER TABLE `debits`
  MODIFY `debit_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `goods`
--
ALTER TABLE `goods`
  MODIFY `good_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `moves`
--
ALTER TABLE `moves`
  MODIFY `move_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `net_prices`
--
ALTER TABLE `net_prices`
  MODIFY `net_price_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `returns`
--
ALTER TABLE `returns`
  MODIFY `return_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `sells`
--
ALTER TABLE `sells`
  MODIFY `sell_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблицы `stores`
--
ALTER TABLE `stores`
  MODIFY `store_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
