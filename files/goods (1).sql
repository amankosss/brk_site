
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Дек 28 2015 г., 09:24
-- Версия сервера: 10.0.20-MariaDB
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `u832858559_brk01`
--

-- --------------------------------------------------------

--
-- Структура таблицы `goods`
--

CREATE TABLE IF NOT EXISTS `goods` (
  `good_id` int(30) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `valid` int(30) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(30) NOT NULL,
  `net_price` int(30) NOT NULL,
  `cat_id` int(30) NOT NULL DEFAULT '0',
  `comp_id` int(30) NOT NULL,
  `pic_url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `bonus` int(30) NOT NULL,
  `discount` int(30) NOT NULL,
  PRIMARY KEY (`good_id`),
  UNIQUE KEY `id` (`good_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Дамп данных таблицы `goods`
--

INSERT INTO `goods` (`good_id`, `code`, `valid`, `name`, `description`, `price`, `net_price`, `cat_id`, `comp_id`, `pic_url`, `bonus`, `discount`) VALUES
(1, '12345678999', 1, 'Ручка', 'Очень красивая', 100, 70, 0, 1, '', 0, 0),
(2, '12312312312', 1, 'Тарелка', 'Не бьется', 299, 112, 0, 1, '', 0, 0),
(3, '1231245123', 1, 'Шкаф', 'Комфортный', 100000, 32100, 0, 1, '', 10, 0),
(4, '12341512312', 1, 'Нож KENZO', 'Очень острый', 500, 133, 0, 1, '', 0, 10),
(5, '12412423124', 1, 'Бензин', 'В упаковке 10 литров', 999, 121, 0, 2, '', 0, 10),
(11, '1', 1, 'Звёздные войны: Пробуждение силы (2015)', 'Через тридцать лет после гибели Дарта Вейдера и Императора галактика по-прежнему в опасности. Государственное образование Новый Порядок во главе с их таинственным верховным лидером Сноуком и его правой рукой Кайло Реном идёт по стопам Империи, пытаясь захватить всю власть. В это нелёгкое время судьба сводит юную девушку Рей и бывшего штурмовика Нового Порядка Финна с героями времён войны с Империей — Ханом Соло, Чубаккой и Королевой Леей. Вместе они должны дать бой Новому Порядку, однако настаёт тот момент, когда становится очевидно, что лишь джедаи могут остановить Сноука и Кайло Рена. И в галактике в живых остаётся только один…', 800, 197, 11, 4, '', 0, 0),
(12, '2', 1, '1+1 (2011)', 'Пострадав в результате несчастного случая, богатый аристократ Филипп нанимает в помощники человека, который менее всего подходит для этой работы, — молодого жителя предместья Дрисса, только что освободившегося из тюрьмы. Несмотря на то, что Филипп прикован к инвалидному креслу, Дриссу удается привнести в размеренную жизнь аристократа дух приключений.', 500, 127, 6, 4, '', 0, 0),
(13, '4', 1, 'Начало (2010)', 'Кобб — талантливый вор, лучший из лучших в опасном искусстве извлечения: он крадет ценные секреты из глубин подсознания во время сна, когда человеческий разум наиболее уязвим. Редкие способности Кобба сделали его ценным игроком в привычном к предательству мире промышленного шпионажа, но они же превратили его в извечного беглеца и лишили всего, что он когда-либо любил. ', 500, 147, 4, 4, '', 0, 10),
(14, '5', 1, 'Интерстеллар (2014)', 'Когда засуха приводит человечество к продовольственному кризису, коллектив исследователей и учёных отправляется сквозь червоточину (которая предположительно соединяет области пространства-времени через большое расстояние) в путешествие, чтобы превзойти прежние ограничения для космических путешествий человека и переселить человечество на другую планету.', 650, 147, 11, 4, '', 10, 10),
(15, '6', 1, 'Крестный отец (1972)', 'Криминальная сага, повествующая о нью-йоркской сицилийской мафиозной семье Корлеоне. Фильм охватывает период 1945-1955 годов. Глава семьи, Дон Вито Корлеоне, выдаёт замуж свою дочь. В это время со Второй мировой войны возвращается его любимый сын Майкл. Майкл, герой войны, гордость семьи, не выражает желания заняться жестоким семейным бизнесом. Дон Корлеоне ведёт дела по старым правилам, но наступают иные времена, и появляются люди, желающие изменить сложившиеся порядки. На Дона Корлеоне совершается покушение.', 700, 342, 9, 4, '', 0, 50),
(16, '7', 1, 'Шерлок Холмс (2009)', 'Величайший в истории сыщик Шерлок Холмс вместе со своим верным соратником Ватсоном вступают в схватку, требующую нешуточной физической и умственной подготовки, ведь их враг представляет угрозу для всего Лондона.', 1000, 500, 3, 4, '', 0, 0),
(17, '8', 1, 'Ип Ман (2008)', 'Ип Ман — признанный мастер кунг фу, живущий в Фошане, городе, славном своими школами боевых искусств. Ип Ман практикует Вин Чун и хотя является сильнейшим бойцом города, своей школы у него нет, и он не берет учеников. Дома он проводит лишь легкие спарринги с приятелями, чтобы указать на ошибки друг друга. Проходят годы, Китай захватывают воинственные японцы, и Ип Ман оказывается одним из немногих, кто даже в жесточайших условиях оккупации не забывает о чести, достоинстве и, конечно же, мудрости, которую несет в себе кунг-фу.', 1500, 721, 4, 4, '', 0, 20),
(18, '9', 1, 'Гарри Поттер и философский камень (2001)', 'Жизнь десятилетнего Гарри Поттера нельзя назвать сладкой: его родители умерли, едва ему исполнился год, а от дяди и тётки, взявших сироту на воспитание, достаются лишь тычки да подзатыльники. Но в одиннадцатый день рождения Гарри всё меняется. Странный гость, неожиданно появившийся на пороге, приносит письмо, из которого мальчик узнаёт, что на самом деле он волшебник и принят в Хогвартс — школу магии. А уже через пару недель Гарри будет мчаться в поезде Хогвартс-экспресс навстречу новой жизни, где его ждут невероятные приключения, верные друзья и самое главное — ключ к разгадке тайны смерти его родителей.', 1300, 612, 11, 4, '', 20, 0),
(19, '10', 1, 'Гарри Поттер и узник Азкабана (2004)', 'В третьей части истории о юном волшебнике полюбившиеся всем герои — Гарри Поттер, Рон и Гермиона — возвращаются уже на третий курс школы чародейства и волшебства Хогвартс. На этот раз они должны раскрыть тайну узника, сбежавшего из зловещей тюрьмы Азкабан, чье пребывание на воле создает для Гарри смертельную опасность…', 1100, 552, 11, 4, '', 20, 0),
(20, '20', 1, 'iPhone 6s', 'iPhone 6s и iPhone 6s Plus — смартфоны корпорации Apple, работающие на iOS 9, представленные 9 сентября 2015 года. Смартфоны представляют собой девятое поколение iPhone.', 250000, 100000, 13, 4, 'images/iphone_6s.png', 0, 0),
(25, '12hbjhb23h43', 1, 'Бэтман 2 ', 'История...  тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест тест', 1500, 500, 6, 4, 'images/goods/good_25.png', 10, 0),
(24, '1231241312', 1, 'Домик в деревне 2', 'Это тоже тестовый фильм. Не обращайте внимания', 1000, 0, 4, 4, 'images/goods/good_24.png', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
