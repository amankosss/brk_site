<?php 
include_once '../../config.php';
//get variable $conn
setConnection();
//set meta and other important moments
addImportantMoments();

$user_id = (int)$_GET['id'];
$user_password = $_GET['password'];
$user_comp_id = (int)$_GET['comp_id'];
// if user is valid return $is_user_valid = 1
checkUser($user_id,$user_password,$conn);
// exit if not valid user
if($is_valid_user==0) exit();

$goods_arr = Array();
$result = $conn->query("SELECT * FROM  `goods` WHERE `comp_id` = '$user_comp_id'");
if ($result->num_rows > 0) { //take all goods of company 
    while ($row = $result->fetch_assoc()) {
        array_push($goods_arr,$row);
    }//return it by [json]
    echo json_encode($goods_arr,JSON_UNESCAPED_UNICODE);   
} 
else{
    echo '{"message_show":"Товаров на сервере не обнаружено"}';
}
?>