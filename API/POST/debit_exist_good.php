<?php 
include_once '../../config.php';
//set meta and other important moments
addImportantMoments();

$user_id = (int)$_POST['id'];
$user_password = $_POST['password'];
$user_comp_id = (int)$_POST['comp_id'];
$user_store_id = (int)$_POST['store_id'];
$code = $_POST['code'];
$count = (int)$_POST['count'];

// if user is valid return $is_user_valid = 1
checkUser($user_id,$user_password,$conn);
// exit if not valid user
if($is_valid_user==0) exit();
// goods which are not exist
$not_valid_arr = Array();

$result = $conn->query("SELECT * FROM  goods WHERE comp_id = $user_comp_id AND store_id = $user_store_id");
if ($result->num_rows > 0) {
    $bool = 0;
    $cur_good;
    while ($row = $result->fetch_assoc()) {
        if($row['code'] == $code){
            $bool = 1;
            $cur_good = $row;
            break;
        }
    }
    if($bool == 0){
        echo '{"error_show":"Товар на сервере отсутствует"}';        
    }
    else if((int)$count<1){
        echo '{"error_show":"Не правильно введен количество товара"}';                
    }
    else{
        $new_count = $count + (int)$cur_good['count'];
        $cur_id = (int)$cur_good['id'];
        $sql = "UPDATE goods SET count = count + $count 
            WHERE comp_id = $user_comp_id AND store_id = $user_store_id AND id = $cur_id";
        if ($conn->query($sql) === TRUE) {//count of good updated
            echo '{"message_show":"Товар успешно оприходован"}';     
        } else {// WE MUST FIX ERROR HERE!!! NOT FORGET!!!
                echo '{"error_show":"Очень жаль но не удалось оприходовать товар, попробуйте еще раз!"}';        
        }
    }
}
else{
    echo '{"message_show":"Товаров на сервере не обнаружено"}';
}
?>