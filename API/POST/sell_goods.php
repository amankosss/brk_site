<?php 
include_once '../../config.php';
//get variable $conn
setConnection();
//set meta and other important moments
addImportantMoments();

$user_id = (int)$_POST['id'];
$user_password = $_POST['password'];
$user_comp_id = (int)$_POST['comp_id'];
$user_store_id = (int)$_POST['store_id'];
$user_client_id = (int)$_POST['client_id'];
$code = (Array)$_POST['code'];
$count = (Array)$_POST['count'];

// if user is valid return $is_user_valid = 1
checkUser($user_id,$user_password,$conn);
// exit if not valid user
if($is_valid_user==0) exit();
// goods which are existing and we can sell(valid count)
$valid_arr = Array();
// goods which we cannot sell
$not_valid_arr = Array();
// counts which we must set
$new_count = Array();
$result = $conn->query("SELECT * FROM  goods WHERE comp_id = $user_comp_id AND store_id = $user_store_id");
if ($result->num_rows > 0) {
    for($i=0;$i<count($code);$i++){//add valid goods to sell to $valid_arr
        $bool = 0;
        //WE MUST FIX IT !!! DON'T FORGET DEVELOPER
        $res = $conn->query("SELECT * FROM  goods WHERE comp_id = $user_comp_id AND store_id = $user_store_id");
        while ($row = $res->fetch_assoc()) {
            if($row['code'] == $code[$i]){
                if((int)$row['count'] >= (int)$count[$i]){
                    array_push($valid_arr,$row);
                    array_push($new_count,((int)$row['count']-(int)$count[$i]));
                }
                else {
                    array_push($not_valid_arr, $row['code']);
                }
                $bool = 1;
                break;
            }
        }
        if($bool == 0){
            array_push($not_valid_arr,$code[$i]);    
        }
    }

    for($i=0 ; $i<count($valid_arr);$i++){//update all goods which are sold
        $cur_id = (int)$valid_arr[$i]['id'];
        $cur_code = $valid_arr[$i]['code'];
        $old_count = (int)$valid_arr[$i]['count'];
        $cur_price = (int)$valid_arr[$i]['price'];
        $cur_net_price = (int)$valid_arr[$i]['net_price'];
        $cur_discount = (int)$valid_arr[$i]['discount'];
        $cur_bonus = (int)$valid_arr[$i]['bonus'];
        $dif_count = (int)$valid_arr[$i]['count'] - (int)$new_count[$i];
        $cur_new_count = (int)$new_count[$i];
        /*
        echo "cur_code : $cur_code <br> comp_id: $user_comp_id <br> 
            store id: $user_store_id<br> client_id: $user_client_id <br> 
                date: ".date('d-m-y'). " <br> time: ".date('H:i:s'). "<br> 
                price: $cur_price <br> net_price: $cur_net_price <br> 
                dif_count: $dif_count <br> discount: $cur_discount <br>
                bonus: $cur_bonus <br>";
        */
        $sql = "UPDATE goods SET count = $cur_new_count 
            WHERE comp_id = $user_comp_id AND store_id = $user_store_id AND id = $cur_id";
        
        if ($conn->query($sql) === TRUE) {
            //count of good updated
            $sql = "INSERT INTO sells (code ,comp_id,store_id,client_id,sell_date,sell_time,
                    price,net_price,count,discount,bonus) 
                VALUES ('$cur_code', $user_comp_id,$user_store_id,$user_client_id,"."'".date("Y-m-d") . "'," . 
                    "'".date("H:i:s")."'," ."$cur_price,$cur_net_price,$dif_count,$cur_discount,$cur_bonus)";
            if ($conn->query($sql) === TRUE) {
            } else {
                // WE MUST FIX ERROR HERE!!! NOT FORGET!!!
            }
        } else {//not update
            array_push($not_valid_arr,$row['code']);    
        }    
    }
    if(count($not_valid_arr)==0){
        echo '{"message_show":"Все товары успешно проданы"}';
        exit();
    }
    echo json_encode($not_valid_arr,JSON_UNESCAPED_UNICODE);
} 
else{
    echo '{"message_show":"Товаров на сервере не обнаружено"}';
}
?>