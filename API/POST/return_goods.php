<?php 
include_once '../../config.php';
//get variable $conn
setConnection();
//set meta and other important moments
addImportantMoments();

$user_id = (int)$_POST['id'];
$user_password = $_POST['password'];
$user_comp_id = (int)$_POST['comp_id'];
$user_store_id = (int)$_POST['store_id'];
$user_client_id = (int)$_POST['client_id'];
$code = (Array)$_POST['code'];
$count = (Array)$_POST['count'];
$user_reason = $_POST['reason'];
// if user is valid return $is_user_valid = 1
checkUser($user_id,$user_password,$conn);
// exit if not valid user
if($is_valid_user==0) exit();
// goods which we cannot sell
$not_valid_arr = Array();
$result = $conn->query("SELECT * FROM  goods WHERE comp_id = $user_comp_id AND store_id = $user_store_id");
if ($result->num_rows > 0) {
    for($i=0;$i<count($code);$i++){//add valid goods to sell to $valid_arr
        $bool = 0;
        $cur_good ;
        //WE MUST OPTIMIZE THIS MOMENT ! DON'T FORGET !!!
        $res = $conn->query("SELECT * FROM  goods WHERE comp_id = $user_comp_id AND store_id = $user_store_id");
        while ($row = $res->fetch_assoc()) {
            if($row['code'] == $code[$i]){
                $bool = 1;
                $cur_good =$row;
                break;
            }
        }
        if($bool == 0 && $count[$i]<1){
            array_push($not_valid_arr,$code[$i]);    
        }
        else{
            $cur_id = (int)$cur_good['id'];
            $cur_code = $cur_good['code'];
            $old_count = (int)$cur_good['count'];
            $cur_price = (int)$cur_good['price'];
            $cur_net_price = (int)$cur_good['net_price'];
            $cur_discount = (int)$cur_good['discount'];
            $cur_bonus = (int)$cur_good['bonus'];
            $return_count = (int)$count[$i];
            $sql = "INSERT INTO returns (code ,comp_id,store_id,client_id,return_date,return_time,
                    price,net_price,count,discount,bonus,reason) 
                    VALUES ('$cur_code', $user_comp_id,$user_store_id,$user_client_id,"."'".date("Y-m-d") . "'," . 
                    "'".date("H:i:s")."'," ."$cur_price,$cur_net_price,$return_count,$cur_discount,$cur_bonus,'$user_reason')";
            //echo $sql;
            if ($conn->query($sql) === TRUE) {
                //RETURNED
            } else {// WE MUST FIX ERROR HERE!!! NOT FORGET!!!
                array_push($not_valid_arr,$row['code']);    
            }
        }
    }
    if(count($not_valid_arr)==0){
        echo '{"message_show":"Все товары успешно возвращены"}';
        exit();
    }
    echo json_encode($not_valid_arr,JSON_UNESCAPED_UNICODE);
} 
else{
    echo '{"message_show":"Товаров на сервере не обнаружено"}';
}


?>