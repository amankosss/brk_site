<?php ob_start(); ?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead(); 
		checkPostAddFlowCategory();
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php addFlowCategoryFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>