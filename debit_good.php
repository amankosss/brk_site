<?php ob_start(); ?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead();
		checkPostDebit();
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php debitGoodFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>