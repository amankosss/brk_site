<?php
$JOB = array (
  'act' => 'restore',
  'type' => 'run',
  'db' => 'bhelp',
  'charset' => 'utf8',
  'file' => 'bhelp_2016-09-28_10-13-14.sql.gz',
  'strategy' => '1',
  'correct' => '0',
  'autoinc' => '0',
  'obj' => 
  array (
    'TA' => 
    array (
      0 => '*',
    ),
  ),
  'job' => 'ln0f5Sy1',
  'file_ext' => 'sql.gz',
  'file_name' => 'backup/bhelp_2016-09-28_10-13-14.sql.gz',
  'file_tmp' => 'backup/bhelp_2016-09-28_10-13-14.sql.gz',
  'file_rtl' => 'backup/ln0f5Sy1.rtl',
  'file_log' => 'backup/ln0f5Sy1.log',
  'file_stp' => 'backup/ln0f5Sy1.stp',
  'todo' => 
  array (
    'TA' => 
    array (
      0 => 
      array (
        0 => 'TA',
        1 => 'cashflow_types',
        2 => '30',
        3 => '16384',
      ),
      1 => 
      array (
        0 => 'TA',
        1 => 'cashflows',
        2 => '195',
        3 => '16384',
      ),
      2 => 
      array (
        0 => 'TA',
        1 => 'categories',
        2 => '6',
        3 => '16384',
      ),
      3 => 
      array (
        0 => 'TC',
        1 => 'clients',
        2 => '0',
        3 => '16384',
      ),
      4 => 
      array (
        0 => 'TA',
        1 => 'companies',
        2 => '5',
        3 => '16384',
      ),
      5 => 
      array (
        0 => 'TA',
        1 => 'debits',
        2 => '1078',
        3 => '98304',
      ),
      6 => 
      array (
        0 => 'TA',
        1 => 'goods',
        2 => '996',
        3 => '245760',
      ),
      7 => 
      array (
        0 => 'TA',
        1 => 'history',
        2 => '2253',
        3 => '1589248',
      ),
      8 => 
      array (
        0 => 'TA',
        1 => 'moves',
        2 => '89',
        3 => '16384',
      ),
      9 => 
      array (
        0 => 'TC',
        1 => 'net_prices',
        2 => '0',
        3 => '16384',
      ),
      10 => 
      array (
        0 => 'TC',
        1 => 'returns',
        2 => '0',
        3 => '16384',
      ),
      11 => 
      array (
        0 => 'TA',
        1 => 'sells',
        2 => '400',
        3 => '65536',
      ),
      12 => 
      array (
        0 => 'TA',
        1 => 'stores',
        2 => '7',
        3 => '16384',
      ),
      13 => 
      array (
        0 => 'TA',
        1 => 'users',
        2 => '10',
        3 => '16384',
      ),
    ),
  ),
);
?>