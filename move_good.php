<?php ob_start(); ?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead(); 
		checkPostMove();
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php moveGoodFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>