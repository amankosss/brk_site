<?php
//ob_start();
# add views(functions)	
include_once("views.php");
# constant values
//$site_url="http://localhost/brk_site/ ";
$default_good_icon = "images\good.png";

# set connetction to localhost or server

function setConnection() {
    // Create connection
    global $conn, $servername, $username, $password, $dbname;
    $conn = new mysqli($servername, $username, $password, $dbname);
    if ($conn->connect_error) {// Check connection
        die("Connection failed: " . $conn->connect_error);
    }
    $conn->set_charset("utf8");
    date_default_timezone_set("Asia/Almaty");

    # times of tables' update
    global $db_tables, $db_updates;
    $update_db = $conn->query("SHOW TABLE STATUS FROM " . $dbname);
    $db_tables = array();
    $db_updates = array();
    if ($update_db->num_rows > 0) {
        while ($row = $update_db->fetch_assoc()) {
            array_push($db_tables, $row['Name']);
            array_push($db_updates, $row['Update_time']);
        }
    }
}

# default head of any php API file

function addImportantMoments() {
    ?>
    <meta charset="UTF-8">
    <?php
    date_default_timezone_set("Asia/Almaty");
}

function isServerHost() {
    $whitelist = array('127.0.0.1', '::1');
    if (in_array($_SERVER['REMOTE_ADDR'], $whitelist))
        return false;
    return true;
}

# default head of any html/php file

function getHead() {
    global $main_url;
    $styles_url = $main_url . "styles/";
    ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Bootstrap -->
    <link href="<?php echo $styles_url; ?>bootstrap.css" rel="stylesheet">
    <link href="<?php echo $styles_url; ?>style.css" rel="stylesheet" >
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->
    <link rel="stylesheet" href="chosen/chosen.css">
<?php
}

# functions for work with cookie

function checkUser($user_id, $user_password, $conn) {
    global $is_valid_user;
    $is_valid_user = 0;
    $text_showed = 0;
    $sql = "SELECT * FROM  `users` ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        // checking user 
        while ($row = $result->fetch_assoc()) {
            if ($row["id"] == $user_id) {
                if ($row["password"] == $user_password) {
                    $is_valid_user = 1;
                } else {
                    //existing user send invalid password
                }
                break;
            }
        }
    }
    if ($is_valid_user == 0) {//not found user
        echo '{"error_show":"Не правильный пароль или логин"}';
    }
}

function checkLoggedIn($status) {
    switch ($status) {
        case "yes":
            if (!isset($_COOKIE["loggedIn"])) {
                header("Location: oauth.php");
                exit;
            } 
            else {
                setcookie('COOKIE_LAST_TIME', time(), 0, '/');
            }
            break;
        case "no":
            if (isset($_COOKIE["loggedIn"]) && $_COOKIE["loggedIn"] == true) {
                header("Location: index.php");
                exit;
            }
            break;
    }
    return true;
}

function cleanMemberCookie($user) {
    setcookie('loggedIn', true, 0, '/');
    setcookie('user', serialize($user), 0, '/');
    if (isset($_COOKIE["user"])) {
        global $conn;
        $text = ' Пользователь зашел в веб-сайт ';
        $type = "Вход";
        $emp_id = $user['id'];
        $comp_id = $user['comp_id'];
        $conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')");
    }
}

function flushMemberCookie() {
    if ($_COOKIE["loggedIn"] && $_COOKIE["user"]) {
        global $conn;
        $user = unserialize($_COOKIE["user"]);
        $emp_id = $user['id'];
        $comp_id = $user['comp_id'];
        $text = ' Пользователь вышел с веб-сайта ';
        $type = "Выход";
        $conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')");
    }
    setcookie('loggedIn', false, time()-3600*24, '/');
    setcookie('user', 0, time()-3600*24, '/');
    //session_destroy();
    return true;
}

# checks for operations with good

function existStoreId($store_id) {
    loadStores();
    global $stores;
    return isset($stores[$store_id]);
}

function existGoodId($good_id) {
    loadGoods();
    global $goods;
    return isset($goods[$good_id]);
}

# checking operations post

function checkPostDebit() {
    if (isset($_POST["good_id"])) {
        loadStores();
        loadGoods();
        global $stores, $goods, $error_number, $error_show;
        if (debitGood($_POST["good_id"], $_POST["store_id"], $_POST["good_count"], $_POST["good_net_price"])) {
            ?>
            <script>alert("Оприходавано");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {
                        var d = document.getElementById("input_count");
                        d.className += " has-error";
                    }
                    alert("Не удалось! " + <?php echo '"' . $error_show . '"'; ?>);
                };
            </script>
            <?php
        }
    }
}

function checkPostSell() {
    if (isset($_POST["good_id"])) {
        global $error_number, $error_show;
        if (sellGood($_POST["good_id"], $_POST["store_id"],$_POST["account_id"], $_POST["client_id"], $_POST["good_count"],$_POST["is_net_sell"],$_POST["comment"])) {
            ?>
            <script>alert(" Продано! ");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {// count is less than 1 or store have no enough
                        var d = document.getElementById("input_count");
                        d.className += " has-error";
                    }
                    alert("Не удалось ! " + <?php echo '"' . $error_show . $_POST["good_id"] . " " . $_POST["store_id"] . " " . $_POST["good_count"] . '"'; ?>);
                };
            </script>
            <?php
        }
    }
}

function checkPostMove() {
    if (isset($_POST["good_id"])) {
        global $error_number, $error_show;
        if (moveGood($_POST["good_id"], $_POST["from_store_id"], $_POST["to_store_id"], $_POST["good_count"])) {
            ?>
            <script>alert("Товар Перемещен!");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {
                        var d = document.getElementById("input_count");
                        d.className += " has-error";
                    } else if (x == 2) {
                        var d = document.getElementById("input_from_store");
                        d.className += " has-error";
                        var d2 = document.getElementById("input_to_store");
                        d2.className += " has-error";
                    }
                    alert("Не удалось ! " + <?php echo '"' . $error_show . $_POST["good_id"] . " " . $_POST["from_store_id"] . " " . $_POST["to_store_id"] . " " . $_POST["good_count"] . '"'; ?>);
                };
            </script>
            <?php
        }
    }
}

function checkPostReturn() {
    if (isset($_POST["good_id"])) {
        global $error_number, $error_show;
        if (returnGood($_POST["good_id"], $_POST["store_id"], $_POST["client_id"], $_POST["good_count"], $_POST["reason"])) {
            ?>
            <script>alert(" Возврат оформлен! ");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {// count is less than 1 or store have no enough
                        var d = document.getElementById("input_count");
                        d.className += " has-error";
                    }
                    alert("Не удалось ! " + <?php echo '"' . $error_show . $_POST["good_id"] . " " . $_POST["store_id"] . " " . $_POST["good_count"] . '"'; ?>);
                };
            </script><?php
        }
    }
}

function checkPostAdd() {
    if (isset($_POST["good_code"]) || isset($_POST["good_name"])) {
        global $error_number, $error_show;
        if (addNewGood($_POST["cat_id"], $_POST["good_code"], $_POST["good_name"], $_POST["good_description"], $_POST["good_price"], $_POST["good_net_price"], $_POST["good_bonus"], $_POST["good_discount"])) {
            ?>
            <script>alert("Товар добавлен!");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    alert("Не удалось ! " + <?php echo "\"" . $error_show . "\""; ?>);
                };
            </script>
            <?php
        }
    }
}

function checkPostAddCategory() {
    if (isset($_POST["cat_name"]) && isset($_POST["cat_description"])) {
        global $error_number, $error_show;
        if (addNewGoodCategory($_POST["cat_name"], $_POST["cat_description"])) {
            showAlert("Категория добавлена");
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {
                        var d = document.getElementById("input_cat_name");
                        d.className += " has-error ";
                    }

                };
            </script>
            <?php
            showAlert($error_show);
        }
    }
}

function checkPostAddFlowCategory() {
    if (isset($_POST["cat_id"]) && isset($_POST["cat_name"])) {
        global $error_number, $error_show;
        if (addNewFlowCategory($_POST["cat_id"], $_POST["cat_name"], $_POST["cat_description"], $_POST["is_real"])) {
            showAlert("Категория добавлена");
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {
                        var d = document.getElementById("input_cat_name");
                        d.className += " has-error ";
                    }
                };
            </script>
            <?php
            showAlert($error_show);
        }
    }
}

function checkPostFlowOperation() {
    if (isset($_POST["account_id"]) && isset($_POST["flow_id"]) && isset($_POST["flow_price"]) && isset($_POST["comment"])) {
        global $error_number, $error_show;
        if (flowOperation($_POST["account_id"], $_POST["flow_id"], $_POST["flow_price"], $_POST["comment"])) {
            showAlert("Транзакция завершена");
        } else {
            ?>
            <script>
                window.onload = function () {
                    x = <?php echo $error_number; ?> * 1;
                    if (x == 1) {
                        var d = document.getElementById("flow_price");
                        d.className += " has-error ";
                    }
                };
            </script>
            <?php
            showAlert($error_show);
        }
    }
}

function checkPostDeleteOperation() {
    if (isset($_POST["operation_id"]) && isset($_POST["operation_title_id"]) && isset($_POST["operation_comment_id"])) {
        global $error_number, $error_show;
        $operation_title = $_POST["operation_title_id"];
        $operation_id = $_POST["operation_id"];
        $comment = $_POST["operation_comment_id"];
        if (deleteOperation($operation_title, $operation_id, $comment)) {
            showAlert("Транзакция завершена2");
            if (isset($_COOKIE["SAVED_PAGE"])) {
                header("Location: " . $_COOKIE['SAVED_PAGE']);
            }
        } else {
            showAlert($error_show);
        }
    }
    //else 
    //	showAlert("Ошибка");
}

function checkPost() {
    global $error_number, $error_show;
    if (!isset($_POST["post_operation"]))
        return;
    if ($_POST["post_operation"] == "ADD_CLIENT") {
        # ADD_CLIENT
        if (addNewClient($_POST["client_name"], $_POST["client_surname"], $_POST["client_email"], $_POST["client_phone"])) {
            ?>
            <script>alert("Клиент добавлен!");</script> <?php
        } else {
            ?>
            <script>
                window.onload = function () {
                    alert("Не удалось добавить клиента! " + <?php echo "\"" . $error_show . "\""; ?>);
                };
            </script>
            <?php
        }
    }
}

# operations with goods

function debitGood($good_id, $store_id, $count, $net_price) {
    global $conn, $error_show, $error_number;
    // use for transaction
    $conn->autocommit(false);
    if ($count < 1) {
        $error_show = "Не валидное количество товара";
        $error_number = 1;
        return false;
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    if (is_null($net_price) || $net_price == "")
        $net_price = getGoodLastNetPrice($good_id);
    $sql = "INSERT INTO debits (good_id, count, comp_id, store_id, employee_id, debit_date, net_price) 
				VALUES ('$good_id', '$count' , '$comp_id','$store_id','$emp_id'," . "'" . date("Y-m-d H:i:s") . "', $net_price)";
    if ($conn->query($sql) === TRUE) {
        loadGoods();
        loadStores();
        global $goods, $stores;
        $text = 'Оприходавано ' . $count . ' штук товара "' . $goods[$good_id]["name"] . '" на склад "' . $stores[$store_id]["name"] . '"';
        $type = "Оприходование";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
					VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function moveGood($good_id, $from_store_id, $to_store_id, $count) {
    global $conn, $error_show, $error_number, $goods, $stores;
    // use for transaction
    $conn->autocommit(false);
    if ($count < 1) {
        $error_show = "Не валидное количество товара";
        $error_number = 1;
        return false;
    }
    if ($from_store_id == $to_store_id) {
        $error_show = "Нельзя с одного магазина в тот же магазин перемещать товар";
        $error_number = 2;
        return false;
    }
    if (getGoodCount($good_id, $from_store_id) < (int) $count) {
        $error_show = "На складе нету столько товара";
        $error_number = 1;
        return false;
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $sql = "INSERT INTO moves (good_id, comp_id, from_store_id, to_store_id, count, employee_id, move_date) 
				VALUES ('$good_id', '$comp_id', '$from_store_id', '$to_store_id', '$count','$emp_id', '" . date("Y-m-d H:i:s") . "')";
    if ($conn->query($sql) === TRUE) {
        loadGoods();
        loadStores();
        global $goods, $stores;
        $text = 'Перемещено ' . $count . ' штук товара "' . $goods[$good_id]["name"] . '" со склада "' . $stores[$from_store_id]["name"] .
                '" на склад "' . $stores[$to_store_id]["name"] . '"';
        $type = "Перемещение";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function sellGood($good_id, $store_id,$account_id, $client_id, $count, $is_net_sell,$comment) {
    loadGoods();
    global $conn, $goods, $error_show, $error_number;
    // use for transaction
    $conn->autocommit(false);
    if ($count < 1) {
        $error_show = "Не валидное количество товара";
        $error_number = 1;
        return false;
    }
    if (getGoodCount($good_id, $store_id) < (int) $count) {
        $error_show = "В магазине нету столько товара";
        $error_number = 1;
        return false;
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $price = $goods[$good_id]["price"];
    $discount = $goods[$good_id]["discount"];
    $bonus = $goods[$good_id]["bonus"];
    $sell_date = date("Y-m-d H:i:s");
    $cash_date = "NULL";
    $is_net = 0;
    $type = "Продажа";
    if(isset($is_net_sell)){
        // Интернет продажа
        $is_net = 2;
        $type = "Интернет продажа";
    }
    else {
        // Обычная продажа
        $cash_date = "'" . $sell_date . "'";
    }
    $sql = "INSERT INTO sells (good_id, count, comp_id, store_id, account_id, employee_id, client_id, sell_date, cash_date, price, discount,bonus,is_net_sell, comment) 
				VALUES ('$good_id', '$count' , '$comp_id','$store_id','$account_id','$emp_id','$client_id','$sell_date', $cash_date ,'$price','$discount','$bonus','$is_net','$comment')";
    if ($conn->query($sql) === TRUE) {
        loadStores();        loadAccounts();
        global $stores,$accounts;
        $text = 'Продано ' . $count . ' штук товара "' . $goods[$good_id]["name"] . '" c магазина "' . $stores[$store_id]["name"] . '" на кассу "' . $accounts[$account_id]['title'] . '"';
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
					VALUES ('$emp_id', '$comp_id', '$text','$type','$sell_date')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            $error_number = 0;
            $error_show = "Ошибка на сервере2, попробуйте снова ! ";
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function returnGood($good_id, $store_id, $client_id, $count, $reason) {
    loadGoods();
    global $conn, $goods, $error_show, $error_number;
    // use for transaction
    $conn->autocommit(false);
    if ($count < 1) {
        $error_show = "Не валидное количество товара";
        $error_number = 1;
        return false;
    }
    // if(getGoodCount($good_id,$store_id) < (int)$count){
    // 	$error_show = "В магазине нету столько товара";		
    // 	$error_number = 1;
    // 	return false;
    // }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $price = $goods[$good_id]["price"];
    $discount = $goods[$good_id]["discount"];
    $bonus = $goods[$good_id]["bonus"];
    $sql = "INSERT INTO returns (good_id, count, comp_id, store_id, employee_id, client_id, return_date, price, discount,bonus,reason) 
				VALUES ('$good_id', '$count' , '$comp_id','$store_id','$emp_id','$client_id','" . date("Y-m-d H:i:s") . "','$price','$discount','$bonus','$reason')";
    if ($conn->query($sql) === TRUE) {
        loadStores();
        global $stores;
        $text = ' Оформлен возврат ' . $count . ' штук товара "' . $goods[$good_id]["name"] . '" в магазине "' . $stores[$store_id]["name"] . '"';
        $type = "Возврат";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function addNewGood($cat_id, $code, $name, $description, $price, $net_price, $bonus, $discount) {
    loadGoods();
    global $conn, $goods, $error_show, $error_number;
    // use for transaction
    $valid = 1;
    $conn->autocommit(false);
    if ($name != "") {
        foreach ($goods as $key => $good) {
            if ($good["code"] == $code) {
                $error_show = " Товар с таким штрих-кодом уже есть. ";
                $error_number = 2;
                return false;
            }
        }
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    
    $sql = "INSERT INTO goods (code, valid, name, description,
		price, net_price, cat_id, comp_id,discount,bonus) 
				VALUES ('$code',$valid,'$name','$description',$price,$net_price,$cat_id,$comp_id,$discount,$bonus)";
    if ($conn->query($sql) === TRUE) {
        $is_photo = true;
        if (is_uploaded_file($_FILES['good_img']["tmp_name"])) {
            loadGoods();
            global $goods;
            $id = 0;
            foreach ($goods as $key => $good) {
                if ($comp_id == $good["comp_id"] && $code == $good["code"] && $name == $good["name"]) {
                    $id = $good["good_id"];
                }
            }
            if ($id == 0) {
                $error_number = 0;
                $error_show = "Ошибка на сервере, попробуйте снова ! ";
                return false;
            }
            $_path_to_file = "images/goods/good_" . $id . ".png";
            move_uploaded_file($_FILES["good_img"]["tmp_name"], $_path_to_file);
            $is_photo = $conn->query("UPDATE goods SET pic_url='$_path_to_file' WHERE good_id=$id");
        }
        if ($is_photo) {
            loadCategories();
            global $categories;
            $text = 'Добавлен новый товар. 
					Категория: "' . $categories[$cat_id]["title"] . '",
					Штрих-код: "' . $code . '", 
					Название: "' . $name . '",
				Описание: "' . $description . '",
				Цена: "' . $price . '",
				Себестоимость: "' . $net_price . '",
				Бонус: "' . $bonus . '%",
				Скидка: "' . $discount . '%".';
            $type = "Новый товар";
            if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
						VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
                $conn->commit();
                return true;
            } else {
                $conn->rollback();
                return false;
            }
        } else {
            $error_number = 0;
            $error_show = "Проблема с загрузкой фотографии ! ";
            $conn->rollback();
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function addNewClient($client_name, $client_surname, $client_email, $client_phone) {
    loadClients();
    global $conn, $clients, $error_show, $error_number;
    // use for transaction
    $conn->autocommit(false);
    if ($client_email != "") {
        foreach ($clients as $key => $client) {
            if ($client["email"] == $client_email) {
                $error_show = "Клиент с такой почтой уже есть. ";
                $error_number = 1;
                return false;
            }
        }
    } else {
        $error_show = " Не правильно указана почта. ";
        $error_number = 1;
        return false;
    }
    if ($client_phone != "") {
        foreach ($clients as $key => $client) {
            if ($client["phone"] == $client_phone) {
                $error_show = " Клиент с таким номером уже есть. ";
                $error_number = 2;
                return false;
            }
        }
    } else {
        $error_show = " Не правильно указана почта. ";
        $error_number = 2;
        return false;
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    
    $sql = "INSERT INTO clients (phone, email, comp_id, name, surname) 
				VALUES ('$client_phone','$client_email','$comp_id','$client_name','$client_surname')";
    if ($conn->query($sql) === TRUE) {
        loadCategories();
        global $categories;
        $text = 'Добавлен новый клиент. 
			Имя: "' . $client_name . '",
			Фамилия: "' . $client_surname . '", 
			Телефон: ' . $client_phone . ',
			Почта: "' . $client_email . '".';
        $type = "Новый клиент";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
					VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

function addNewGoodCategory($cat_title, $cat_description) {
    loadCategories();
    global $conn, $error_show, $error_number, $categories;
    // use for transaction
    $conn->autocommit(false);
    foreach ($categories as $key => $category) {
        if ($category["title"] == $cat_title) {
            $error_show = "Такая категория уже существует";
            $error_number = 1;
            return false;
        }
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $parent_id = 0;
    $level = 1;
    $sql = "INSERT INTO categories (title, description, parent_id, level, comp_id) 
				VALUES ('$cat_title', '$cat_description' , $parent_id,$level,$comp_id)";
    if ($conn->query($sql) === TRUE) {
        $text = 'Добавлена категория товаров "' . $cat_title . '" с описанием "' . $cat_description . '"';
        $type = "Новая категория товаров";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            $error_show = "Не удалось записать в историю, попробуйте снова !";
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова !";
        return false;
    }
}

function addNewFlowCategory($cat_id, $cat_title, $cat_description, $is_real) {
    loadFlowCategories();
    global $conn, $error_show, $error_number, $inflows, $outflows;
    $conn->autocommit(false);
    if ($cat_id == 1)
        $flow_categories = $inflows;
    else
        $flow_categories = $outflows;
    foreach ($flow_categories as $key => $category) {
        if ($category["title"] == $cat_title) {
            $error_show = "Такая категория уже существует";
            $error_number = 1;
            return false;
        }
    }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $sql = "INSERT INTO cashflow_types (title, description, flow_type,is_real, comp_id) 
				VALUES ('$cat_title', '$cat_description' , $cat_id,$is_real,$comp_id)";
    if ($conn->query($sql) === TRUE) {
        $text = 'Добавлена категория денежного потока "' . $cat_title . '" с описанием "' . $cat_description . '"';
        $type = "Новая категория денежних потоков";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            $error_show = "Не удалось записать в историю, попробуйте снова !";
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова !";
        return false;
    }
}

function flowOperation($account_id, $flow_id, $flow_price, $comment) {
    loadFlowCategories();
    loadAccounts();
    global $conn, $accounts, $inflows, $outflows;
    $cur_cat;
    $find = false;
    foreach ($inflows as $key => $flow) {
        if ($flow["cashflow_type_id"] == $flow_id) {
            $cur_cat = $flow;
            $find = true;
            break;
        }
    }
    if (!$find)
        foreach ($outflows as $key => $flow) {
            if ($flow["cashflow_type_id"] == $flow_id) {
                $cur_cat = $flow;
                break;
            }
        }
    // if(!$cur_cat["flow_type"] && calcStoreMoney($store_id) < $flow_price){
    // 	$error_show = "На кассе не достаточно средств";		
    // 	$error_number = 1;
    // 	return false;
    // }
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $operation = "приток на кассу";
    if (!$cur_cat["flow_type"])
        $operation = "отток с кассы";

    $sql = "INSERT INTO cashflows (cashflow_type_id, comment, comp_id, employee_id, account_id, price, cashflow_date) 
				VALUES ($flow_id, '$comment' , $comp_id, $emp_id, $account_id, $flow_price, '" . date("Y-m-d H:i:s") . "')";
    if ($conn->query($sql) === TRUE) {
        $text = 'Произведен ' . $operation . ' "' . $accounts[$account_id]["title"] . '" с комментарием "' . $comment . '" в размере ' . $flow_price . ' тг ';
        $type = "Приток на кассу";
        if (!$cur_cat["flow_type"])
            $type = "Отток на кассу";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
				VALUES ('$emp_id', '$comp_id', '$text','$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            $error_show = "Не удалось записать в историю, попробуйте снова !";
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова !";
        return false;
    }
}

function deleteOperation($operation_type, $id, $history) {
    global $conn, $error_show, $error_number;
    // use for transaction
    $conn->autocommit(false);
    //if($count < 1){
    //	$error_show = "Не валидное количество товара";		
    //	$error_number = 1;
    //	return false;
    //}
    $table = $operation_type . "s";
    $column_id = $operation_type . "_id";
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $sql = "DELETE FROM $table WHERE comp_id = $comp_id AND $column_id = $id ";
    if ($conn->query($sql) === TRUE) {
        $type = "Удаление операции";
        if ($conn->query("INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) 
			VALUES ('$emp_id', '$comp_id', \"$history\" ,'$type'," . "'" . date("Y-m-d H:i:s") . "')")) {
            $conn->commit();
            return true;
        } else {
            $conn->rollback();
            $error_number = 0;
            $error_show = "Ошибка с записью в историю, попробуйте снова ! ";
            return false;
        }
    } else {
        $error_number = 0;
        $error_show = "Ошибка на сервере, попробуйте снова ! ";
        return false;
    }
}

# check user's login and pass and return $user

function checkPass($login, $password) {
    global $conn, $default_male_icon, $default_female_icon;
    $default_male_icon = 'images\male2.png';
    $default_female_icon = 'images\female2.png';
    $sql = "SELECT * FROM users WHERE email='$login' and password='$password'";
    $result = $conn->query($sql);
    if ($result->num_rows == 1) {
        $row = ($result->fetch_assoc());
        if (!isset($row["pic_url"]) || is_null($row["pic_url"]) || $row["pic_url"] == "") {
            if ($row["gender"] == "M" || $row["gender"] == "М")
                $row["pic_url"] = $default_male_icon;
            else //if($row["gender"]=="F")
                $row["pic_url"] = $default_female_icon;
        }
        //exit( $row["job"] . " <br>". ((int)$row["job"] & 1) . " <br>". ((int)$row["job"] & 2) . " <br>". ((int)$row["job"] & 7) . " <br><hr>");
        //$row["type_ru"] = "Сотрудник";
        return $row;
    } else
        return 0;
}

# check for access to system (1)
function hasAccessToSystem($job){
    return $job%2;
}
# check for access to sell, return (2)
function hasAccessToSell($job) {
    $job /= 2;
    return $job%2;
}
# check for access to move (4)
function hasAccessToMove($job) {
    $job /= 4;
    return $job%2;
}
# check for access to debit (8)
function hasAccessToDebit($job) {
    $job /= 8;
    return $job%2;
}
# check for access to all actions (16)
function hasAccessToEditAllActions($job) {
    $job /= 16;
    return $job%2;
}
# check for access to net_price (32)
function hasAccessToNetPrice($job) {
    $job /= 32;
    return $job%2;
}
# check for access to confirm_net_sells (64)
function hasAccessToConfirmNetSells($job) {
    $job /= 64;
    return $job%2;
}

//TODO FIX
function checkAccessToEmp($emp_id) {
    loadEmployees();
    $user = unserialize($_COOKIE["user"]);
    if ($user["type"] == "admin")
        return true;
    if ($user["id"] == $emp_id)
        return true;
    return false;
}

# function for check validation of field data(email, name , password, number, etc...)

function field_validator($field_descr, $field_data, $field_type, $min_length = "", $max_length = "", $field_required = 1) {

    global $messages;

    if (!$field_data && !$field_required) {
        return;
    }

    $field_ok = false;

    $email_regexp = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
    $email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

    $data_types = array(
        "email" => $email_regexp,
        "digit" => "^[0-9]$",
        "number" => "^[0-9]+$",
        "alpha" => "^[a-zA-Z]+$",
        "alpha_space" => "^[a-zA-Z ]+$",
        "alphanumeric" => "^[a-zA-Z0-9]+$",
        "alphanumeric_space" => "^[a-zA-Z0-9 ]+$",
        "string" => ""
    );

    if ($field_required && empty($field_data)) {
        $messages[] = "Поле $field_descr является обезательным";
        return;
    }

    if ($field_type == "string") {
        $field_ok = true;
    } else {
        $field_ok = ereg($data_types[$field_type], $field_data);
    }

    if (!$field_ok) {
        $messages[] = "Пожалуйста введите нормальный $field_descr.";
        return;
    }

    if ($field_ok && ($min_length > 0)) {
        if (strlen($field_data) < $min_length) {
            $messages[] = "$field_descr должен быть не короче $min_length символов.";
            return;
        }
    }

    if ($field_ok && ($max_length > 0)) {
        if (strlen($field_data) > $max_length) {
            $messages[] = "$field_descr не должен быть длиннее $max_length символов.";
            return;
        }
    }
}

# special functions to calculate of counts of goods

function getSpecificGoodCount($table, $good_id, $store_id) {
    #from $table select all count of $good in the store with $store_id  
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM $table WHERE comp_id=$comp_id AND good_id=$good_id ";
    if (!is_null($store_id) && $store_id != 0)
        $sql .= " AND store_id = $store_id ";
    //echo $sql . "<br>";
    $result = $conn->query($sql);
    $cnt = 0;
    if (is_object($result) && $result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $cnt+= (int) $row["count"];
        }
    }
    return $cnt;
}

function getGoodMovesCountFromStore($good_id, $store_id) {
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM moves WHERE comp_id=$comp_id AND good_id=$good_id AND from_store_id = $store_id";
    $result = $conn->query($sql);
    $cnt = 0;
    if (is_object($result) && $result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $cnt+= (int) $row["count"];
        }
    }
    return $cnt;
}

function getGoodMovesCountToStore($good_id, $store_id) {
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM moves WHERE comp_id=$comp_id AND good_id=$good_id AND to_store_id = $store_id";
    $result = $conn->query($sql);
    $cnt = 0;
    if (is_object($result) && $result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $cnt+= (int) $row["count"];
        }
    }
    return $cnt;
}
function getGoodCountInWay($good_id, $store_id) {
    global $conn;
    $store_check = "";
    if($store_id != 0)
        $store_check = " AND `store_id` = '" . $store_id . "' ";
    $sql = "SELECT count(*) FROM `sells`
                WHERE `is_net_sell` = 2
                AND `good_id` = '" . $good_id . "' $store_check ;";
    $result = $conn->query($sql);
    if ($row = $result->fetch_assoc()) 
        if (isset($row["count(*)"]))
            return $row["count(*)"];
    return 0;
}
function getGoodCount($good_id, $store_id) {

    return getSpecificGoodCount("debits", $good_id, $store_id) - getSpecificGoodCount("sells", $good_id, $store_id) + getSpecificGoodCount("returns", $good_id, $store_id) + getGoodMovesCountToStore($good_id, $store_id) - getGoodMovesCountFromStore($good_id, $store_id);
}

function calcGoodSells($good_id, $months_ago = 0){
    $sql = "";
    global $conn;
    if($months_ago == 0)
        $sql = "SELECT count(*) FROM `sells`
                WHERE `sell_date` > LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL 1 MONTH
                AND `sell_date` < DATE_ADD(LAST_DAY(CURDATE()), INTERVAL 1 DAY)
                AND `good_id` = '" . $good_id . "';";
    else $sql = "SELECT count(*) FROM `sells`
                WHERE `sell_date` > LAST_DAY(CURDATE()) + INTERVAL 1 DAY - INTERVAL " . ($months_ago+1) . " MONTH
                AND `sell_date` < DATE_ADD(LAST_DAY(CURDATE()- INTERVAL " . $months_ago . " MONTH), INTERVAL 1 DAY)
                AND `good_id` = '" . $good_id . "';";
    //return $sql;
    $result = $conn->query($sql);
    if ($row = $result->fetch_assoc()) {
        if (isset($row["count(*)"]))
            return $row["count(*)"];
        else return -1;
    }
}

function calcStoreMoney($store_id) {
    $money = 0;
    # load flow operations
    loadFlowCategories();
    global $cashflows, $conn;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM cashflows WHERE comp_id='$comp_id' AND store_id='$store_id' ORDER BY cashflow_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 1)
                $money += $row["price"];
            else if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 0)
                $money -= $row["price"];
        }
    }
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' AND store_id='$store_id' ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $money += intval($row["price"] * ((100 - $row["discount"]) / 100.0) * $row['count']);
            //echo "+" . intval( $row["price"] *  ((100-$row["discount"])/100.0) * ((100-$row["bonus"])/100.0)*$row['count']);
        }
    }
    $sql = "SELECT * FROM returns WHERE comp_id='$comp_id' AND store_id='$store_id' ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $money -= intval($row["price"] * ((100 - $row["discount"]) / 100.0) * $row['count']);
            //echo "-" . intval( $row["price"] *  ((100-$row["discount"])/100.0) * ((100-$row["bonus"])/100.0)*$row['count']);
        }
    }
    return $money;
}
//TODO
function calcAccountMoney($account_id) {
    $money = 0;
    # load flow operations
    loadFlowCategories();
    global $cashflows, $conn;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM cashflows WHERE comp_id=$comp_id AND account_id=$account_id ORDER BY cashflow_date DESC";
    $result = $conn->query($sql);
    //showAlert("SELECT * FROM cashflows WHERE comp_id=$comp_id AND account_id=$account_id ORDER BY cashflow_date DESC");
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 1)
                $money += $row["price"];
            else if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 0)
                $money -= $row["price"];
        }
    }
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' AND account_id='$account_id' AND is_net_sell != 2";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $money += intval($row["price"] * ((100 - $row["discount"]) / 100.0) * $row['count']);
            //echo "+" . intval( $row["price"] *  ((100-$row["discount"])/100.0) * ((100-$row["bonus"])/100.0)*$row['count']);
        }
    }
    $sql = "SELECT * FROM returns WHERE comp_id='$comp_id' AND account_id='$account_id' ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
       while ($row = $result->fetch_assoc()) {
           $money -= intval($row["price"] * ((100 - $row["discount"]) / 100.0) * $row['count']);
           //echo "-" . intval( $row["price"] *  ((100-$row["discount"])/100.0) * ((100-$row["bonus"])/100.0)*$row['count']);
       }
    }
    return $money;
}
function calcStoreGoodsMoney($store_id) {
    $money = 0;
    //TODO
    $user = unserialize($_COOKIE["user"]);
    if(!hasAccessToNetPrice($user['job']))
        return 0;
    $GLOBALS["store_goods_net_money"] = 0;
    loadGoods();
    global $goods;
    foreach ($goods as $key => $good) {
        $count = getGoodCount($good["good_id"], $store_id);
        $money += $count * $good["price"];
        if(hasAccessToNetPrice($user['job']))
            $GLOBALS["store_goods_net_money"] += $count * $good["net_price"];
    }
    return $money;
}    

# function to find the last net price of good

function getGoodNetPrice($good_id,$job = 255) {
    global $conn, $goods;
    # if no access to net_price
    if(!hasAccessToNetPrice($job)) return 0; 
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM net_prices WHERE comp_id='$comp_id' AND good_id='$good_id' ORDER BY from_date DESC";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            return $row["net_price"];
        }
    }
    if(isset($goods[$good_id]["net_price"]))
        return $goods[$good_id]["net_price"];
    return 0;
   }

function getGoodBenefit($good_id) {
    if(!hasAccessToNetPrice($job)) return 0; 
    global $goods;
    return $goods[$good_id]["price"] * (100 - $goods[$good_id]["discount"]) * (100 - $goods[$good_id]["bonus"]) / 10000 - getGoodNetPrice($good_id);
}
function getGoodCode($good_id){
    loadGoods();
    return $GLOBALS['goods'][$good_id]['code'];
}
function getGoodNetPriceByDate($good_id, $sell_date) {
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    # count of sold goood before this date
    $sell_count = 0;
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' AND good_id='$good_id' AND sell_date < '$sell_date' ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $sell_count += $row["count"];
        }
    }

    # count of debited good before this date
    $debit_count = 0;
    $last_debit_date = toDate("1900/00/00");
    $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' AND good_id='$good_id' AND debit_date < '$sell_date' ORDER BY debit_date";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $debit_count += $row["count"];
            if ($debit_count > $sell_count) {
                $last_debit_date = $row["debit_date"];
                break;
            }
        }
    }

    //echo 	"<script language='javascript'> alert($sell_count); </script>";
    $sql = "SELECT * FROM net_prices WHERE comp_id='$comp_id' AND good_id='$good_id' AND from_date < '$last_debit_date' ORDER BY from_date DESC";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            return $row["net_price"];
        }
    }
    return $goods[$good_id]["net_price"];
}

function getGoodNetPriceExactByDate($good_id, $sell_date, $sell_good_count) {
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    # count of sold goood before this date
    $sell_count = 0;
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' AND good_id='$good_id' AND sell_date < '$sell_date' ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $sell_count += $row["count"];
        }
    }

    # count of debited good before this date
    $debit_count = 0;
    $valid_count = 0;
    $last_debit_date;
    # sum of all net prices
    $sum_of_net = 0;
    $sum_of_good = 0;
    $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' AND good_id='$good_id' AND debit_date < '$sell_date' ORDER BY debit_date ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $debit_count += $row["count"];
            //echo " My Good info: " . $goods[$good_id]["name"] . " debited: " . $row["debit_date"] . " sell_date:" .$sell_date."<br>";
            if ($debit_count > $sell_count) {
                $last_debit_date = $row["debit_date"];
                $sql = "SELECT * FROM net_prices WHERE comp_id='$comp_id' AND good_id='$good_id' AND from_date < '$last_debit_date' ORDER BY from_date DESC";
                $sub_result = $conn->query($sql);
                $cur_net_price = 0;
                if ($result->num_rows > 0) {
                    while ($sub_row = $sub_result->fetch_assoc()) {
                        $cur_net_price = $sub_row["net_price"];
                        //echo  " net: " . $cur_net_price . "<br>"; 	
                        break;
                    }
                }
                if ($cur_net_price == 0)
                    $cur_net_price = $goods[$good_id]["net_price"];
                $valid_count = $debit_count - $sell_count;
                if ($valid_count >= $sell_good_count) {
                    //echo "YGood: " . $goods[$good_id]["name"] . " sum+=: " . ($sell_good_count - $sum_of_good). "*" . $cur_net_price . "<br>"; 	
                    $sum_of_net += ($sell_good_count - $sum_of_good) * $cur_net_price;
                    return (int) ($sum_of_net / $sell_good_count);
                } else {
                    //echo "XGood: " . $goods[$good_id]["name"] . " sum+=: " . min($row["count"],$valid_count). "*" . $cur_net_price . "<br>"; 	
                    $sum_of_net += min($row["count"], $valid_count) * $cur_net_price;
                    $sum_of_good += min($row["count"], $valid_count);
                }
            }
        }
    }
    #if we have no debited good , we just take last net_price of good
    return getGoodNetPriceByDate($good_id, $sell_date);
}
// TODO fix bonus if its exist
function getGoodRealPrice($price, $discount,$bonus = 0){
    //var_dump($price);
    //var_dump($discount);
    $price = intval($price)*(100-intval($discount));
    $price /= 100.0;
    return $price;
}

function getTableLastUpdate($table) {
    global $db_tables, $db_updates;
    foreach ($db_tables as $key => $table_name) {
        if ($table_name == $table)
            return $db_updates[$key];
    }
}

function getGoodLastNetPrice($good_id) {
    global $conn, $goods;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    # count of sold goood before this date
    $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' AND good_id='$good_id' ORDER BY debit_date DESC";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            if (isset($row["net_price"]) && $row["net_price"]!=0)
                return $row["net_price"];
        }
    }
    if(isset($goods[$good_id]["net_price"]) && $goods[$good_id]["net_price"]>0)
        return $goods[$good_id]["net_price"];
    return 0;
}

# functions for load information about company goods,stores,clients,sells...

function loadEmployees() {
    global $conn, $employees;
    $employees = array();
    $default_male_icon = 'images\male2.png';
    $default_female_icon = 'images\female2.png';
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM users WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            if ($row["type"] == "keeper")
                $row["type_ru"] = "Кладовщик";
            if ($row["type"] == "seller")
                $row["type_ru"] = "Продавец";
            if ($row["type"] == "admin")
                $row["type_ru"] = "Управляющий";
            if (!isset($row["pic_url"]) || is_null($row["pic_url"]) || $row["pic_url"] == "") {
                if ($row["gender"] == "M" || $row["gender"] == "М")
                    $row["pic_url"] = $default_male_icon;
                else //if($row["gender"]=="F")
                    $row["pic_url"] = $default_female_icon;
            }
            $employees[$row["id"]] = $row;
        }
    }
}

function loadGoods() {
    //if (isset($GLOBALS['goods']) && isset($GLOBALS['goods_last_update']) && $GLOBALS['goods_last_update'] === $time()) {
    //    echo "<pre>" . var_dump($GLOBALS['goods']) . "</pre>";
    //    return;
    //}
    if (isset($GLOBALS['goods']) && isset($GLOBALS['goods_last_update'])) {
    //    echo "<pre>" . var_dump($GLOBALS['goods']) . "</pre>";
        return;
    }
    //exit;
    global $conn, $goods, $default_good_icon;
    #reload only if table updated
    //showErrorView(toTime($last_update));
    //showErrorView(toTime($_SESSION['goods_last_update']));
    
    $goods = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM goods WHERE comp_id='$comp_id' ORDER BY name";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            #set default picture
            if (is_null($row["pic_url"]) || $row["pic_url"] == "")
                $row["pic_url"] = $default_good_icon;
            if (is_null($row["bonus"]) || $row["bonus"] == "")
                $row["bonus"] = 0;
            if (is_null($row["discount"]) || $row["discount"] == "")
                $row["discount"] = 0;
            //$row["count"] = getGoodCount($row["good_id"],0);
            //$row["sell_count"] = getSpecificGoodCount("sells",$row["good_id"],0);
            //$row["return_count"] = getSpecificGoodCount("returns",$row["good_id"],0);
            $row["url"] = "good.php?good_id=" . $row["good_id"];
            $goods[$row["good_id"]] = $row;
        }
        $GLOBALS['goods_last_update'] = time();
    }
}

function loadStores() {
    global $conn, $stores;
    $stores = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM stores WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["url"] = "store.php?store_id=" . $row["store_id"];
            $stores[$row["store_id"]] = $row;
        }
    }
}
function loadAccounts() {
    global $conn, $accounts, $user;
    $accounts = array();
    if(!isset($user) || !isset($user['id']) )
        $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM accounts WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["url"] = "account.php?account_id=" . $row["id"];
            $accounts[$row["id"]] = $row;
        }
    }
}

function loadClients() {
    global $conn, $clients, $user;
    $clients = array();
    if(!isset($user) || !isset($user['id']) )
        $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM clients WHERE comp_id='$comp_id' ORDER BY client_id";
    $result = $conn->query($sql);
    $client["client_id"] = 0;
    $client["name"] = "[Не известно]";
    $client["phone"] = "Не известно";
    $clients[0] = $client;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            //$row["url"] = "client.php?client_id=" . $row["client_id"];
            $clients[$row["client_id"]] = $row;
        }
    }
}

function loadCategories() {
    loadGoods();
    global $conn, $categories, $goods;
    $categories = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM categories WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
//    # add category for goods without category
//    $cat['cat_id'] = 0;
//    $cat['title'] = "Без категории";
//    $cat["count"] = 0;
//    foreach ($goods as $key => $good) {
//        if ($good["cat_id"] == $cat["cat_id"])
//            $cat["count"] ++;
//    }
//    $categories[0] = $cat;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            //$row["url"] = "client.php?client_id=" . $row["client_id"];
            $row["count"] = 0;
            foreach ($goods as $key => $good) {
                if ($good["cat_id"] == $row["cat_id"])
                    $row["count"] ++;
            }
            $categories[$row["cat_id"]] = $row;
        }
    }
}

function loadSells($store_id, $days) {
    global $conn, $sells;
    $sells = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sub_sql = " ";
    if ($store_id != 0)
        $sub_sql .= "AND store_id = $store_id ";
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql AND DATEDIFF('" . date("Y:m:d") . "',sell_date) < $days ORDER BY sell_date DESC";
    if ($days == 0)
        $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($sells, $row);
        }
    }
}
function loadSellsByAccountId($account_id, $days) {
    global $conn, $sells;
    $sells = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sub_sql = " ";
    if ($account_id != 0)
        $sub_sql .= "AND account_id = $account_id ";
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql AND DATEDIFF('" . date("Y:m:d") . "',sell_date) < $days ORDER BY sell_date DESC";
    if ($days == 0)
        $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($sells, $row);
        }
    }
}

function loadCompany() {
    global $conn, $comp_info;
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    
    $sql = "SELECT * FROM companies WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $comp_info = $row;
        }
    }
}

function loadGoodSells($good_id) {
    global $conn, $good_sells;
    $good_sells = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' AND good_id = $good_id ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($good_sells, $row);
        }
    }
}

function loadEmpsActions() {
    loadEmployees();
    global $conn, $emp_actions, $employees;
    $emp_actions = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user['comp_id'];
    $emp_id = $user['id'];
    $sub_sql = "AND employee_id = $emp_id ";
    if (!hasAccessToSystem($user["job"]))
        $sub_sql = "";
    #it is for seller
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "sell";
            $row["title"] = "Продажа";
            $row["date"] = $row["sell_date"];
            $row["id"] = $row["sell_id"];
            array_push($emp_actions, $row);
        }
    }
    $sql = "SELECT * FROM returns WHERE comp_id='$comp_id' $sub_sql ORDER BY return_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "return";
            $row["title"] = "Возврат";
            $row["date"] = $row["return_date"];
            $row["id"] = $row["return_id"];
            array_push($emp_actions, $row);
        }
    }
    #it is for keeper
    $sql = "SELECT * FROM moves WHERE comp_id='$comp_id' $sub_sql ORDER BY move_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "move";
            $row["title"] = "Перемещение";
            $row["date"] = $row["move_date"];
            $row["store_id"] = $row["to_store_id"];
            $row["from_id"] = $row["from_store_id"];
            $row["id"] = $row["move_id"];
            array_push($emp_actions, $row);
        }
    }
    $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' $sub_sql ORDER BY debit_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "debit";
            $row["title"] = "Оприходование";
            $row["date"] = $row["debit_date"];
            $row["id"] = $row["debit_id"];
            array_push($emp_actions, $row);
        }
    }
    # load flow operations
    loadFlowCategories();
    global $cashflows;
    $sql = "SELECT * FROM cashflows WHERE comp_id='$comp_id' $sub_sql ORDER BY cashflow_date DESC ";
    $result = $conn->query($sql);
    //exit($result->num_rows);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "cashflow";
            $row["date"] = $row["cashflow_date"];
            $row["id"] = $row["cashflow_id"];
            if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 1)
                $row["title"] = "Приток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
            else if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 0)
                $row["title"] = "Отток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
            else
                $row["title"] = "Не известно";
            array_push($emp_actions, $row);
        }
    }

    usort($emp_actions, "cmpActions");
}

function loadSelectedEmpsActions() {
    loadEmployees();
    global $conn, $emp_actions, $employees, $selected_actions;
    //TODO_WRONG
    $selected_actions = 255;
    //exit();
    //showErrorView((string)$selected_actions);
    $emp_actions = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user['comp_id'];
    $emp_id = $user['id'];
    $sub_sql = " AND employee_id = $emp_id ";
    //if(hasAccessManage($user["job"]))
    $sub_sql = "";
    if ($selected_actions == 1 || $selected_actions == 255) {
        #it is for seller
        $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql ORDER BY sell_date DESC ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row["type"] = "sell";
                $row["title"] = "Продажа";
                $row["date"] = $row["sell_date"];
                $row["id"] = $row["sell_id"];
                $row["table"] = "sell";
                array_push($emp_actions, $row);
            }
        }
    }
    if ($selected_actions == 2 || $selected_actions == 255) {
        $sql = "SELECT * FROM returns WHERE comp_id='$comp_id' $sub_sql ORDER BY return_date DESC ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row["type"] = "return";
                $row["title"] = "Возврат";
                $row["date"] = $row["return_date"];
                $row["id"] = $row["return_id"];
                $row["table"] = "return";
                array_push($emp_actions, $row);
            }
        }
    }
    if ($selected_actions == 4 || $selected_actions == 255) {
        #it is for keeper
        $sql = "SELECT * FROM moves WHERE comp_id='$comp_id' $sub_sql ORDER BY move_date DESC ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row["type"] = "move";
                $row["title"] = "Перемещение";
                $row["date"] = $row["move_date"];
                $row["store_id"] = $row["to_store_id"];
                $row["from_id"] = $row["from_store_id"];
                $row["id"] = $row["move_id"];
                $row["table"] = "move";
                array_push($emp_actions, $row);
            }
        }
    }
    if ($selected_actions == 8 || $selected_actions == 255) {
        $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' $sub_sql ORDER BY debit_date DESC ";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row["type"] = "debit";
                $row["title"] = "Оприходование";
                $row["date"] = $row["debit_date"];
                $row["id"] = $row["debit_id"];
                $row["table"] = "debit";
                array_push($emp_actions, $row);
            }
        }
    }
    if ($selected_actions == 16 || $selected_actions == 255) {
        # load flow operations
        loadFlowCategories();
        global $cashflows;
        $sql = "SELECT * FROM cashflows WHERE comp_id='$comp_id' $sub_sql ORDER BY cashflow_date DESC ";
        $result = $conn->query($sql);
        //exit($result->num_rows);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row["table"] = "cashflow";
                $row["date"] = $row["cashflow_date"];
                $row["id"] = $row["cashflow_id"];
                if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 1){
                    $row["title"] = "Приток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
                    $row["type"] = "inflow";
                }
                else if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 0){
                    $row["title"] = "Отток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
                    $row["type"] = "outflow";
                }
                else
                    $row["title"] = "Не известно";
                array_push($emp_actions, $row);
            }
        }
    }
    usort($emp_actions, "cmpActions");
}

function loadNetSells(){
    loadEmployees();
    global $conn, $net_sells, $employees;
    $net_sells = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user['comp_id'];
    //if(hasAccessManage($user["job"]))
    //exit(0);
    $sql = "SELECT * FROM sells WHERE is_net_sell != 0 AND comp_id='$comp_id' ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "sell";
            $row["title"] = "Продажа";
            $row["date"] = $row["sell_date"];
            $row["id"] = $row["sell_id"];
            $row["table"] = "sell";
            array_push($net_sells, $row);
        }
    }
}

function loadEmpActions($emp_id) {
    if ($emp_id == 0) {
        loadSelectedEmpsActions();
        return;
    }
    loadEmployees();
    global $conn, $emp_actions, $employees;
    $emp_actions = array();
    $user = $employees[$emp_id];
    $comp_id = $user['comp_id'];
    $sub_sql = "AND employee_id = $emp_id ";
    # it is for seller
    $sql = "SELECT * FROM sells WHERE comp_id='$comp_id' $sub_sql ORDER BY sell_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "sell";
            $row["title"] = "Продажа";
            $row["date"] = $row["sell_date"];
            $row["id"] = $row["sell_id"];
            array_push($emp_actions, $row);
        }
    }
    $sql = "SELECT * FROM returns WHERE comp_id='$comp_id' $sub_sql ORDER BY return_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "return";
            $row["title"] = "Возврат";
            $row["date"] = $row["return_date"];
            $row["id"] = $row["return_id"];
            array_push($emp_actions, $row);
        }
    }
    #it is for keeper
    $sql = "SELECT * FROM moves WHERE comp_id='$comp_id' $sub_sql ORDER BY move_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "move";
            $row["title"] = "Перемещение";
            $row["date"] = $row["move_date"];
            $row["store_id"] = $row["to_store_id"];
            $row["from_id"] = $row["from_store_id"];
            $row["id"] = $row["move_id"];
            array_push($emp_actions, $row);
        }
    }
    $sql = "SELECT * FROM debits WHERE comp_id='$comp_id' $sub_sql ORDER BY debit_date DESC ";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "debit";
            $row["title"] = "Оприходование";
            $row["date"] = $row["debit_date"];
            $row["id"] = $row["debit_id"];
            array_push($emp_actions, $row);
        }
    }
    # load flow operations
    loadFlowCategories();
    global $cashflows;
    $sql = "SELECT * FROM cashflows WHERE comp_id='$comp_id' $sub_sql ORDER BY cashflow_date DESC ";
    //showErrorView($sql);
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $row["type"] = "cashflow";
            $row["date"] = $row["cashflow_date"];
            $row["id"] = $row["cashflow_id"];
            if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 1)
                $row["title"] = "Приток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
            else if ($cashflows[$row["cashflow_type_id"]]["flow_type"] == 0)
                $row["title"] = "Отток [" . $cashflows[$row["cashflow_type_id"]]["title"] . "]";
            else
                $row["title"] = "Не известно";
            array_push($emp_actions, $row);
        }
    }
    usort($emp_actions, "cmpActions");
}

function loadUserHistory($user_id) {
    global $conn, $history;
    $history = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sub_sql = " ";
    if ($user_id != 0)
        $sub_sql .= "AND emp_id = $emp_id ";
    $sql = "SELECT * FROM history WHERE comp_id='$comp_id' $sub_sql ORDER BY history_date DESC";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            array_push($history, $row);
        }
    }
}

function loadFlowCategories() {
    global $conn, $inflows, $outflows, $cashflows;
    $inflows = array();
    $outflows = array();
    $cashflows = array();
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $sql = "SELECT * FROM cashflow_types WHERE comp_id='$comp_id'";
    $result = $conn->query($sql);
    // $x["title"] = "Доход по умолчанию";
    // $x["description"] = "Приток денег на кассу по умолчанию";
    // $x["flow_type"] = 1;
    // $x["is_real"] = 1;
    // $x["comp_id"] = $comp_id;
    // $x["cashflow_type_id"] = 0;
    // $inflows[0] = $x;
    // $y["comp_id"] = $comp_id;
    // $y["cashflow_type_id"] = 0;
    // $y["title"] = "Расход по умолчанию";
    // $y["description"] = "Отток денег с кассы по умолчанию";
    // $y["flow_type"] = 0;
    // $y["is_real"] = 1;
    // $outflows[0] = $y;
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            if ($row['flow_type'] == 1)
                $inflows[$row["cashflow_type_id"]] = $row;
            else if ($row['flow_type'] == 0)
                $outflows[$row["cashflow_type_id"]] = $row;
            $cashflows[$row["cashflow_type_id"]] = $row;
        }
    }
}

function loadOperation($id, $table) {
    global $conn, $operation;
    $tables = $table . "s";
    $column = $table . "_id";
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    
    $sql = "SELECT * FROM $tables WHERE comp_id='$comp_id' AND $column = '$id'";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $operation = $row;
            $operation["type"] = $table;
            $operation["id"] = $id;
            if ($type == "move"){
                $operation["store_id"] = $operation["from_store_id"];
                $operation["date"] = $operation["move_date"];
            }
            else if($type == "sell")
                $operation["date"] = $operation["sell_date"];
            else if($type == "return")
                $operation["date"] = $operation["return_date"];
            else if($type == "debit")
                $operation["date"] = $operation["debit_date"];          
            else if($type == "cashflow")
                $operation["date"] = $operation["cashflow_date"];          
        }
    }
    else {
        return false;
    }
    return true;
}

# functions to convert from datetime to date(29/10/1995) or to time (17:48)

function toDate($date) {

    return date_format(date_create($date), "d/m/Y");
}

function toTime($date) {

    return date_format(date_create($date), "H:i:s");
}

# sorting functions

function cmpActions($a, $b) {
    if ($a["date"] > $b["date"])
        return -1;
    else if ($a["date"] < $b["date"])
        return 1;
    else
        return 0;
}
?>