<?php
#for changing location
//ob_start();
global $main_url;
# default footer and header

function getHeaderView() {
    global $main_url;
    ?>
    <header >
        <div class="container-fluid">
            <div class="row">
                <div class="navbar navbar-inverse" style="background-color: #000;">
                    <div class="container-fluid"  styles="border-radius:0px;">
                        <div class="navbar-header">
                            <a href="<?php echo $main_url; ?>all_goods.php" class="navbar-brand" >Все товары</a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#responsive-menu">
                                <span class="sr-only" >Open the navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="responsive-menu">
                            <ul class="nav navbar-nav">
                                <li><a href="<?php echo $main_url; ?>index.php"><span class="glyphicon glyphicon-user"></span></a></li>
                                <li><a href="<?php echo $main_url; ?>sell_good.php"><span class="glyphicon glyphicon-ok-sign"></span> Продать</a></li>
                                

                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li style="margin-top: 8px;"><select class="chosen-select" data-placeholder="Вы ищете..." 
                                            style="margin-top: 10px; width:15em" onchange="location.href=this.value">
                                    <option value="" disabled selected style='display:none;'>Вы ищете...</option>
                                    <?php
                                    
                                    loadStores();                                    
                                     
                                    loadGoods();
                                    global $stores,$goods;
                                    foreach ($goods as $key => $good) {
                                       echo "<option value ='" . "good.php?good_id=" . $good["good_id"] . "'>" . $good["name"] . " </option>";
                                    }  
                                    foreach ($stores as $key => $store) {
                                        echo "<option value ='" . "store.php?store_id=" . $store["store_id"] . "'>" . $store["name"] . " </option>";
                                    }
                                    ?>
                                    
                                    </select>
                                <li><a href="<?php echo $main_url; ?>search.php"><span class="glyphicon glyphicon-search"></span> </a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-tasks"></span> Операции <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo $main_url; ?>debit_good.php"><span class="glyphicon glyphicon-plus-sign"></span> Оприходовать товар</a></li>
                                        <li><a href="<?php echo $main_url; ?>return_good.php"><span class="glyphicon glyphicon-remove-sign"></span> Возврат товара</a></li>
                                        <li><a href="<?php echo $main_url; ?>move_good.php"><span class="glyphicon glyphicon-share-alt"></span> Перемещение товара</a></li>
                                        <li><a href="<?php echo $main_url; ?>add_good.php"><span class="glyphicon glyphicon-check"></span> Новый товар</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo $main_url; ?>income_operation.php"><span class="glyphicon glyphicon-plus-sign"></span> Приток на кассу</a></li>
                                        <li><a href="<?php echo $main_url; ?>outcome_operation.php"><span class="glyphicon glyphicon-minus-sign"></span> Отток с кассы </a></li>
                                        <li><a href="<?php echo $main_url; ?>add_flow_category.php"><span class="glyphicon glyphicon-check"></span> Создание операции с кассой</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo $main_url; ?>add_good_category.php"><span class="glyphicon glyphicon-check"></span> Новая категория товаров</a></li>
                                        <li><a href="<?php echo $main_url; ?>add_client.php"><span class="glyphicon glyphicon-check"></span> Добавить клиента </a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-list-alt"></span> Списки <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo $main_url; ?>list.php?type=operations"> Все операции</a></li>
                                        <li><a href="<?php echo $main_url; ?>custom_list.php"> Выборочные операции</a></li>
                                        <li><a href="<?php echo $main_url; ?>goods_sells.php"> Промежуточные продажи</a></li>
                                        <li><a href="<?php echo $main_url; ?>net_sells.php"> Интернет продажи</a></li>
                                        <li><a href="<?php echo $main_url; ?>residual_goods.php">Остатки</a></li>
                                        <li><a href="<?php echo $main_url; ?>operations_history.php"  role="button" aria-haspopup="true" aria-expanded="false">
                                         История </a></li>
                                        <li><a href="<?php echo $main_url; ?>company_rating.php?days=7&active=1"><s> Архив продаж</s></a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo $main_url; ?>list.php?type=accounts">Остатки по кассам</a></li>
                                        <li><a href="<?php echo $main_url; ?>list.php?type=cashflow_types"><s>Типы оттоков притоков</s></a></li>
                                        <li><a href="<?php echo $main_url; ?>list.php?type=categories"><s>Категории товаров</s></a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="<?php echo $main_url; ?>employees.php">Сотрудники</a></li>
                                        <li><a href="<?php echo $main_url; ?>debugs/debug_of_net_prices.php"><s>Ошибки</s></a></li>
    
                                    </ul>
                                </li>
                                <li><a href="<?php echo $main_url; ?>logout.php"><span class="glyphicon glyphicon-log-out"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div style="height:120px; width:100%;">
        .
    </div>
    <?php
}

function getFooterView() {
    ?>
    <footer >
        
        <br><hr><br>
        <div class="col-lg-1" style="float:right;"></div>
        <div class="col-lg-3" style="float:right;font-family:Comic Sans;font-size:16px">
            <p style=""><a href="http://www.vk.com/amankosss">Аманкос Рахымбергенов</a> </p>
            <p style="">amankosss@gmail.com</p>
            <p style="">+7 747 415-63-35 © 2015-2016</p>
            <br>	
        </div>
    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="chosen/docsupport/prism.js" type="text/javascript" charset="utf-8"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    <script src="chosen/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Не найдено!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    </script>
    <?php
}

# good.php

function getGoodInfoView() {
    global $good;
    $user = unserialize($_COOKIE["user"]);
    
    ?>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <img style="margin-top:20px; max-width:230px;" src="<?php echo $good["pic_url"]; ?>" alt="tovar Photo" class="img-thumbnail">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <div class="row">
                            <h1><?php echo $good["name"]; // print name of good  ?></h1>
                        </div>
                        <div class="row">
                            <p>Описание:</p>
                        </div>
                    </div>  
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                    </div>
                </div>
            </div>
            <div class="row">
                <p><?php echo $good["description"]; ?></p>
            </div>
        </div>
    </div>
    <br>
    <div style="width:100% ; text-align: center;">
        <div style="margin: 0 auto; display: inline-block;">
            <div class="good_info"><p style="float:left">Цена: <?php echo $good["price"] . "тг"; ?></p></div>
            <div class="good_info"><p style="float:left">В наличии: <?php echo getGoodCount($good["good_id"], 0) . " штук"; ?></p></div>
            <div class="good_info"><p style="float:left">Скидка: <?php echo $good["discount"] . "%"; ?></p></div>
            <div class="good_info"><p style="float:left">Бонус:  <?php echo $good["bonus"] . "%"; ?></p></div>
            <div class="good_info"><p style="float:left">Себестоимость:  <?php echo getGoodNetPrice($good["good_id"],$user["job"]) . "тг"; ?></p></div>
            <div class="good_info"><p style="float:left">ISBN:  <?php echo getGoodCode($good["good_id"]); ?>
                <button style="border-radius: 5px;padding:0px 4px 0 4px; margin-left: 5px; background-color: #444;">
                    <span class=' glyphicon glyphicon-pencil' onclick="if (confirm('Уверен, что хочешь изменить?'))
                        location.href = 'edit_operation.php<?php echo $operation_info; ?>';" 
                                    style='font-size: 10px; color: #fff; '></span></button></p></div>
        </div>
    </div>
    <?php
}

function getGoodStatisticsView() {
    loadStores();
    loadClients();
    global $good, $stores, $clients;
    ?>
    <div class="container table-responsive">      
        <div class="row"> 
            <div class="col-lg-4" styles="margin:0;">
                <h1 class="text-center">Остаток товара:</h1>
                <table class="my_table table table-striped table-bordered table-hover ">
                    <thead>
                        <tr class="top">
                            <td>№</td>
                            <td>Магазин</td>
                            <td>Количество</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $cnt = 0;
                        foreach ($stores as $key => $store) {
                            echo "<tr>";
                            echo "<td>" . ++$cnt . "</td>";
                            echo "<td><a href='" . $store["url"] . "'>" . $store["name"] . "</td>";
                            echo "<td>" . getGoodCount($good["good_id"], $store["store_id"]) . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-8" styles="margin:0;">
                <h1 class="text-center">Последние продажи:</h1>
                <table class="my_table table table-striped table-bordered table-hover ">
                    <thead>
                        <tr>
                            <td>№</td>
                            <td>Магазин</td>
                            <td>Покупатель</td>
                            <td>Дата</td>
                            <td>Время</td>
                            <td>Цена</td>
                            <td>Количество</td>
                            <td>Итого</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        loadGoodSells($good["good_id"]);
                        global $good_sells;
                        $cnt = 0;
                        foreach ($good_sells as $key => $good_sell) {
                            echo "<tr>";
                            echo "<td>" . ++$cnt . "</td>";
                            echo "<td><a href='" . $stores[$good_sell["store_id"]]["url"] . "'>" .
                            $stores[$good_sell["store_id"]]["name"] . "</td>";
                            echo "<td>" . $clients[$good_sell["client_id"]]["phone"] ." - " . $clients[$good_sell["client_id"]]["name"] . "</td>";
                            echo "<td>" . toDate($good_sell["sell_date"]) . "</td>";
                            echo "<td>" . toTime($good_sell["sell_date"]) . "</td>";
                            echo "<td>" . $good_sell["price"] . "</td>";
                            echo "<td>" . $good_sell["count"] . "</td>";
                            echo "<td>" . $good_sell["count"] * $good_sell["price"] . "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?php
}

# all_goods.php

function getAllGoodsView() {
    loadClients();
    loadGoods();
    loadStores();
    global $goods, $cat_id;
    ?>
    <div class="table" style="margin-left:10px;">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    $days = 7;
                    $table_headers = array("№", "Товар", "Цена", "Себестоимость", "Остаток", "Продано", "Возврат");
                    foreach ($table_headers as $key => $value) {
                        echo "<td>" . $value . "</td>";
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $page = 1;
                $user = unserialize($_COOKIE["user"]);
                if (isset($_GET['page']))
                    $page += $_GET['page'] * 100;
                $cnt = $page;
                foreach ($goods as $key => $good) {
                    if ($good["cat_id"] == $cat_id || $cat_id == -1) {
                        if ($cnt > 100+$page ) {
                            $cnt++;
                            continue;
                        }
                        echo " <tr>  ";
                        echo "<td>" . $cnt++ . "</td>";
                        echo "<td><a href='" . $good["url"] . "'>" . $good["name"] . "</a></td>";
                        echo "<td>" . $good["price"] . "</td>";
                        echo "<td>" . getGoodNetPrice($good["good_id"],$user["job"]) . "</td>";
                        echo "<td>" . getGoodCount($good["good_id"], 0) . "</td>";
                        echo "<td>" . getSpecificGoodCount("sells", $good["good_id"], 0) . "</td>";
                        echo "<td>" . getSpecificGoodCount("returns", $good["good_id"], 0) . "</td>";
                        echo " </tr>";
                    }
                }
                ?>
            </tbody>
        </table>
        <?php
        $pages = $cnt - 1;
        if ($pages > 100) {
            for ($i = 1; $i <= $pages; $i+=100) {
                $mx = min($i + 99, $pages);
                echo "<a href='#' >" . $i . "-" . $mx . "</a> ";
            }
        }
        ?>
    </div>
    <script type='text/javascript'>
    <?php
    $js_array = json_encode($goods);
    echo "var javascript_array = " . $js_array . ";\n";
    ?>
    </script>
    <?php
}

function getGoodsSellsView(){
    loadGoods();
    loadStores();
    global $goods, $stores,$categories;
    ?>
    <div class="table" style="margin-left:10px;">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    $days = 7;
                    $table_headers = array("№", "Категория", "Товар", "Препредыдущий месяц", "Предыдущий месяц", "Текущий месяц", "Остаток");
                    foreach ($table_headers as $key => $value) {
                        echo "<td>" . $value . "</td>";
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                $positive_goods = array();
                $negative_goods = array();
                foreach ($goods as $key => $good) {
                    $cur_good;
                    if(is_array($GLOBALS["selected_categories"]) && array_key_exists($good["cat_id"], $GLOBALS["selected_categories"])){
                        echo " <tr>  ";
                        echo "<td>" . $cnt++ . "</td>";
                        echo "<td>" . $categories[$good["cat_id"]]["title"] . "</td>";
                        echo "<td><a href='" . $good["url"] . "'>" . $good["name"] . "</a></td>";
                        echo "<td>" . calcGoodSells($good["good_id"], 2) . "</td>";
                        echo "<td>" . calcGoodSells($good["good_id"], 1) . "</td>";
                        echo "<td>" . calcGoodSells($good["good_id"], 0) . "</td>";
                        echo "<td>" . getGoodCount($good["good_id"], 0) . "</td>";
                        echo " </tr>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php

}
function getResidualGoodsView() {
    loadClients();
    loadGoods();
    loadStores();
    global $goods, $stores,$categories;
    ?>
    <div class="table" style="margin-left:10px;">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    $days = 7;
                    $table_headers = array("№", "Категория", "Товар");
                    if(isset($GLOBALS['in_way'])){
                        array_push($table_headers, 'В пути');
                    }
                    foreach ($stores as $key => $store) {
                        if(is_array($GLOBALS["selected_stores"]) && array_key_exists($store['store_id'], $GLOBALS["selected_stores"]))
                            array_push($table_headers, $store['name']);
                    }
                    array_push($table_headers, 'Всего');

                    foreach ($table_headers as $key => $value) {
                        echo "<td>" . $value . "</td>";
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                $positive_goods = array();
                $negative_goods = array();
                foreach ($goods as $key => $good) {
                    $cur_good;
                    if(is_array($GLOBALS["selected_categories"]) && array_key_exists($good["cat_id"], $GLOBALS["selected_categories"])){
                        $exist = false;
                        $stores_money = array();
                        foreach ($stores as $key => $store) {
                            if(isset($GLOBALS["selected_stores"]) && array_key_exists($store['store_id'], $GLOBALS["selected_stores"])){
                                $cc = getGoodCount($good["good_id"], $store["store_id"]);
                                if($cc>0)
                                    $exist = true;
                                array_push($stores_money,$cc);
                            }
                        }
                        if(!$exist && (!isset($GLOBALS['in_way']) || getGoodCountInWay($good["good_id"], 0) == 0) ) 
                                continue;
                        echo " <tr>  ";
                        echo "<td>" . $cnt++ . "</td>";
                        echo "<td>" . $categories[$good["cat_id"]]["title"] . "</td>";
                        echo "<td><a href='" . $good["url"] . "'>" . $good["name"] . "</a></td>";
                        if(isset($GLOBALS['in_way'])){
                            echo "<td>" . getGoodCountInWay($good["good_id"], 0) . "</td>";
                        }
                        for ($i = 0; $i< count($stores_money); $i++) {
                            echo "<td>" . $stores_money[$i] . "</td>";
                        }
                        echo "<td>" . getGoodCount($good["good_id"], 0) . "</td>";
                        echo " </tr>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}

function getCategoriesView() {
    loadCategories();
    loadGoods();
    global $categories, $cat_id, $goods, $page;
    ?>
    <div class="my_categories list-group" style="padding-top:20px;">
        <a href="<?php echo $page; ?>" class="list-group-item active" 	>
            Все товары
            <?php echo "<span class='badge'>" . count($goods) . "</span>"; ?>
        </a>
        <?php
        foreach ($categories as $key => $category) {
            $sub_str = "";
            if ($category["cat_id"] == $cat_id)
                $sub_str = " style='background-color:#FEE733;' ";
            echo "<a href='" . $page . "?cat_id=" . $category["cat_id"] . "' class='list-group-item' " . $sub_str . ">";
            echo $category["title"];
            if ($category["count"] > 0)
                echo "<span class='badge'>" . $category["count"] . "</span>";
            echo "</a>";
        }
        ?>
        <a href="add_good_category.php" class='list-group-item' style='background-color:#eee;'> Добавить категорию 
            <span class='badge'> +</span></a>
    </div>
    <?php
}

# company_rating.php

function getCompanySellsView($store_id) {
    loadGoods();
    loadClients();
    loadStores();
    global $stores, $days, $goods, $clients, $sells;
    loadSells($store_id, $days);
    ?>

    <div class="table">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    $table_headers = array("№", "Магазин", "Товар", "Покупатель", "Дата", "Время", "Цена", "Себестоимость", "Кол", "Итого");
                    foreach ($table_headers as $key => $value)
                        echo "<td>" . $value . "</td>";
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach ($sells as $key => $sell) {
                    $net_price = 0;
                    $user = unserialize($_COOKIE["user"]);
                    if(hasAccessToNetPrice($user["job"]))
                        $net_price = getGoodNetPriceExactByDate($sell["good_id"], $sell["sell_date"], $sell["count"]);
                    
                    echo "<tr>";
                    echo "<td>" . $cnt++ . "</td>";
                    echo "<td><a href='" . $stores[$sell["store_id"]]["url"] . "' >" .
                    $stores[$sell["store_id"]]["name"] . "</a></td>";
                    echo "<td><a href='" . $goods[$sell["good_id"]]["url"] . "' >" .
                    $goods[$sell["good_id"]]["name"] . "</a></td>";
                    echo "<td>" . $clients[$sell["client_id"]]["phone"] . " - " . $clients[$sell["client_id"]]["name"] . "</td>";
                    echo "<td>" . toDate($sell["sell_date"]) . "</td>";
                    echo "<td>" . toTime($sell["sell_date"]) . "</td>";
                    echo "<td>" . $sell["price"] . "</td>";
                    echo "<td>" . $net_price . "</td>";
                    echo "<td>" . $sell["count"] . "</td>";
                    echo "<td>" . $sell["count"] * $sell["price"] . "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}
function getCompanySellsByAccountIdView($account_id) {
    loadGoods();
    loadClients();
    loadAccounts();
    loadStores();
    global $stores, $accounts, $days, $goods, $clients, $sells;
    loadSellsByAccountId($account_id, $days);
    ?>

    <div class="table">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    $table_headers = array("№", "Магазин","Касса", "Товар", "Покупатель", "Дата", "Время", "Цена", "Кол", "Итого");
                    foreach ($table_headers as $key => $value)
                        echo "<td>" . $value . "</td>";
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach ($sells as $key => $sell) {
                    //$net_price = 0;
                    $user = unserialize($_COOKIE["user"]);
                    //if(hasAccessToNetPrice($user["job"]))
                        //$net_price = getGoodNetPriceExactByDate($sell["good_id"], $sell["sell_date"], $sell["count"]);
                    
                    echo "<tr>";
                    echo "<td>" . $cnt++ . "</td>";
                    echo "<td><a href='" . $stores[$sell["store_id"]]["url"] . "' >" .
                    $stores[$sell["store_id"]]["name"] . "</a></td>";
                    echo "<td><a href='" . $accounts[$sell["account_id"]]["url"] . "' >" .
                    $accounts[$sell["account_id"]]["title"] . "</a></td>";
                    echo "<td><a href='" . $goods[$sell["good_id"]]["url"] . "' >" .
                    $goods[$sell["good_id"]]["name"] . "</a></td>";
                    echo "<td>" . $clients[$sell["client_id"]]["phone"] . " - " . $clients[$sell["client_id"]]["name"] . "</td>";
                    echo "<td>" . toDate($sell["sell_date"]) . "</td>";
                    echo "<td>" . toTime($sell["sell_date"]) . "</td>";
                    echo "<td>" . $sell["price"] . "</td>";
                    //echo "<td>" . $net_price . "</td>";
                    echo "<td>" . $sell["count"] . "</td>";
                    echo "<td>" . $sell["count"] * $sell["price"] . "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}

# store.php

function getStoreInfoView() {
    loadCompany();
    global $store, $comp_info;
    $store_name = $store["name"];
    $src_icon = $store["pic_url"];
    $comp_name = $comp_info["name"];
    $store_phone = "87789158745";
    $money2 = calcStoreGoodsMoney($store["store_id"]);
    $money3 = $GLOBALS["store_goods_net_money"];
    if (is_null($store["pic_url"]) || $store["pic_url"] == "")
        $src_icon = "images/good.png";
    ?>
    <div class="row" style="padding:20px">
        <div class="col-lg-1"></div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div style="">
                <?php
                echo '<img  src="' . $src_icon . '" style="max-height:200px;" alt="Profile Photo" class="img-thumbnail">';
                ?>
            </div>        
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="row">
                <h2><?php echo $store_name ?></h2>
            </div>
            <div class="row">
                <p>Компания: <?php echo $comp_name; ?>   </p>
            </div>
            <div class="row">
                <p>Остаток книг в деньгах: <?php echo $money2; ?> тг </p>
            </div>
            <div class="row">
                <p>Остаток книг в деньгах(себ): <?php echo $money3; ?> тг </p>
            </div>
            <div class="row">
                <p>Телефон: <?php echo $store_phone; ?> </p>
            </div>
        </div>
    </div>
    <?php
}
function getAccountInfoView() {
    loadCompany();
    global $account, $comp_info;
    $account_name = $account["title"];
    $src_icon = $account["pic_url"];
    $comp_name = $comp_info["name"];
    $account_phone = "87789158745";
    $money = calcAccountMoney($account["id"]);
    if (is_null($store["pic_url"]) || $store["pic_url"] == "")
        $src_icon = "images/good.png";
    ?>
    <div class="row" style="padding:20px">
        <div class="col-lg-1"></div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div style="">
                <?php
                echo '<img  src="' . $src_icon . '" style="max-height:200px;" alt="Profile Photo" class="img-thumbnail">';
                ?>
            </div>        
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="row">
                <h2><?php echo $account_name ?></h2>
            </div>
            <div class="row">
                <p>Компания: <?php echo $comp_name; ?>   </p>
            </div>
            <div class="row">
                <p>Наличные: <?php echo $money; ?> тг </p>
            </div>
            <div class="row">
                <p>Телефон: <?php echo $account_phone; ?> </p>
            </div>
        </div>
    </div>
    <?php
}

# list.php

function getStoresView() {
    loadStores();
    global $stores;
    ?>
    <div class="table" style="margin-left:10px;">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    echo "<tr>";
                    $headers = array("№", "Магазин", "Адрес", "Деньги на кассе");
                    foreach ($headers as $key => $header)
                        echo "<td>" . $header . "</td>";
                    echo "</tr>";
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach ($stores as $key => $store) {
                    $title = "<a href='store.php?store_id=" . $store["store_id"] . "'>" . $store["name"] . "</a>";
                    echo "<tr>";
                    echo "<td>" . $cnt++ . "</td>";
                    echo "<td>" . $title . "</td>";
                    echo "<td>" . $store["address"] . "</td>";
                    echo "<td>" . calcStoreMoney($store["store_id"]) . "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}
function getAccountsView() {
    loadAccounts();
    global $accounts;
    ?>
    <div class="table" style="margin-left:10px;">
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <tr>
                    <?php
                    echo "<tr>";
                    $headers = array("№", "Касса", "Адрес", "Деньги на кассе");
                    foreach ($headers as $key => $header)
                        echo "<td>" . $header . "</td>";
                    echo "</tr>";
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                $cnt = 1;
                foreach ($accounts as $key => $account) {
                    $title = "<a href='account.php?account_id=" . $account["id"] . "'>" . $account["title"] . "</a>";
                    echo "<tr>";
                    echo "<td>" . $cnt++ . "</td>";
                    echo "<td>" . $title . "</td>";
                    echo "<td>" . $account["address"] . "</td>";
                    echo "<td>" . calcAccountMoney($account["id"]) . "</td>";
                    echo "</tr>";
                }
                ?>
            </tbody>
        </table>
    </div>
    <?php
}

# index.php

function getMyInfoView() {
    loadCompany();
    global $comp_info;
    ?>
    <div class="row">
        <div class="col-lg-3 col-lg-offset-1 col-md-4 col-sm-4 col-xs-4">
            <div style="">
                <?php
                $user = unserialize($_COOKIE["user"]);
                if (is_null($user["pic_url"]) ||
                        !isset($user["pic_url"]) ||
                        sizeof($user["pic_url"]) < 1) {
                    if ($user["gender"] == "M") {
                        $src_icon = "images/male2.png";
                    } else {
                        $src_icon = "images/female2.png";
                    }
                } else
                    $src_icon = $user["pic_url"];
                echo '<img  src="' . $src_icon . '" style="max-height:200px;" height="100" alt="Profile Photo" 
					class="img-thumbnail">';
                ?>
            </div>        
        </div>
        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-8">
            <div class="row">
                <h3><?php echo $user["surname"] . " " . $user["name"] . " " . $user["patronymic"] ?></h3>
            </div>
            <div class="row">
                <p>Компания: <?php echo $comp_info["name"]; ?>  </p>
            </div>
            <div class="row">
                <p>Должность: <?php echo $user["type"]; ?>  </p>
            </div>
            <div class="row">
                <p>Почта: <?php echo $user["email"]; ?> </p>
            </div>
                
            <?php 
                echo "Accesses:";
                if(hasAccessToSystem($user['job'])) 
                    echo "1 ";
                if(hasAccessToSell($user['job'])) 
                    echo "2 ";
                if(hasAccessToMove($user['job'])) 
                    echo "4 ";
                if(hasAccessToDebit($user['job'])) 
                    echo "8 ";
                if(hasAccessToEditAllActions($user['job'])) 
                    echo "16 ";
                if(hasAccessToNetPrice($user['job'])) 
                    echo "32 ";
                if(hasAccessToConfirmNetSells($user['job'])) 
                    echo "64 ";
                
            ?>
        </div>
    </div>
    <?php
}

function getMyActionsView() {
    $user = unserialize($_COOKIE["user"]);
    if (hasAccessToSystem($user["job"]))
        $emp_id = 0;
    else
        $emp_id = $user["id"];
    // ПОТОМ ПОСТАВИТЬ ЧЕКЕР ОБРАТНО
    $emp_id = 0;
    ?>
    <div class="table-responsive">
        <h2 class="text-center">Последние 100 операции:</h2>
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>	<?php getActionsViewHeaderFromArray($emp_id); ?>	</thead>
            <tbody>	<?php getActionsViewFromArray($emp_id); ?>	</tbody>
        </table>
    </div>
    <?php
}

function getSelectedActionsView() {
    // ПОТОМ ПОСТАВИТЬ ЧЕКЕР ОБРАТНО
    $emp_id = 0;
    ?>
    <div class="table">
        <h2 class="text-center">Действия за указанный период:</h2>
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>	<tr>
                    <?php
                    $headers = array("Тип операции", "Магазин", "Касса", "Сотрудник", "Категория","Товар", "Цена", "Дата", "Время", "Количество", "Отменить");
                    foreach ($headers as $key => $header) {
                        echo "<td>" . $header . "</td>";
                    }
                    ?>
                </tr></thead>
            <tbody>	<?php
                loadEmpActions($emp_id);
                loadStores();
                loadEmployees();
                loadGoods();
                loadCategories();
                loadAccounts();
                loadFlowCategories();
                global $emp_actions, $goods, $stores, $employees, $accounts,$cashflows;
                global $all_stores,$all_categories,$all_inflows,$all_outflows,$all_employees,$all_accounts;
                $user = unserialize($_COOKIE["user"]);
                if ($emp_id != 0)
                    $user = $employees[$emp_id];
                if(isset($_GET["to_date"]))// чтобы искал включая последний день
                    $to_date = $_GET["to_date"] . " 23:59:59";
                if(isset($_GET["stores"]))
                    $selected_stores = $_GET["stores"];
		if(isset($_GET["employees"]))
                    $selected_employees = $_GET["employees"];
		if(isset($_GET["inflows"]))
                    $selected_inflows = $_GET["inflows"];
		if(isset($_GET["outflows"]))
                    $selected_outflows = $_GET["outflows"];
                if(isset($_GET["accounts"]))
                    $selected_accounts = $_GET["accounts"];
                if(isset($_GET["categories"]))
                    $selected_categories = $_GET["categories"];
                
		global $total_sell_count, $total_sell_money, $total_sell_net_prices;
                global $total_net_sell_count, $total_net_sell_money, $total_net_sell_net_prices;
                global $total_move_count, $total_move_money, $total_move_net_prices;
                $total_sell_count = 0;
                $total_sell_money = 0;
                $total_sell_net_prices = 0;
                $total_net_sell_count = 0;
                $total_net_sell_money = 0;
                $total_net_sell_net_prices = 0;
                $total_move_count = 0;
                $total_move_money = 0;
                $total_move_net_prices = 0;
                $GLOBALS["total_prices"] = 0;
                foreach ($emp_actions as $key => $action) {
                    if ($action['type'] == "sell" && $action['is_net_sell'] == 0 && !isset($_GET["checkbox_sells"]) )
                        continue;
                    if ($action['type'] == "sell" && $action['is_net_sell'] != 0 && !isset($_GET["checkbox_net_sells"]))
                        continue;
                    if ($action['type'] == "return" && !isset($_GET["checkbox_returns"]))
                        continue;
                    if ($action['type'] == "move" && !isset($_GET["checkbox_moves"]))
                        continue;
                    if ($action['type'] == "debit" && !isset($_GET["checkbox_debits"]))
                        continue;
                    if ($action['type'] == "inflow" && !isset($_GET["checkbox_inflows"]))
                        continue;
                    if ($action['type'] == "outflow" && !isset($_GET["checkbox_outflows"]))
                        continue;
                    if (isset($_GET["from_date"]) && $action['date'] < $_GET["from_date"])
                        continue;
                    if (isset($_GET["to_date"]) && $action['date'] > $to_date)
                        continue;
                    if ($action['type'] == "inflow" && !isset($selected_inflows[$action['cashflow_type_id']]) && !isset($_GET["all_inflows"]))
                        continue;
                    if ($action['type'] == "outflow" && !isset($selected_outflows[$action['cashflow_type_id']]) && !isset($_GET["all_outflows"]))
                        continue;
                    if ( !isset($_GET["all_categories"]) && (!isset($selected_categories[$goods[$action["good_id"]]['cat_id']])) )
                        continue;
                    if ( !isset($_GET["all_stores"]) && !isset($selected_stores[$action['store_id']]))
                        continue;
                    if ( isset($action['account_id']) && !isset($_GET["all_accounts"]) && !isset($selected_accounts[$action['account_id']]))
                        continue;
                    if ( !isset($_GET["all_employees"]) && !isset($selected_employees[$action['employee_id']]))
                        continue;
                    $background = "";
                    if(isset($action["is_net_sell"]) && $action["is_net_sell"] != 0){
                        $title = "Интернет продажа";
                        if($action["is_net_sell"] == 2)
                            continue;
                            //$background = "background-color:#E0EDFF;";
                    }
                    if($action["type"] == "inflow" || $action["type"] == "outflow"){
                        if($cashflows[$action['cashflow_type_id']]["is_real"] == 1)
                            $background = "background-color:#E0FFED;";
                    }
                    if ($action["type"] == "sell" && $action['is_net_sell'] == 0){
                        $total_sell_count += $action["count"];
                        $total_sell_money += $action["count"] * getGoodRealPrice($action["price"],$action['discount'],$action['bonus']);
                        $total_sell_net_prices += $action["count"] * getGoodNetPriceExactByDate($action["good_id"],$action["date"],$action["count"]);
                    }
                    else if ($action["type"] == "sell"  && $action['is_net_sell'] == 1){
                        $total_net_sell_count += $action["count"];
                        $total_net_sell_money += $action["count"] * getGoodRealPrice($action["price"],$action['discount'],$action['bonus']);
                        $total_net_sell_net_prices += $action["count"] * getGoodNetPriceExactByDate($action["good_id"],$action["date"],$action["count"]);
                    }
                    else if ($action["type"] == "move"){
                        $total_move_count += $action["count"];
                        $total_move_money += $action["count"] * getGoodRealPrice($GLOBALS["goods"][$action["good_id"]]["price"],$action['discount'],$action['bonus']);
                        $total_move_net_prices += $action["count"] * getGoodNetPriceExactByDate($action["good_id"],$action["date"],$action["count"]);
                    }
                    $title = "Операция";
                    if (isset($action['title']))
                        $title = $action['title'];

                    $price = "-";
                    if (isset($action['price'])){
                        $price = $action['price'];
                        $GLOBALS["total_prices"] += $price;
                    }
                    else if (isset($goods[$action["good_id"]]["price"]))
                        $price = $goods[$action["good_id"]]["price"];

                    $count = "-";
                    if (isset($action['count']))
                        $count = $action['count'];
                    else if ($action['type'] == "cashflow")
                        $count = "-";

                    $date = toDate($action["date"]);
                    $time = toTime($action["date"]);

                    $category = "-";
                    if(isset($action["good_id"]))
                        $category = $GLOBALS["categories"][$goods[$action["good_id"]]['cat_id']]["title"];
                    
                    $good = "-";
                    if (isset($action["good_id"]) && isset($goods[$action["good_id"]]["name"]))
                        $good = "<a href='" . $goods[$action["good_id"]]["url"] . "' >" . $goods[$action["good_id"]]["name"] . "</a>";
                    else if ($action["table"] == "cashflow")
                        $good = $action['comment'];
                    
                    $employee = "-";
                    if (isset($employees[$action["employee_id"]]["name"]))
                        $employee = "<a href='employee.php?emp_id=" . $action["employee_id"] . "'>" .
                                $employees[$action["employee_id"]]["surname"] . " " . 
                                $employees[$action["employee_id"]]["name"] . " " .
                                //$employees[$action["employee_id"]]["patronymic"] .	
                                "</a>";

                    $store = "";
                    if ($action["type"] == "move" && isset($stores[$action["from_store_id"]]["url"]))
                        $store = "<a href='" . $stores[$action["from_store_id"]]["url"] . "'>" .
                                $stores[$action["from_id"]]["name"] . "</a> -> ";
                    if (isset($stores[$action["store_id"]]["name"]))
                        $store .= "<a href='" . $stores[$action["store_id"]]["url"] . "'>" . $stores[$action["store_id"]]["name"] . "</a>";
                    if ($store == "")
                        $store = "-";

                    $account = "-";
                    if (isset($action["account_id"]))
                        $account = "<a href='" . $accounts[$action["account_id"]]["url"] . "'>" . $accounts[$action["account_id"]]["title"] . "</a>";
                    
                    echo "<tr style=" . "'$background'" . ">";
                    echo "<td>" . $title . "</td>";
                    echo "<td>" . $store . "</td>";
                    echo "<td>" . $account . "</td>";
                    echo "<td>" . $employee . "</td>";
                    echo "<td>" . $good . "</td>";
                    echo "<td>" . $category . "</td>";
                    echo "<td>" . $price . "</td>";
                    echo "<td>" . $date . "</td>";
                    echo "<td>" . $time . "</td>";
                    echo "<td>" . $count . "</td>";
                    $operation_info = "?table=" . $action["table"] . "&type=" . $action["type"] . "&id=" . $action["id"];
                    ?>
                <!-- old button 
                    <td><button class='myButton'>
                        <span class='glyphicon glyphicon-pencil' onclick="if (confirm('Уверен, что хочешь изменить?'))
                                    location.href = 'edit_operation.php<?php echo $operation_info; ?>';" style='font-size: 20px;'></span>
                    </button>
                </td> 
                    <button class='myButton' type="button" onclick="if (confirm('Уверен, что хочешь удалить?'))
                            location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                        <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
                    </button>
                    -->
                <td>
                    <button class='myButton' type="button" onclick="location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                        <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
                    </button>
                </td> 
                </tr>
            <?php } 
            
            ?>
            </tbody>
        </table>
    </div>
    <?php
}

# tabs for period of time and form of period of time

function getTabs($active_day, $link) {
    global $active;
    $_days = array(1, 7, 30, 90, 365, 0, 0);
    $tab_text = array("Сегодня", "Неделя", "Месяц", "Квартал", "Год", "Весь период", "Выбрать период");
    echo '<div class="my_tabs range_tabs" >';
    echo '<ul class="nav nav-tabs">';
    for ($i = 0; $i < 7; $i++) {
        echo '<li ';
        if ($active_day == $i)
            echo ' class=active ';
        echo' ><a href="' . $link . 'days=' . $_days[$i] . '&active=' . $i . '">' . $tab_text[$i] . '</a></li>';
    }
    echo "</ul>";
    echo "</div>";
    if ($active == 6) {
        ?>
        <form action="action_page.php">
            <input type="date" name="bday">
            <input type="submit">
        </form><?php
    }
}

# authorization views

function displayErrorsView($messages) {
    print("<b>Возникли следующие ошибки:</b>\n<ul>\n");

    foreach ($messages as $msg) {
        print("<li>$msg</li>\n");
    }
    print("</ul>\n");
}

function getAuthFormView() {
    ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-4 col-lg-offset-4" >
        <div class="form-group">
            <label for="inputEmail">Почта</label>
            <input type="email" name="login"
                   value="<?php print isset($_POST["login"]) ? $_POST["login"] : "" ; ?>"
                   maxlength="30" class="form-control" id="inputEmail" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="inputPassword">Пароль</label>
            <input type="password" name="password" value="" maxlength="15" class="form-control" id="inputPassword" placeholder="Password">
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="is_long"> Запомнить пароль на неделю </label>
        </div>
        <button name="submit" type="submit" class="btn btn-primary">Войти</button>
    </form> <?php
}



function getUserInfoView($emp_id) {
    loadCompany();
    loadEmployees();
    global $comp_info, $employees;
    $user = unserialize($_COOKIE["user"]);
    if ($emp_id != 0)
        $user = $employees[$emp_id];
    ?>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <div style="">
                <?php
                if (is_null($user["pic_url"]) ||
                        !isset($user["pic_url"]) ||
                        sizeof($user["pic_url"]) < 1) {
                    if ($user["gender"] == "M") {
                        $src_icon = "images/male2.png";
                    } else {
                        $src_icon = "images/female2.png";
                    }
                } else
                    $src_icon = $user["pic_url"];
                echo '<img  src="' . $src_icon . '" style="max-height:200px;" height="100" alt="Profile Photo" 
					class="img-thumbnail">';
                ?>
            </div>        
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <div class="row">
                <h3><?php echo $user["surname"] . " " . $user["name"] . " " . $user["patronymic"] ?></h3>
            </div>
            <div class="row">
                <p>Компания: <?php echo $comp_info["name"]; ?>  </p>
            </div>
            <div class="row">
                <p>Должность: <?php echo $user["type"]; ?>  </p>
            </div>
            <div class="row">
                <p>Почта: <?php echo $user["email"]; ?> </p>
            </div>
        </div>
    </div>
    <?php
}

function getUserActionsView($emp_id) {
    ?>
    <div class="table-responsive">
        <h2 class="text-center">Последние действия:</h2>
        <table class="my_table table table-striped table-bordered table-hover ">
            <thead>
                <?php getActionsViewHeaderFromArray($emp_id); ?>
            </thead>
            <tbody>
                <?php getActionsViewFromArray($emp_id); ?>
            </tbody>
        </table>
    </div>
    <?php
}

function getActionsViewHeaderFromArray() {
    echo "<tr>";
    $headers = array("№","Тип операции", "Магазин", "Касса", "Сотрудник", "Товар", "Цена", "Дата", "Время", "Количество", "Отменить");
    //if(checkAccessToEmp($emp_id))
    foreach ($headers as $key => $header) {
        echo "<td>" . $header . "</td>";
    }
    echo "</tr>";
}

function getActionsViewFromArray($emp_id) {
    loadEmpActions($emp_id);
    loadStores();
    loadAccounts();
    loadGoods();
    loadFlowCategories();
    global $accounts, $emp_actions, $goods, $stores, $employees, $cashflows;
    $user = unserialize($_COOKIE["user"]);
    if ($emp_id != 0)
        $user = $employees[$emp_id];
    $cnt = 1;
    foreach ($emp_actions as $key => $action) {
        if($cnt > 100) break;
                    
        $title = "Операция";
        if (isset($action['title']))
            $title = $action['title'];

        $price = "-";
        if (isset($action['price']))
            $price = $action['price'];
        else if (isset($goods[$action["good_id"]]["price"]))
            $price = $goods[$action["good_id"]]["price"];

        $count = "-";
        if (isset($action['count']))
            $count = $action['count'];
        else if ($action['type'] == "cashflow")
            $count = "-";

        $date = toDate($action["date"]);
        $time = toTime($action["date"]);

        $good = "-";
        if (isset($action["good_id"]) && isset($goods[$action["good_id"]]["name"]))
            $good = "<a href='" . $goods[$action["good_id"]]["url"] . "' >" . $goods[$action["good_id"]]["name"] . "</a>";
        else if ($action["type"] == "cashflow")
            $good = "Деньги [" . $action['comment'] . "]";

        $employee = "-";
        if (isset($employees[$action["employee_id"]]["name"]))
            $employee = "<a href='employee.php?emp_id=" . $action["employee_id"] . "'>" .
                    //$employees[$action["employee_id"]]["surname"] . " " . 
                    $employees[$action["employee_id"]]["name"] . " " .
                    //$employees[$action["employee_id"]]["patronymic"] .	
                    "</a>";

        $store = "";
        if ($action["type"] == "move" && isset($stores[$action["from_store_id"]]["url"]))
            $store = "<a href='" . $stores[$action["from_store_id"]]["url"] . "'>" .
                    $stores[$action["from_id"]]["name"] . "</a> -> ";
        if (isset($stores[$action["store_id"]]["name"]))
            $store .= "<a href='" . $stores[$action["store_id"]]["url"] . "'>" . $stores[$action["store_id"]]["name"] . "</a>";
        if ($store == "")
            $store = "-";
        
        $account = "-";
        if (isset($accounts[$action["account_id"]]["title"]))
            $account = "<a href='" . $accounts[$action["account_id"]]["url"] . "'>" . $accounts[$action["account_id"]]["title"] . "</a>";
        
        $background = "";
        if(isset($action["is_net_sell"]) && $action["is_net_sell"] != 0){
            $title = "Интернет продажа";
            if($action["is_net_sell"] == 2)
                    $background = "background-color:#E0EDFF;";
        }
        if($action["type"] == "inflow" || $action["type"] == "outflow"){
            if($cashflows[$action['cashflow_type_id']]["is_real"] == 1)
                $background = "background-color:#E0FFED;";
        }
        
        echo "<tr style=" . "'$background'" . ">";
        echo "<td>" . $cnt++ . "</td>";
        echo "<td>" . $title . "</td>";
        echo "<td>" . $store . "</td>";
        echo "<td>" . $account . "</td>";
        echo "<td>" . $employee . "</td>";
        echo "<td>" . $good . "</td>";
        echo "<td>" . $price . "</td>";
        echo "<td>" . $date . "</td>";
        echo "<td>" . $time . "</td>";
        echo "<td>" . $count . "</td>";
        $operation_info = "?table=" . $action["table"] . "&type=" . $action["type"] . "&id=" . $action["id"];
        ?>
       <!-- <td><button class='myButton'>
                <span class='glyphicon glyphicon-pencil' onclick="if (confirm('Уверен, что хочешь изменить?'))
                            location.href = 'edit_operation.php<?php echo $operation_info; ?>';" style='font-size: 20px;'></span>
            </button>
        </td> -->
                <!-- old button 
                <button class='myButton' type="button" onclick="if (confirm('Уверен, что хочешь удалить?'))
                        location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                    <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
                </button>
                -->
        <td>
            <button class='myButton' type="button" onclick="location.href = 'delete_operation.php<?php echo $operation_info; ?>';" >
                <span class='glyphicon glyphicon-trash' style='font-size: 20px;'></span>
            </button>
        </td> 
        </tr><?php
    }
}

# operations with good

function debitGoodFormView() {
    ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
        <div class="form-group" id="input_store">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Склад:</label>
            <div class="col-sm-8 col-lg-8 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="store_id" >
                    <?php
                    loadStores();
                    global $stores;
                    $user = unserialize($_COOKIE["user"]);
                    foreach ($stores as $key => $store) {
                        $selected = "";
                        if($user['store_id'] == $store['store_id'])
                            $selected = "selected";
                        echo "<option " . $selected . " value ='" . $store["store_id"] . "' >" . $store["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_good">
            <label for="good_id" class="col-sm-2 col-lg-2 control-label">Товар:</label>
            <div class="col-sm-8 col-lg-8 col-lg-offset-1">
                <select data-placeholder="Выберите товар" class="chosen-select form-control" id="good_id" name="good_id">
                    <?php
                    loadGoods();
                    global $goods;
                    foreach ($goods as $key => $good) {
                        echo "<option value ='" . $good["good_id"] . "' >" . $good["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group " id = "input_count">
            <label for="good_count" class="col-sm-2 col-lg-2 control-label">Количество:</label>
            <div class="col-sm-8 col-lg-8 col-lg-offset-1">
                <input type="number" class="form-control" placeholder="По умолчанию" id="good_count" name="good_count" value="0">
            </div>
        </div>
        <? if(hasAccessToNetPrice($user['job'])){ 
        ?>
        <div class="form-group " id = "input_net_price">
            <label for="good_net_price" class="col-sm-2 col-lg-2 control-label">Себестоимость:</label>
            <div class="col-sm-8 col-lg-8 col-lg-offset-1">
                <input type="number" class="form-control" id="good_net_price" name="good_net_price" placeholder="По умолчанию">
            </div>
        </div>
        <? } ?>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Оприходовать</button>
            </div>
        </div>
    </form> <?php
}

function returnGoodFormView() {
    ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
        <div class="form-group" id="input_store">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Магазин:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="store_id" >
                    <?php
                    loadStores();
                    global $stores;
                    $user = unserialize($_COOKIE["user"]);
                    foreach ($stores as $key => $store) {
                        $selected = "";
                        if($user['store_id'] == $store['store_id'])
                            $selected = "selected";
                        echo "<option " . $selected . " value ='" . $store["store_id"] . "' >" . $store["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_good">
            <label for="good_id" class="col-sm-2 col-lg-2 control-label">Товар:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_id" name="good_id">
                    <?php
                    loadGoods();
                    global $goods;
                    foreach ($goods as $key => $good) {
                        echo "<option value ='" . $good["good_id"] . "' >" . $good["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group " id = "input_count">
            <label for="good_count" class="col-sm-2 col-lg-2 control-label">Количество:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <input type="number" class="form-control" id="good_count" name="good_count" value="0">
            </div>
        </div>
        <div class="form-group" id = "input_client">
            <label for="client_id" class="col-sm-2 col-lg-2 control-label">Номер клиента:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="client_id" name="client_id">
                    <?php
                    loadClients();
                    global $clients;
                    foreach ($clients as $key => $client) {
                        echo "<option value ='" . $client["client_id"] . "' >" . $client["phone"] . " - " . $client["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_reason">
            <label for="reason" class="col-sm-2 col-lg-2 control-label">Причина:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <input type="text" class="form-control" id="reason" name="reason" placeholder="...">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Возврат</button>
            </div>
        </div>
    </form> <?php
}

function moveGoodFormView() {
    ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
        <div class="form-group" id="input_from_store">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Откуда: </label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="from_store_id" >
                    <?php
                    loadStores();
                    global $stores;
                    $user = unserialize($_COOKIE["user"]);
                    foreach ($stores as $key => $store) {
                        $selected = "";
                        if($user['store_id'] == $store['store_id'])
                            $selected = "selected";
                        echo "<option " . $selected . " value ='" . $store["store_id"] . "' >" . $store["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id="input_to_store">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Куда: </label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="to_store_id" >
                    <?php
                    loadStores();
                    global $stores;
                    foreach ($stores as $key => $store) {
                        echo "<option value ='" . $store["store_id"] . "' >" . $store["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Товар:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="good_id">
                    <?php
                    loadGoods();
                    global $goods;
                    foreach ($goods as $key => $good) {
                        echo "<option value ='" . $good["good_id"] . "' >" . $good["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_count">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Количество:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <input type="number" class="form-control" name="good_count" value="0">
            </div>
        </div>
        <div class="form-group" >
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Переместить</button>
            </div>
        </div>
    </form> <?php
}

function sellGoodFormView() {
    ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
        <div class="form-group" id="input_store">
            <label for="good_store" class="col-sm-2 col-lg-2 control-label">Магазин:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_store" name="store_id" >
                    <?php
                    loadStores();
                    global $stores;
                    $user = unserialize($_COOKIE["user"]);
                    foreach ($stores as $key => $store) {
                        $selected = "";
                        if($user['store_id'] == $store['store_id'])
                            $selected = "selected";
                        echo "<option " . $selected . " value ='" . $store["store_id"] . "' >" . $store["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id="input_account">
            <label class="col-sm-2 col-lg-2 control-label">Касса:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="sell_account" name="account_id" >
                    <?php
                    loadAccounts();
                    global $accounts;
                    foreach ($accounts as $key => $account) {
                        $selected = "";
                        if($user['account_id'] == $account['id'])
                            $selected = "selected";
                        echo "<option " . $selected . " value ='" . $account["id"] . "' >" . $account["title"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_good">
            <label for="good_id" class="col-sm-2 col-lg-2 control-label">Товар:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="good_id" name="good_id">
                    <?php
                    loadGoods();
                    global $goods;
                    foreach ($goods as $key => $good) {
                        echo "<option value ='" . $good["good_id"] . "' >" . $good["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group " id = "input_count">
            <label for="good_count" class="col-sm-2 col-lg-2 control-label">Количество:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <input type="number" class="form-control" id="good_count" name="good_count" value="0">
            </div>
        </div>
        <div class="form-group" id = "input_comment">
            <label class="col-sm-2 col-lg-2 control-label">Комментарии:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <input type="text" class="form-control" id="reason" name="comment" placeholder="...">
            </div>
        </div>
        <div class="form-group" id = "input_client">
            <label for="client_id" class="col-sm-2 col-lg-2 control-label">Номер клиента:</label>
            <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                <select class="chosen-select form-control" id="client_id" name="client_id">
                    <?php
                    loadClients();
                    global $clients;
                    $cnt = 1;
                    foreach ($clients as $key => $client) {
                        echo "<option value ='" . $client["client_id"] . "' >" . $cnt++ . ") " . $client["phone"]. " - " .$client["name"] . " </option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group" id = "input_internet">
            <div class="col-sm-9 col-lg-9 col-lg-offset-3">
                <p style="font-size:15px;"><input type="checkbox" name="is_net_sell" />
                Интернет продажа</p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Продать</button>
            </div>
        </div>
    </form> <?php
                }

function addGoodFormView() {
                    ?>
    <script>
        function getName(str) {
            if (str.lastIndexOf('\\')) {
                var i = str.lastIndexOf('\\') + 1;
            } else {
                var i = str.lastIndexOf('/') + 1;
            }
            var filename = str.slice(i);
            var uploaded = document.getElementById("fileformlabel");
            uploaded.innerHTML = filename;
        }
    </script>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" enctype="multipart/form-data" 
          class="col-lg-offset-3 col-lg-5 form-horizontal">
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Категория:</label>
            <select class="chosen-select form-control" name="cat_id">
                <?php
                loadCategories();
                global $categories;
                foreach ($categories as $key => $cat) {
                    echo "<option value ='" . $cat["cat_id"] . "' > " . $cat["title"] . " </option>";
                }
                ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Закачайте картинку:</label>
            <div class="fileform">
                <div id="fileformlabel"></div>
                <button class="selectbutton btn btn-primary">Обзор</button>
                <input type="file" name="good_img" id="upload" onchange="getName(this.value);" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Штрих-код:</label>
            <input type="text" class="form-control" name="good_code" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Название товара:</label>
            <input type="text" class="form-control" name="good_name" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Описание товара:</label>
            <input type="text" class="form-control" name="good_description" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Цена:</label>
            <input type="number" class="form-control" name="good_price" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Себестоимость:</label>
            <input type="number" class="form-control" name="good_net_price" value="0" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Бонус в процентах(%):</label>
            <input type="number" class="form-control" name="good_bonus" value="0" min="0" max="100">
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Скидка в процентах(%):</label>
            <input type="number" class="form-control" name="good_discount" value="0" min="0" max="100">
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </div>
    </form> <?php
            }

            function addClientFormView() {
                ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" enctype="multipart/form-data" 
          class="col-lg-offset-3 col-lg-5 form-horizontal">
        <input type="hidden" name="post_operation" value="ADD_CLIENT"/>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Имя:</label>
            <input type="text" class="form-control" name="client_name" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Фамилия:</label>
            <input type="text" class="form-control" name="client_surname" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">E-mail:</label>
            <input type="text" class="form-control" name="client_email" >
        </div>
        <div class="form-group">
            <label class="control-label" style="margin-bottom:10px;">Тел:</label>
            <input type="number" class="form-control" name="client_phone" >
        </div>
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <button type="submit" class="btn btn-primary">Добавить клиента</button>
            </div>
        </div>
    </form> <?php
          }

          function getAddGoodCategoryFormView() {
              ?>
    <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" enctype="multipart/form-data" 
          class="col-lg-offset-3 col-lg-5 form-horizontal"> <?php
              getSimpleFormGroup("input_cat_name", "Название категории:", "cat_name", "text", "");
              getSimpleFormGroup("", "Описание категории:", "cat_description", "text", "");
              getButtonFormGroup("col-lg-offset-3 col-lg-9", "Добавить категорию товаров", " btn-primary ");
              echo "</form>";
          }

          function getDeleteOperationFormView() {
              if (isset($_GET["id"]) && isset($_GET["table"]) && loadOperation($_GET["id"], $_GET["table"])) {
                  $id = $_GET["id"];
                  $table = $_GET["table"];
                  setcookie('SAVED_PAGE', $_SERVER["HTTP_REFERER"], 0, '/');   
              } else if (!isset($_POST["operation_id"])) {
                  showErrorView("Не правильно выбран товар для удаления");
                  return;
              }
              global $operation;
              getOperationMainInfoForm($operation, false);
          }

          function getOperationMainInfoForm($operation, $is_edit) {
              $type;
              switch ($operation["table"]) {
                  case "sell":
                      $type = "Продажа";
                      break;
                  case "return":
                      $type = "Возврат";
                      break;
                  case "debit":
                      $type = "Оприходование";
                      break;
                  case "move":
                      $type = "Перемещение";
                      break;
                  case "inflow":
                      $type = "Приток";
                      break;
                  case "outflow":
                      $type = "Отток";
                      break;
                  case "cashflow":
                      $type = "Приток/Отток";
                      break;
                  default:
                      $type = "Не известно";
                      break;
              }
              ?>
        <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" enctype="multipart/form-data" 
              class="col-lg-offset-3 col-lg-5 form-horizontal">
            <h2><?php echo "Операция №" . $operation['id']; ?></h2><?php
            loadGoods();
            loadEmployees();
            loadStores();
            loadClients();
            global $goods, $employees, $stores, $clients;
            $good_info = "";
            $user = unserialize($_COOKIE["user"]);
            if ($operation["type"] != "cashflow")
                $good_info = " товар '" . $goods[$operation['good_id']]["name"] . "', ";
            
            $history = "Удалена операция типа: '" . $type .
                    "' сотрудником '" . $employees[$user["id"]]["surname"] . ' ' . $employees[$user["id"]]["name"] . " " . $employees[$user["id"]]["patronymic"] .
                    "'. Информация об удаленной операции: ".
                    $good_info .
                    "сотрудник '" . $employees[$operation['employee_id']]["surname"] . " " . $employees[$operation['employee_id']]["name"] . " " . $employees[$operation['employee_id']]["patronymic"] . "'," .
                    " магазин '" . $stores[$operation['store_id']]["name"] . "'";
            if ($operation["type"] == "move")
                $history .= ", куда(магазин) '" . $stores[$operation['to_store_id']]["name"] . "'";
            # клиент - продажа и возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                $history .= ", клиент '" . $clients[$operation['client_id']]["phone"] . " - ". $clients[$operation['client_id']]["name"] . "'";
            # цена операции - продажа, возврат, приток, отток
            if ($operation["type"] == "sell" || $operation["type"] == "return" || $operation["type"] == "outflow" || $operation["type"] == "inflow")
                $history .= ", цена '" . $operation['price'] . "'";
            # количество товара - продажа, возврат, перемещения , оприходование
            if ($operation["type"] == "sell" || $operation["type"] == "return" || $operation["type"] == "move" || $operation["type"] == "debit")
                $history .= ", количество '" . $operation['count'] . "'";
            # скидка - продажа, возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                $history .= ", скидка '" . $operation['discount'] . "%'";
            # бонус - продажа, возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                $history .= ", бонус '" . $operation['bonus'] . "%'";
            # причина - возврат
            if ($operation["type"] == "return")
                $history .= ", причина '" . $operation['reason'] . "'";
            $history .= ", дата '" . toDate($operation['date']) . "  " . toTime($operation['date']) . "'.";
            //exit($history);
            ?> 
            <input class="form-control" type="hidden" name="operation_id" value="<?php echo $operation['id']; ?>">
            <input class="form-control" type="hidden" name="operation_comment_id" value="<?php echo $history; ?>">
            <input class="form-control" type="hidden" name="operation_title_id" value="<?php echo $operation['type']; ?>">

            <?php
            # тип операции - все типы операции
            getNotEditableFormGroup("Тип операции:", "operation_category_id", $type, "");
            # название товара - все типы операции кроме притока и оттока
            if ($operation["type"] != "cashflow")
                getNotEditableFormGroup("Товар:", "good_id", $goods[$operation['good_id']]["name"], "good.php?good_id=" . $operation['good_id']);
            # имя сотрудника - все типы операции
            getNotEditableFormGroup("Сотрудник:", "employee_id", $employees[$operation['employee_id']]["name"], "employee.php?emp_id=" . $operation['employee_id']);
            # название магазина - все типы операции
            getNotEditableFormGroup("Магазин:", "store_id", $stores[$operation['store_id']]["name"], "store.php?store_id=" . $operation["store_id"]);
            # название магазина куда - перемещения
            if ($operation["type"] == "move")
                getNotEditableFormGroup("Куда:", "to_store_id", $stores[$operation['to_store_id']]["name"], "store.php?store_id=" . $operation["to_store_id"]);
            # дата операции - все типы операции
            getNotEditableFormGroup("Дата:", "date_id", toDate($operation['date']) . "  " . toTime($operation['date']), "");
            # клиент - продажа и возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                getNotEditableFormGroup("Клиент:", "client_id", $clients[$operation['client_id']]["name"], "");
            # цена операции - продажа, возврат, приток, отток
            if ($operation["type"] == "sell" || $operation["type"] == "return" || $operation["type"] == "outflow" || $operation["type"] == "inflow")
                getNotEditableFormGroup("Цена:", "price_id", $operation['price'], "");
            # количество товара - продажа, возврат, перемещения , оприходование
            if ($operation["type"] == "sell" || $operation["type"] == "return" || $operation["type"] == "move" || $operation["type"] == "debit")
                getNotEditableFormGroup("Количество:", "count_id", $operation['count'], "");
            # скидка - продажа, возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                getNotEditableFormGroup("Скидка:", "discount_id", $operation['discount'] . "%", "");
            # бонус - продажа, возврат
            if ($operation["type"] == "sell" || $operation["type"] == "return")
                getNotEditableFormGroup("Бонус:", "bonus_id", $operation['bonus'] . "%", "");
            # причина - возврат
            if ($operation["type"] == "return")
                getNotEditableFormGroup("Причина:", "reason_id", $operation['reason'], "");
            ?>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <button type="submit" class="btn btn-danger">Удалить операцию</button>
                </div>
            </div>
        </form>
        <?php
    }

# operations with flow 

    function getFinanceIncomeOperationFormView() {
        ?>
        <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
            <div class="form-group" id="input_account">
                <label for="good_account" class="col-sm-2 col-lg-2 control-label">Касса:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <select class="chosen-select form-control" id="good_account" name="account_id" >
                        <?php
                        loadAccounts();
                        global $accounts;
                        $user = unserialize($_COOKIE["user"]);
                        foreach ($accounts as $key => $account) {
                            $selected = "";
                            if($user['account_id'] == $account['id'])
                                $selected = "selected";
                            echo "<option " . $selected . " value ='" . $account["id"] . "' >" . $account["title"] . " </option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group" id = "input_good">
                <label for="flow_id" class="col-sm-2 col-lg-2 control-label">Тип притока:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <select class="chosen-select form-control" id="flow_id" name="flow_id">
                        <?php
                        loadFlowCategories();
                        global $inflows;
                        foreach ($inflows as $key => $inflow) {
                            echo "<option value ='" . $inflow["cashflow_type_id"] . "' >" . $inflow["title"] . " </option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group " id = "input_price">
                <label for="flow_price" class="col-sm-2 col-lg-2 control-label">Сумма:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <input type="number" step="any" class="form-control" id="flow_price" name="flow_price" value="0">
                </div>
            </div>
            <div class="form-group" id = "input_comment">
                <label for="comment" class="col-sm-2 col-lg-2 control-label">Комментарии:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <input type="text" class="form-control" id="comment" name="comment" placeholder="...">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <button type="submit" class="btn btn-primary">Выполнить приток на кассу</button>
                </div>
            </div>
        </form> <?php
                    }

                    function getFinanceOutcomeOperationFormView() {
                        ?>
        <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" class="col-lg-offset-3 col-lg-5 form-horizontal">
            <div class="form-group" id="input_account">
                <label for="good_account" class="col-sm-2 col-lg-2 control-label">Касса:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <select class="chosen-select form-control" id="good_account" name="account_id" >
                        <?php
                        loadAccounts();
                        global $accounts;
                        $user = unserialize($_COOKIE["user"]);
                        foreach ($accounts as $key => $account) {
                            $selected = "";
                            if($user['account_id'] == $account['id'])
                                $selected = "selected";
                            echo "<option " . $selected . " value ='" . $account["id"] . "' >" . $account["title"] . " </option>";
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-group" id = "input_good">
                <label for="flow_id" class="col-sm-2 col-lg-2 control-label">Тип оттока:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <select class="chosen-select form-control" id="flow_id" name="flow_id">
    <?php
    loadFlowCategories();
    global $outflows;
    foreach ($outflows as $key => $outflow) {
        echo "<option value ='" . $outflow["cashflow_type_id"] . "' >" . $outflow["title"] . " </option>";
    }
    ?>
                    </select>
                </div>
            </div>
            <div class="form-group " id = "input_price">
                <label for="flow_price" class="col-sm-2 col-lg-2 control-label">Сумма:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <input type="number" class="form-control" id="flow_price" name="flow_price" value="0">
                </div>
            </div>
            <div class="form-group" id = "input_comment">
                <label for="comment" class="col-sm-2 col-lg-2 control-label">Комментарии:</label>
                <div class="col-sm-10 col-lg-9 col-lg-offset-1">
                    <input type="text" class="form-control" id="comment" name="comment" placeholder="...">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <button type="submit" class="btn btn-primary">Выполнить отток с кассы</button>
                </div>
            </div>
        </form> <?php
}

function addFlowCategoryFormView() {
    ?>
        <form action="<?php print $_SERVER["PHP_SELF"]; ?>" method="POST" enctype="multipart/form-data" 
              class="col-lg-offset-3 col-lg-5 form-horizontal">
            <div class="form-group">
                <label class="control-label" style="margin-bottom:10px;">Категория:</label>
                <select class="form-control" name="cat_id">
                    <option value ="1" >Приток</option>
                    <option value ="0" >Отток</option>	
                </select>
            </div>
            <div class="form-group" id="input_cat_name">
                <label class="control-label" style="margin-bottom:10px;">Название операции (Аренда, интернет, и т.п.):</label>
                <input type="text" class="form-control" name="cat_name" >
            </div>
            <div class="form-group">
                <label class="control-label" style="margin-bottom:10px;">Описание операции:</label>
                <input type="text" class="form-control" name="cat_description" >
            </div>
            <div class="form-group">
                <label class="control-label" style="margin-bottom:10px;">Категория:</label>
                <select class="form-control" name="is_real">
                    <option value ="1" >Учитывать как доход/расход компании</option>
                    <option value ="0" >Не учитывать как доход/расход, просто поток наличных с кассы </option>	
                </select>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-3 col-lg-9">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </div>
            </div>
        </form> <?php
    }

# history

    function getUserHistory($user_id) {
        loadUserHistory($user_id);
        loadEmployees();
        global $history, $employees;
        ?><div class="table">
            <table class="my_table table table-striped table-bordered table-hover ">
                <thead>
                    <tr>
                    <?php
                    $table_headers = array("№", "Тип операции", "Сотрудник", "Операция", "Дата", "Время");
                    foreach ($table_headers as $key => $value)
                        echo "<td>" . $value . "</td>";
                    ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $cnt = 1;
                    foreach ($history as $key => $hist) {
                        echo "<tr>";
                        echo "<td>" . $cnt++ . "</td>";
                        echo "<td>" . $hist["history_type"] . "</td>";
                        echo "<td><a href='employee.php?emp_id=" . $hist["emp_id"] . "' >" .
                        $employees[$hist["emp_id"]]["name"] . "</a></td>";
                        echo "<td>" . $hist["history_text"] . "</td>";
                        echo "<td>" . toDate($hist["history_date"]) . "</td>";
                        echo "<td>" . toTime($hist["history_date"]) . "</td>";
                        echo "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div><?php
    }

    function showErrorView($error) {

        echo "<h3> Ошибка: " . $error . "</h3>";
    }

    function showAlert($message) {
        echo "<script>alert('" . $message . "');</script>";
    }

# forms 

    function getSimpleFormGroup($form_id, $label_text, $input_name, $input_type, $input_text) {
        ?>
        <div class="form-group" id="<?php echo $form_id; ?>">
            <label class="control-label" style="margin-bottom:10px;"><?php echo $label_text; ?></label>
            <input type="<?php echo $input_type; ?>" class="form-control" name="<?php echo $input_name; ?>" value="<?php echo $input_text; ?>">
        </div>
    <?php
    }

    function getButtonFormGroup($div_class, $button_text, $btn_class) {
        ?>
        <div class="form-group">
            <div class="<?php echo $div_class; ?>">
                <button type="submit" class="btn <?php echo $btn_class; ?>"><?php echo $button_text; ?></button>
            </div>
        </div>
            <?php
            }

    function getNotEditableFormGroup($label1_text, $label2_name, $label2_text, $url) {
        ?>
        <div class="form-group">
            <label class="control-label" style="height:auto;margin-bottom:10px;"><?php echo $label1_text; ?></label>
            <input class="form-control" style="" type="hidden" name="<?php echo $label2_name; ?>" value="<?php echo $label2_text; ?>">
            <label   class="form-control" style="height:auto;">
        <?php
        if ($url != "")
            echo "<a href='$url' target='_blank'>";
        echo $label2_text;
        if ($url != "")
            echo "</a>";
        ?>
            </label>
        </div> <?php
    }

    function getComingSoonView() {
        echo "<br><br><br><h2> Страница находится в разработке. Наберитесь терпения =)</h2><br><br><br><br><br><br><br><br><br>";
    }

    function getErrorPageView() {
        ?> <br><br><br><h3> Страница не найдена. Обратитесь в <a href="http://www.vk.com/proamankos">службу поддержки</a></h3><br><br><br><br><br><br><br><br><br>
            <?php
        }

        function getNoAccesPageView() {
            ?> <br><br><br><h3> У вас нету доступа к данной странице</a></h2><br><br><br><br><br><br><br><br><br>
    <?php
    getFooterView();
    exit();
}
?>




