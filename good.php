<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
			loadGoods(); 
			if(isset($_GET["good_id"]) && isset($goods[$_GET["good_id"]]))
				$good = $goods[$_GET["good_id"]];
			else #first element of array
				$good = reset($goods);	
		?>
		<title> BRK </title>
	</head>
	<body>
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getGoodInfoView(); ?>
			<?php getGoodStatisticsView(); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>