<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php';
			checkLoggedIn("yes");
		?>
    	<?php getHead(); ?>
    	<title> BRK </title>
	</head>
	<body >
		<?php getHeaderView(); ?>
		<div class="container">
			<?php getUserHistory(0); ?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>