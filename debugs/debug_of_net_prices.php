<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <?php 
                include_once '../config.php'; 
                checkLoggedIn("yes");
                getHead();
        ?>
        <title> BRK </title>
    </head>
    <body>
    <?php getHeaderView(); ?>
        <div class="container">
            <h1>Товары без себестоимостей 4</h1>
            <table class="my_table table table-striped table-bordered table-hover "
                   style="margin: auto ">
            <?php        
            loadGoods();
            loadCategories();
            global $conn, $goods, $categories;
            $comp_id = $_SESSION["user"]['comp_id'];
            $cnt = 1;
            foreach ($goods as $key => $good) {
                $sql = "SELECT COUNT(good_id) FROM debits WHERE good_id=" . $good['good_id'];
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    if ( $row['COUNT(good_id)'] > 0){
                        if($good["net_price"] == 0){
                        //if($good["net_price"] == $good["price"]){
                            echo "<tr style='background:#cef'>";
                                echo "<td>" . $cnt++ . "</td>";
                                echo "<td>" . $good["good_id"] . "</td>";
                                echo "<td>" . $good["code"] . "</td>";
                                echo "<td><a href='../" . $good["url"] . "' >" . $good['name'] . "</a></td>";
                                echo "<td>" . $good["author"] . "</td>";
                                echo "<td>" . $good["publisher"] . "</td>";
                                echo "<td>" . $categories[$good['cat_id']]["title"] . "</td>";
                                echo "<td>" . $good["price"] . " тг" . "</td>";
                            echo "</tr>";
                        }
                    }
                }
            }
            foreach ($goods as $key => $good) {
                $sql = "SELECT COUNT(good_id) FROM debits WHERE good_id=" . $good['good_id'];
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    if ( $row['COUNT(good_id)'] == 0){
                        if($good["net_price"] == 0){
                        //if($good["net_price"] == $good["price"]){
                            echo "<tr>";
                                echo "<td>" . $cnt++ . "</td>";
                                echo "<td>" . $good["good_id"] . "</td>";
                                echo "<td>" . $good["code"] . "</td>";
                                echo "<td><a href='../" . $good["url"] . "' >" . $good['name'] . "</a></td>";
                                echo "<td>" . $good["author"] . "</td>";
                                echo "<td>" . $good["publisher"] . "</td>";
                                echo "<td>" . $categories[$good['cat_id']]["title"] . "</td>";
                                echo "<td>" . $good["price"] . " тг" . "</td>";
                            echo "</tr>";
                        }
                    }
                }
            }
            ?>
            </table>
        </div>
        <footer>
           <?php getFooterView(); ?>
        </footer>
        </body>
</html>
