<?php ob_start(); ?>
<head>
	<?php 
		include_once 'config.php'; 
		checkLoggedIn("yes"); 
		getHead(); 
		checkPost(); # $post_operation = ADD_CLIENT
	?>
    <title> BRK </title>
</head>
<body>
	<?php getHeaderView(); ?>
	<div class="container">
		<?php addClientFormView(); ?>
	</div>
	<?php getFooterView(); ?>
</body>