<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
			loadAccounts();
			$days = 7; $active = 1; $account = reset($accounts);	
			if(isset($_GET["days"])) 	$days =  $_GET["days"];
			if(isset($_GET["active"])) 	$active =  $_GET["active"];
			if(isset($_GET["account_id"])) 	$account = $accounts[$_GET["account_id"]];
		?>
		<title> Касса </title>
	</head>
	<body>
		<?php getHeaderView(); ?>
		<div class="container">
			<?php
				getAccountInfoView();
				echo '<h1 class = "top_text"> Последние продажи магазина:</h1>';
				getTabs($active,"account.php?account_id=".$account["id"]."&");
				getCompanySellsByAccountIdView($account["id"]); 
			?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>