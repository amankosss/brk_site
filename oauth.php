<?php
	//DON'T TOUCH
        ob_start();
        #for changing location
	if (isset($_COOKIE["loggedIn"]))
            header("Location: index.php");
        include_once 'config.php'; 
	//checkLoggedIn("no");
	$title="Авторизация";
	if(isset($_POST["submit"])) {
	  	field_validator("login name", $_POST["login"], "string", 4, 30);
	  	field_validator("password", $_POST["password"], "string", 4, 30);
	  	if($messages){
			doIndex();
			exit;
		}
		if( !($row = checkPass($_POST["login"], $_POST["password"])) ) {
		    $messages[]="Incorrect login/password, try again";
		}
		if($messages){
			doIndex();
			exit;
		}
//		$_SESSION['SESSION_IS_LONG'] = $_POST["is_long"];
//		if($_SESSION['SESSION_IS_LONG'])
//			$_SESSION['SESSION_ON_LONG'] = 604800;
//		else $_SESSION['SESSION_ON_LONG'] = 600;
		cleanMemberCookie($row);
                setcookie('COOKIE_LAST_TIME', time(), 0, '/');                 
		header("Location: index.php");	
	} else {
		doIndex();
	}
	?>
<html>
	<head>
	    <?php getHead(); ?>
		<title><?php print $title; ?></title>
	</head>
	<body>
	    <div class="container" >
			<br><br><br><br><br><br>
			<?php
				function doIndex() {
					global $messages;
					global $title;
					if(!empty($messages)){
					  displayErrorsView($messages);
					}
				}
				$sql = "SELECT * FROM  `users` ";
				$result = $conn->query($sql);
				getAuthFormView();
			?>
            </div>
	    <?php getFooterView(); ?>
	</body>
</html>