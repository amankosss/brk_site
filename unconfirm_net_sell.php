<?php
    ob_start();
    include_once 'config.php'; 
    loadGoods(); loadStores(); loadAccounts(); loadEmployees();
    global $conn,$goods,$stores,$accounts,$employees;
    $sell_id = $_GET['sell_id'];
    $conn->autocommit(false);
    
    $user = unserialize($_COOKIE["user"]);
    $comp_id = $user["comp_id"];
    $emp_id = $user["id"];
    $sql = "SELECT * FROM sells WHERE sell_id = $sell_id";
    $result = $conn->query($sql);
    
    //alert($_SERVER["HTTP_REFERER"]);
    if ($row = $result->fetch_assoc()) {
        if (isset($row["sell_id"])){
            $sell_info = $row;
            if($sell_info['is_net_sell'] == 2){
                showAlert("Ошибка! Продажа еще не оплачена");
                if (isset($_COOKIE["SAVED_PAGE"])) {
                    header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
                }   
                exit();
            }
            else if($sell_info['is_net_sell'] == 0){
                showAlert("Ошибка! Продажа не является интернет продажой");
                //$conn->close();
                //if (isset($_COOKIE["SAVED_PAGE"])) {
                //    header("Location: " . $_COOKIE['SAVED_PAGE']);
                //}
                if (isset($_COOKIE["SAVED_PAGE"])) {
                    header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
                }
                exit();
            }
        }
        else {
            showAlert("Ошибка! Не существующая продажа");
            if (isset($_COOKIE["SAVED_PAGE"])) {
                header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
            }
            exit();
        }
    }
    if(!hasAccessToConfirmNetSells($user['job']) && $user['store_id'] != $sell_info['store_id']){
        showAlert("Ошибка! У вас нет доступа для подтверждения продажи");
        if (isset($_COOKIE["SAVED_PAGE"])) {
            header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
        }
        exit();
    }
    $current_date = date("Y-m-d H:i:s");
    $sell_history_info = 'Продано ' . $sell_info["count"] . ' штук товара ' . $goods[$sell_info["good_id"]]["name"] . ' c магазина ' . $stores[$sell_info["store_id"]]["name"] . ' на кассу ' . $accounts[$sell_info["account_id"]]['title'] . '. Дата оформление заказа ' . $sell_info["sell_date"] . '.';
    $history = "Отменена потверждения интернет продажи сотрудником " .
    $user["surname"] . " " . $user["name"] . " " . $user["patronymic"] .". Информация об отмененной интернет продаже: ".
            $sell_history_info . " сотрудник " . $employees[$sell_info['employee_id']]["surname"] . " " . $employees[$sell_info['employee_id']]["name"] . " " . $employees[$sell_info['employee_id']]["patronymic"] . "," .
                    " магазин " . $stores[$sell_info['store_id']]["name"];
    $sql = "UPDATE `sells` SET `is_net_sell` = 2 , `cash_date` = '$current_date' WHERE `comp_id` = $comp_id AND `sell_id` = $sell_id ;";
    //showAlert($sql);
    if ($conn->query($sql) === TRUE) {
        $type = "Отмена оплаты интернет продажи";
             $sql2 = "INSERT INTO history (emp_id, comp_id, history_text, history_type, history_date) "
            . " VALUES ($emp_id, $comp_id, '$history' , '$type' , '$current_date' )";
        if ($conn->query($sql2) === TRUE) {
            $conn->commit();
            showAlert("Отменена оплата интернет продажи");
            if (isset($_COOKIE["SAVED_PAGE"])) {
                header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
            }
        } else {
            $conn->rollback();
            showAlert("Ошибка с записью в историю, попробуйте снова!!!");
            if (isset($_COOKIE["SAVED_PAGE"])) {
                header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
            }
        }
    } else {
        $conn->rollback();
        showAlert("Ошибка на сервере, обратитесь к администратору!!!");
        if (isset($_COOKIE["SAVED_PAGE"])) {
            header("Refresh: 0; URL=" . $_COOKIE['SAVED_PAGE']);
        }
    }
    $conn->close();
    
