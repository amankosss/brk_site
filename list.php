<?php ob_start(); ?>
<!DOCTYPE html>
<html lang="ru">
	<head>
		<?php 
			include_once 'config.php'; 
			checkLoggedIn("yes");
			getHead();
			loadGoods(); 
		?>
		<title> BRK </title>
	</head>
	<body>
		<?php getHeaderView(); ?>
		<div class="container">
			<?php
			if(!isset($_GET["type"])){
				getErrorPageView();
				getFooterView();
				exit();	
			}
			$table = $_GET["type"];
                        $user = unserialize($_COOKIE["user"]);
    			if($table=="operations"){
				$user["selected_actions"] = 255;
				getMyActionsView();
			}
			else if($table == "stores")
				getStoresView();
			else if($table == "accounts")
				getAccountsView();
			else 
				getComingSoonView(); 
			?>
		</div>  
		<?php getFooterView(); ?>
	</body>
</html>